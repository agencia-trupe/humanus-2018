<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

defined('APP_ENV') || define('APP_ENV', APPLICATION_ENV);
defined('ENV_DEV') || define('ENV_DEV', APP_ENV=='development');
defined('ENV_PRO') || define('ENV_PRO', APP_ENV=='production');
defined('ENV_TEST')|| define('ENV_TEST',APP_ENV=='testing');
defined('DS') || define('DS', DIRECTORY_SEPARATOR);

// if(APPLICATION_ENV!='production') error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/models'),
    get_include_path(),
)));

function __autoload($class){
    require_once(implode("/",explode("_",$class)).".php");
}

//Is_Var::dump(get_include_path());exit();

/** ******************************************
 * Libs
 ****************************************** */
Lib::import('*');
Lib::import('helper.*');
/** ******************************************
 * /Libs
 ****************************************** */

/** ******************************************
 * Rotas personalizadas
 ****************************************** */
$routes = array(
    'img' => new Zend_Controller_Router_Route('img/:img/*',array(
        'controller' => 'img',
        'action'     => 'index'
    )),
    'min' => new Zend_Controller_Router_Route('min/:type/:files',array(
        'controller' => 'min',
        'action'     => 'index'
    )),
    'busca' => new Zend_Controller_Router_Route('busca/:busca/',array(
        'controller' => 'busca',
        'action'     => 'index'
    )),
    'seja-humanus' => new Zend_Controller_Router_Route_Regex('seja-humanus/(historia|missao-visao-valores|reconhecimento|projeto-social)',array(
    // 'seja-humanus' => new Zend_Controller_Router_Route('seja-humanus/:pagina/',array(
        'controller' => 'seja-humanus',
        'action'     => 'index',
    ),array(1 => 'pagina')),
    'clientes' => new Zend_Controller_Router_Route_Regex('clientes/(nossos-clientes|cases|diferenciais)',array(
        'controller' => 'clientes',
        'action'     => 'index',
    ),array(1 => 'pagina')),
    'clientes-case' => new Zend_Controller_Router_Route('clientes/case/:alias/',array(
        'controller' => 'clientes',
        'action'     => 'case'
    )),
    'solucoes' => new Zend_Controller_Router_Route_Regex('solucoes/(produtos|consultoria-empresarial|centro-de-capacitacao|webinars)',array(
        'controller' => 'solucoes',
        'action'     => 'index',
    ),array(1 => 'pagina')),
    'solucoes-produto' => new Zend_Controller_Router_Route('solucoes/produto/:alias/',array(
        'controller' => 'solucoes',
        'action'     => 'produto'
    )),
    'solucoes-solucao' => new Zend_Controller_Router_Route('solucoes/solucao/:alias/',array(
        'controller' => 'solucoes',
        'action'     => 'solucao'
    )),
    'blog' => new Zend_Controller_Router_Route('blog/',array(
        'controller' => 'noticias',
        'action'     => 'index'
    )),
    'blog-categoria' => new Zend_Controller_Router_Route('blog/:categoria/',array(
        'controller' => 'noticias',
        'action'     => 'index'
    )),
    'blog-noticia' => new Zend_Controller_Router_Route('blog/:categoria/:noticia/',array(
        'controller' => 'noticias',
        'action'     => 'noticia'
    )),
    'noticias' => new Zend_Controller_Router_Route('noticias/:categoria/',array(
        'controller' => 'noticias',
        'action'     => 'index'
    )),
    'noticia' => new Zend_Controller_Router_Route('noticia/:noticia/',array(
        'controller' => 'noticia',
        'action'     => 'index'
    )),
    'portal-consultoria' => new Zend_Controller_Router_Route('portal/consultoria/:categoria/',array(
        'module'     => 'portal',
        'controller' => 'consultoria',
        'action'     => 'index'
    )),
    'admin-edit' => new Zend_Controller_Router_Route('admin/:controller/edit/:id/',array(
        'module'     => 'admin',
        'controller' => ':controller',
        'action'     => 'edit'
    )),
    'paginas-edit' => new Zend_Controller_Router_Route('admin/paginas/edit/:alias/',array(
        'module'     => 'admin',
        'controller' => 'paginas',
        'action'     => 'edit'
    )),
);

$front = Zend_Controller_Front::getInstance();
$router = $front->getRouter();
$router->addRoutes($routes);
/** ******************************************
 * /Rotas personalizadas
 ****************************************** */

$router = new Zend_Controller_Router_Rewrite();
$request = new Zend_Controller_Request_Http();
$router->route($request);

/** ******************************************
 * Defines
 ****************************************** */
defined('SITE_NAME')
    || define('SITE_NAME', "humanus");

defined('SITE_TITLE')
    || define('SITE_TITLE', "Humanus");

defined('SITE_DESCRIPTION')
    || define('SITE_DESCRIPTION', "");
    
defined('ROOT_PATH')
    || define('ROOT_PATH', $request->getBaseUrl());

defined('IS_DOMAIN')
    || define('IS_DOMAIN', !(bool)strstr(ROOT_PATH, SITE_NAME) && !(bool)strstr(ROOT_PATH, 'previa'));

defined('IS_SSL')
    || define('IS_SSL', (bool)$_SERVER['HTTPS']);

defined('URL')
    || define('URL', (IS_SSL ? 'https' : 'http')."://".$_SERVER['HTTP_HOST'].ROOT_PATH);

defined('FULL_URL')
    || define('FULL_URL', strtolower(URL.str_replace(ROOT_PATH.'','',$_SERVER['REQUEST_URI'])));

defined('SCRIPT_MIN')
    || define('SCRIPT_MIN', 'load'); // 'load' or 'min'

defined('SCRIPT_RETURN_PATH')
    || define('SCRIPT_RETURN_PATH', IS_DOMAIN ? '.' : '..');

defined('JS_PATH')
    || define('JS_PATH', ROOT_PATH.'/public/js');

defined('JS_URL')
    || define('JS_URL', URL.'/public/js');

defined('CSS_PATH')
    || define('CSS_PATH', ROOT_PATH.'/public/css');

defined('CSS_URL')
    || define('CSS_URL', URL.'/public/css');
    
defined('IMG_PATH')
    || define('IMG_PATH', ROOT_PATH.'/public/images');

defined('IMG_URL')
    || define('IMG_URL', URL.'/public/images');
    
defined('FILE_PATH')
    || define('FILE_PATH', ROOT_PATH.'/public/files');
    
defined('FILE_URL')
    || define('FILE_URL', URL.'/public/files');
    
defined('DEFAULT_LANGUAGE')
    || define('DEFAULT_LANGUAGE', 'pt');

defined('HAS_PORTAL')
    || define('HAS_PORTAL', 1);
    
defined('HAS_SALE')
    || define('HAS_SALE', true);

defined('MYSQL_SUPPORTS_MATCHAGAINST')
    || define('MYSQL_SUPPORTS_MATCHAGAINST', false);
    
defined('GOOGLE_ANALYTICS_ID')
    || define('GOOGLE_ANALYTICS_ID', ENV_DEV ? 0 : '');
    
defined('ZOPIM_ID')
    || define('ZOPIM_ID', ENV_DEV ? 0 : '4sxhLOdsiGefAtbUtMeZY3lcjXXpnnPM');
    
/** ******************************************
 * /Defines
 ****************************************** */

/** Zend_Application */
//require_once 'Zend/Application.php';

// load configuration
$config = new Zend_Config_Ini('./application/configs/application.ini',APPLICATION_ENV);

// setup database
if(APPLICATION_ENV=='production'){
    $cache = Zend_Cache::factory(
        'Core', 
        'File',
        array( // frontendOptions
            'automatic_serialization' => true,
            'lifetime' => 100000,
            'cache_id_prefix' => 'metaDataFront_',
        ),
        array( // backendOptions
            'automatic_serialization' => true,
            'lifetime' => 100000,
            'cache_id_prefix' => 'metaDataBack_',
        )
    );
    Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
}

$dbAdapter = Zend_Db::factory(
    $config->resources->db->adapter,
    $config->resources->db->params->toArray()
);
Zend_Db_Table::setDefaultAdapter($dbAdapter);

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();
            
