<?php
/**
 * Funções genéricas de Js
 */
class Js
{
	const return_path = SCRIPT_RETURN_PATH;
    private static $r = array("|","\\",":",";"); // substitutos para DIRECTORY_SEPARATOR na URL por motivos de rewrite do Zend
    
    public function min($files)
    {
        $scripts = array_map('self::convert_path',$files);
		return "<script type=\"text/javascript\" src=\"".URL."/min/js/".implode(',',$scripts)."\"></script>";
    }
    
    static function load()
	{
		$scripts = array();
        $args = func_get_args();
        $args = is_array($args[0]) ? $args[0] : $args;
		foreach($args as $arg){
			if(file_exists(self::return_path.$arg)){
                $scripts[] = "<script type=\"text/javascript\" src=\"".$arg."?".filemtime(self::return_path.$arg)."\"></script>";
            }
		}
		return implode(PHP_EOL,$scripts);
	}
	
	function script($script)
	{
		return "<script type=\"text/javascript\">".PHP_EOL.$script.PHP_EOL."</script>".PHP_EOL;
	}
	
	function alert($alert)
	{
		return self::script("alert(\"".$alert."\")");
	}
	
	function location($location)
	{
		return self::script('location.href = \''.$location.'\'');
	}
	
	function back($number_back = '')
	{
		return self::script('history.back('.$number_back.')');
	}
    
    public static function normalize_path($str)
    {
        return str_replace(self::$r,"/",$str);
    }
        
    public static function convert_path($str)
    {
        return str_replace("/",self::$r[0],$str);
    }
    
    public function exists($file)
    {
        return file_exists(self::return_path.$file.'.js');
    }
}
