<?php
/**
 * Funções genéricas de Css
 */
class Css
{
	const return_path = SCRIPT_RETURN_PATH;
    private static $r = array("|","\\",":",";"); // substitutos para DIRECTORY_SEPARATOR na URL por motivos de rewrite do Zend
    
    public function min($files,$params='')
    {
        $scripts = array();
        $less    = array();
        foreach($files as $f){
            (end(explode('.',$f)) == 'less') ?
                $less[] = $f : $scripts[] = self::convert_path($f);
        }
        $return = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".URL."/min/css/".implode(',',$scripts)."\" " . $params . " />";
        if(count($less)){
            foreach($less as $l){
                $return.= PHP_EOL.self::loadLess(CSS_PATH.'/'.$l);
            }
        }
		return $return;
    }
    
    static function load() // Load geral - detecta se é css ou less (em desenvolvimento)
	{
		$scripts = array();
        $args = func_get_args();
        $params = is_array($args[0]) && isset($args[1]) ? $args[1] : '';
        $args = is_array($args[0]) ? $args[0] : $args;

		foreach($args as $arg){
			$ext = end(explode(".",$arg));
            
            switch($ext){
                case "css":
                    $arg2 = str_replace(".css",".less",$arg);
                    if(file_exists(self::return_path.$arg)){
                        $scripts[] = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $arg . "?".filemtime(self::return_path.$arg)."\" " . $params . " />";
                    } else if(file_exists(self::return_path.$arg2)){
                        $scripts[] = "<link rel=\"stylesheet/less\" href=\"" . $arg2 . "?".filemtime(self::return_path.$arg2)."\" " . $params . " />";
                    }
                    break;
                case "less":
                    $arg2 = str_replace(".less",".css",$arg);
                    if(file_exists(self::return_path.$arg2)){
                        $scripts[] = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $arg2 . "?".filemtime(self::return_path.$arg2)."\" " . $params . " />";
                    } else if(file_exists(self::return_path.$arg)){
                        $scripts[] = "<link rel=\"stylesheet/less\" href=\"" . $arg . "?".filemtime(self::return_path.$arg)."\" " . $params . " />";
                    }
                    break;
            }
		}
		return implode(PHP_EOL,$scripts);
	}
    
    function load1()
	{
		$scripts = array();
        $args = func_get_args();
        $args = is_array($args[0]) ? $args[0] : $args;
		foreach($args as $arg){
			if(file_exists(self::return_path.$arg)){
                $scripts[] = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $arg . "\" />";
            } else {
                $scripts[] = self::loadLess(str_replace(".css",".less",$arg));
            }
		}
		return implode("\n",$scripts);
	}
    
	function loadLess()
	{
		$scripts = array();
        $args = func_get_args();
        $args = is_array($args[0]) ? $args[0] : $args;
		foreach($args as $arg){
			$css = str_replace(".less",".css",$arg);
            
            if(file_exists(self::return_path.$css)){
                $scripts[] = self::load($css);
            } else if(file_exists("..".$arg)){
                $scripts[] = "<link rel=\"stylesheet/less\" href=\"" . $arg . "\" />";
            }
		}
		return implode(PHP_EOL,$scripts);
	}
    
	function style($script)
	{
		return "<style type=\"text/css\">".PHP_EOL.$script.PHP_EOL."</style>".PHP_EOL;
	}
    
    public static function normalize_path($str)
    {
        return str_replace(self::$r,"/",$str);
    }
        
    public static function convert_path($str)
    {
        return str_replace("/",self::$r[0],$str);
    }
    
    public function exists($file)
    {
        $exists = file_exists(self::return_path.$file.'.css') or
        $exists = file_exists(self::return_path.$file.'.less');
        return $exists;
    }
}
