if(APPLICATION_ENV == 'development') $.getScript(JS_PATH+'/less.js', function(){ // load less in dev
    less.watch();
});

$('#nav-treinamentos a[href*="/portal"]').addClass('active');

if(CONTROLLER=='index') {
	// $.getScript(JS_PATH+'/Cep.js',function(){
	//     $('#cep').blur(function(){
	//         Cep.search($(this).val());
	//     });
	// });
	$.getScript(JS_PATH+'/cadastro.js');
}
if(CONTROLLER=='treinamentos' && ACTION=='investimento') {
	$.getScript(JS_PATH+'/meu-carrinho.js');
}

if(CONTROLLER=='avaliacao') {
	// marcar nota
	$('a.icone-carinhas').on('click',function(){
		var $this = $(this),
			$td   = $this.parent('td'),
			$tr   = $td.parent('tr'),
			$trc  = $('tr#tr_nota_comentario_'+$this.data('pergunta-id')),
			$notas= $td.find('a.icone-carinhas'),
			$nota = $td.find('input'),
			nota  = Number($this.data('nota')),
			$comente = $tr.find('.comente');

		$notas.removeClass('active');
		$this.addClass('active');
		$nota.val(nota);
		
		// mostra botão de comentar se nota baixa
		if(nota < 3) {
			if(nota==1) $comente.addClass('red');
			$comente.show();
		} else {
			$comente.hide();
			$comente.removeClass('red');
			$trc.find('textarea').val('');
			$trc.slideUp();
		}
	});

	// mostrando comentarios
	$('.comente').click(function(e){
		var $this = $(this),
			$trc  = $('tr#tr_nota_comentario_'+$this.data('pergunta-id'));
		
		$trc.slideDown();
	});

	// verificar envio
	$('.frm-pesquisa').submit(function(e){
		var $this = $(this),
			err = 0;

		// checando comentario de notas baixas
		$this.find('input[name^=nota]').each(function(i,elm){
			var $elm = $(elm),
				$com = $('#obs_'+$elm.data('pergunta-id'));

			if(Number($elm.val()) < 3 && $com.val()==='') {
				err++;
			}
		});
		
		// todos campos preenchidos
		$this.find('input[type=radio]').each(function(i,elm){
			var $elm = $(elm);
			if($('input[name='+$elm.attr('name')).val()==='') {
				console.log(['Radio',$elm]);
				err++;
			}
		});
		$this.find('textarea:not(.nota-comentario)').each(function(i,elm){
			var $elm = $(elm);
			if($elm.val()==='') {
				err++;
			}
		});

		if(err) {
			e.preventDefault();
			alert('Preencha todas as questões corretamente');
			return false;
		} else {
			// e.preventDefault();
			// alert('OK');
			return true;
		}
	});

}

if(CONTROLLER=='consultoria') {
	$('.bt-cons-del').click(function(e){
		if(!confirm('Deseja reamente excluir o arquivo?')){
			e.preventDefault();
			return false;
		}
	});

	$('#open-frm-consult').click(function(e){
		$('#frm-consult ul').slideToggle();
		$(this).toggleClass('active');
	});

	$('#arquivo').change(function(e){
		var t = this;
		$('#arquivo-consultoria .filename').text(t.files[0].name);
	});

	var $linksDetalhesFBox = $('.bt-cons-detalhes'), optsDetalhes={};
    if($linksDetalhesFBox.length) {
        _loadFancybox(function(){
        	optsDetalhes = {
                maxWidth    : 950,
                maxHeight   : 600,
                padding     : 0,
                fitToView   : false,
                width       : '50%',
                height      : '50%',
                autoSize    : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
            };

            if(isSmartphone) {
            	optsDetalhes.maxWidth = '100%';
            	optsDetalhes.maxHeight = '100%';
            	optsDetalhes.width = '100%';
            	optsDetalhes.height = '100%';
            }

            $linksDetalhesFBox.fancybox(optsDetalhes);
        });
    }
}