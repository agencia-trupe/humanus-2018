var MAPAS = MAPAS || {};

$(document).ready(function(){
    $('input#telefone').focus(function(){
        $(this).removeClass('pre');
    }).blur(function(){
        var $t = $(this);
        if($t.val() == ''){
            $t.addClass('pre');
        }
    });
    
    // enviando contato
    if(ACTION=='index'){
    head.js(JS_PATH+'/is.simple.validate.js');
    $('form.contato').submit(function(e){
        e.preventDefault();
        var $status = $('p.status').removeClass('error');
        $status.html('Enviando...');
        
        // head.js(JS_PATH+'/is.simple.validate.js',function(){
            if($('form.contato').isSimpleValidate({pre:true})){
                var url  = URL+'/'+CONTROLLER+'/enviar.json',
                    data = $('form.contato').serialize();
                
                $.post(url,data,function(json){
                    if(json.error){
                        $status.addClass('error').html(json.error);
                        return false;
                    }
                    
                    // $status.html(json.msg);
                    $status.html(json.msg);
                    _resetForm('form.contato');
                    /*Shop.messageBox('<p class="shop-message-title contato">'+json.msg+'</p>'+
                                    '<p class="shop-message-buttons">'+
                                    '<a href="#" class="shop-close-message-box fechar-contato single">Fechar</a>'+
                                    '</p>');*/
                },'json');
            } else {
                $status.addClass('error').html('* Preencha todos os campos');
            }
        // });

        return false;
    });

    _loadScrollTo(function(){
        $('.ver-mapa').click(function(e){
            var mapa  = MAPAS[$(this).data('id')],
                frame = '<iframe src="'+mapa+'" id="frame_mapa" frameborder="0" style="border:0" allowfullscreen></iframe>';
            
            $('#mapa').html(frame);

            $.scrollTo($('#mapa'),500,{
                axis:'y'
            });
        });
    });
    } // if ACTION index
    
    // trabalhe-conosco
    if(ACTION=='trabalhe-conosco'){
    $.getCss(JS_PATH+'/fileinput/fileinput.css');
    head.js(JS_PATH+'/fileinput/jquery-ui-1.8.9.custom.min.js',JS_PATH+'/fileinput/jquery.fileinput.min.js',function(){
        $('#arquivo').fileinput({
            // inputText: 'ANEXAR CURRÍCULO',
            buttonText: 'ANEXAR CURRÍCULO'
            // buttonText: 'SELECIONAR'
        });
        $('#arquivo').change(function(e){
            if($(this).val().length)
                $('.fileinput-wrapper').addClass('has-file');
            else
                $('.fileinput-wrapper').removeClass('has-file');
        });
    });
    if(APPLICATION_ENV=='development') Test.formTrabalhe();
    } // if ACTION trabalhe-conosco
});
