var MAX_FOTOS = 1, MAX_SIZE = 5242880, allow_photos=false;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink,separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px','height':'170px'});
    $('div.wysiwyg').css({'margin-left':'110px','height':'200px'});
    $('div.wysiwyg div').remove();
    
    $("#frm-produtos").submit(function(e){
        if($.trim($("#titulo").val()).length == 0){
            $("#titulo").focus();
            Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
            return false;
        }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-produtos").submit(e);
    });
    
    $("#search-by").change(function(){
        if($(this).val() == "categoria_id"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(categorias){
                for(i in categorias){
                    $combo.append('<option value="'+i+'">'+categorias[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($("#search-txt").attr("type").indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
    });
    
    // arruma css de radios no IE
    if($.browser.msie){
        $('li.radio label:nth-child(2),li.radio label:nth-child(3)').css({'text-align':'left','width':'75px'});
    }
    
    // seta cor para a foto
    $('.list_fotos select').live('change',function(){
        var $this = $(this),
            $li   = $this.parents('li'),
            foto_id = $li.find('.foto_id').val(),
            produto_id = $('#produtos_id').val(),
            data = {'foto':foto_id,'produto':produto_id,'cor':$this.val()},
            url  = [URL,CONTROLLER,DIR,'foto-cor.json'].join('/');
        
        if(data.cor == 0) {
            alert('Escolha uma cor');
            return false;
        }
        
        $this.attr('disabled',true);
        
        $.post(url,data,function(json){
            $this.attr('disabled',false);
            if(json.error) alert(json.error);
        },'json');
    });
});