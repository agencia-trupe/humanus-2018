var MAX_FOTOS = 1, MAX_SIZE = 5242880, allow_photos = false;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();
    _contentImgSize();
    
    $('input,select,textarea',"#frm-"+DIR).each(function(i,elm){
        var $elm = $(elm);
        if($elm.attr('id')=='pagina_id') return false;
        $elm.attr('name', $elm.attr('name')+'[]');
        $elm.attr('id', $elm.attr('id')+'_'+i);
    });
    
    $("#frm-"+DIR).submit(function(e){
        // if($.trim($("#titulo").val()).length == 0){
        //     $("#titulo").focus();
        //     Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
        //     return false;
        // }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });
});
