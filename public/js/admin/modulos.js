var MAX_FOTOS = 1, MAX_SIZE = 5242880, allow_photos=false;
var PHOTO_DESCRIPTION = false;
var fileUploadOpts2;

/*global $ */
$(document).ready(function () {
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();

    // console.log(PHOTO_DESCRIPTION);
    var $pagina_id = $('#pagina_id');
    comboCategoriaShow($pagina_id.get(0));
    $pagina_id.change(function(e){
        comboCategoriaShow(this);
    });
    $('select.cat-id').change(function(e){
        comboCategoriaChoose(this);
    });

    $("#search-by").change(function(){
        if($(this).val() == "categoria_id"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(categorias){
                for(i in categorias){
                    $combo.append('<option value="'+i+'">'+categorias[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($("#search-txt").attr("type").indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
    });

    /* Upload fotos galeria */
    fileUploadOpts2 = {
        uploadTable: $('#file_upload_list2'),
        downloadTable: $('.list_fotos')
    };
    
    fileUploadOpts2.buildUploadRow = function (files, index) {
        // alert(files[index].size);
        //return $('<tr><td class="filename">' + files[index].name + '<\/td>' +
        return $('<tr>'+
                //'<td class="filename file_upload_preview"><\/td>' +
                '<td class="file_upload_progress"><div></div></td>' +
                '<td class="filesize">'+Files.formatBytes(files[index].size)+'</td>' +
                '<td class="file_upload_cancel">' +
                '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                '<\/button>' +
                '<\/td>' +
                //'<td class="file_delete">' +
                //'<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
                //'<span class="ui-icon ui-icon-trash"><\/span>' +
                //'<\/button>' +
                //'<\/td>' +
                '<\/tr>');
    };

    fileUploadOpts2.buildDownloadRow = function (file) {
        //return $('<tr><td>' + file.name + '<\/td><\/tr>');
        if(file.error){
            return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
        } else {
            var hasOrder = fileUploadOpts2.downloadTable.hasClass('order_fotos');
            
            return $('<li><a href=\'' + URL+'/img/'+DIR+":"+file.name + '\' rel=\'gallery_group2\'>'+
                     '<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=150&h=150\'><\/a>'+ // 3 fotos
                     //'<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=65&h=65\'><\/a>'+ // 4 fotos
                     
                     // '<input type="text" '+
                     //    'name="foto_descricao[]" '+
                     //    'class="txt foto_descricao prevalue" '+
                     //    'id="foto_descricao_'+file.id+'" '+
                     //    'value="" '+
                     //    'data-prevalue="Descrição..." '+
                     //    'data-foto-id="'+file.id+'" '+
                     //    'data-portfolio-id="'+ID+'" \/>'+
                     // '<label for="foto_flag_'+file.id+'">'+
                     // '<input type="checkbox" '+
                     //    'name="foto_flag[]" '+
                     //    'class="txt foto_flag" '+
                     //    'id="foto_flag_'+file.id+'" '+
                     //    'value="1" '+
                     //    'data-foto-id="'+file.id+'" '+
                     //    'data-portfolio-id="'+ID+'" \/>'+
                     // ' Imagem de capa<\/label>'+
                     
                     '<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
                     '<span class="ui-icon ui-icon-trash"><\/span>' +
                     '<\/button>' +
                     ((allow_photos) ? '<button title="Inserir esta imagem no texto" class="ui-state-default ui-corner-all file-insert">'+
                        '<span class="ui-icon ui-icon-arrowthick-1-w"></span>'+
                        '</button>' : '') +
                     '<input type="hidden" name="foto_id[]" value="'+file.id+'" class="foto_id" />' +
                     
                     ((hasOrder) ?
                     '<input type="hidden" name="foto_ordem[]" value="1" class="foto_ordem" />' :
                     '') +

                     '<span class="foto_status"><\/span>'+
                     '<\/li>');
        }
    };
    
    fileUploadOpts2.beforeSend = function(event, files, index, xhr, handler, callBack){
        // console.log('modulos');
        $('#file_upload_list2 tr.error').remove();
        
        var patt_img  = /.*(gif|jpeg|png|jpg)$/i,///(jpeg|jpg|png|gif|bmp)/gi,
            file_name = files[index].name,
            max_size  = (MAX_SIZE)  ? MAX_SIZE  : 2097152,
            max_files = (MAX_FOTOS) ? MAX_FOTOS : 0,
            file_count= $(".list_fotos li").size() + $('#file_upload_list2 tr').size(),
            max_count = (max_files>0) ? file_count <= max_files : true,
            msg       = "Erro ao enviar imagem";
        //console.log(file_name+","+Files.getExt(file_name)+","+patt_img.test(Files.getExt(file_name)));
        //console.log(files[index].size < max_size);
        //console.log(max_count);
        //return false;
        if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size && max_count){
            callBack();
            return true;
        }
        
        /*switch(false){
            modulo patt_img.test(Files.getExt(file_name)):
                msg = "Imagem inválida"; break;
            modulo files[index].size < max_size:
                msg = "Tamanho máx. permitido: "+Files.formatBytes(max_size); break;
            modulo max_count:
                msg = "Você pode enviar no máximo "+max_files+" fotos"; break;
                //msg = "Erro ao enviar imagem"; break;
            default:
                callBack();
                return true;
        }*/ 
        msg = !patt_img.test(Files.getExt(file_name)) ? "Imagem inválida" : msg;
        msg = files[index].size >= max_size ? "Tamanho máx. permitido: "+Files.formatBytes(max_size) : msg;
        msg = !max_count ? "Você pode enviar no máximo "+max_files+" fotos" : msg;
        
        handler.cancelUpload(event, files, index, xhr, handler);
        handler.addNode($('#file_upload_list2'),
            $('<tr class="error"><td colspan="3">'+msg+' ('+file_name+')</td></tr>')
        );
        //callBack();
    };
    
    fileUploadOpts2.onComplete = function (event, files, index, xhr, handler) {
        // var json = handler.response;
        Gallery.show();
        $('.foto_descricao.prevalue').prevalue();

        var hasOrder = fileUploadOpts2.downloadTable.hasClass('order_fotos');
        if(hasOrder) ajust_order_fotos2();
    };
    
    $('#file_upload2').fileUploadUI(fileUploadOpts2);
    
    $("#files2 tr").mouseover(function(){
        $(this).addClass("hover");
    }).mouseout(function(){
        $(this).removeClass("hover");
    });
    
    $("#file_upload_list2 tr.error").live('click',function(){
        $this = $(this);
        
        $this.fadeOut("fast",function(){
            $this.remove();
        })
    });
    
    // descricao
    if(document.getElementById('modulos_fotos2')){
        $('input.foto_descricao').each(function(i,v){
            var $t = $(this);
            if($.trim($t.val())=='') $t.prevalue('Descrição...');
        });
        
        $('input.foto_descricao').live('keyup',function(e){
            if(e.which==13) $(this).blur();
            // if(e.which==13) FotoDesc.save($(this));
        }).live('blur',function(e){
            FotoDesc.save($(this));
        });
        
        $('input.foto_flag').live('change',function(e){
            FotoDesc.check($(this));
        });
    }

    // ordenacao de galeria de fotos
    var $order_fotos2 = $('.list_fotos2.order_fotos');
    if($order_fotos2.length){
        ajust_order_fotos2();
        $order_fotos2.sortable({
            items: "li",
            revert:true,
            start:function(e,ui){
                var $rows = $order_fotos2.find('li');
                $rows.addClass('disabled');
                ui.item.removeClass('disabled').addClass('grabber');
            },
            stop:function(e,ui){
                var $rows = $order_fotos2.find('li');
                $rows.removeClass('disabled');//.removeClass('grabber');
                $rows.attr('style','');
                window.setTimeout(function(){
                    $rows.removeClass('grabber');
                },500);
                ajust_order_fotos2();
            }
        });
    }
});

// ajusta ordem da lista e habilita/desabilita botões de ordenação
function ajust_order_fotos2(){
    var values = {'id':[],'ordem':[]};

    $('.list_fotos2.order_fotos .foto_ordem').each(function(i,v){
        var $this = $(this);
        $this.val(i+1);

        values.id.push($this.parent('li').find('.foto_id').val());
        values.ordem.push(i+1);
    });

    $.post([URL,'admin',CONTROLLER,'ordem-fotos.json'].join('/'),values,function(json){
        if(json.error) alert(json.error);
    },'json');
    
    $('.list_fotos2.order_fotos li').removeClass('disabled');
}

var FotoDesc = {
    save : function(elm){
        if($.trim(elm.val())==''){
            //alert('Preencha o campo descrição.');
            //elm.focus();
            //return false;
        }
        
        var url  = GLOBAL_URL+'save-descricao.json',
            data = {
                'portfolio_id' : elm.data('portfolio-id'),
                'foto_id'   : elm.data('foto-id'),
                'descricao' : elm.val()
            },
            status = elm.parents('li').find('.foto_status');
        
        elm.attr('disabled',true);
        status.html('Aguarde...').show();
        
        $.post(url,data,function(json){
            elm.attr('disabled',false);
            
            if(json.error){
                alert(json.error);
                status.html('');
                return false;
            }
            
            status.html(json.msg||'Concluído').delay(3000).fadeOut('slow');
        },'json');
    },
    
    check : function(elm){
        var url  = GLOBAL_URL+'save-capa.json',
            data = {
                'portfolio_id' : elm.data('portfolio-id'),
                'foto_id'   : elm.data('foto-id'),
                'flag'      : Number(elm.is(':checked'))
            },
            status = elm.parents('li').find('.foto_status');
        
        elm.attr('disabled',true);
        status.html('Aguarde...').show();
        
        $.post(url,data,function(json){
            elm.attr('disabled',false);
            
            if(json.error){
                alert(json.error);
                status.html('');
                return false;
            }
            
            if(data.flag==1){
                $('input.foto_flag').each(function(){
                    $(this).attr('checked',false);
                })
                
                elm.attr('checked',true);
            }
            
            status.html(json.msg||'Concluído').delay(3000).fadeOut('slow');
        },'json');
    }
}

function comboCategoriaShow(combo) {
    var $this = $(combo);

    $('li.cat-id').removeClass('visible');
    $('li.categoria_id_'+$this.val()).addClass('visible');
}
function comboCategoriaChoose(combo) {
    var $this = $(combo),
        val = $this.val();

    if(val=='__none__') val = '';
    $('#categoria_id').val(val);
}

