var Msg = Messenger;
Msg.container = $("div.main .content");

$(document).ready(function(){
    if(APPLICATION_ENV == 'development') $.getScript(JS_PATH+'/less.js',function(){
        // less.watch();
        // js_watch();
    }); // load less in dev
        
    // check https
    if(FULL_URL.indexOf('https:')!=-1) if(window.location.href.indexOf('https:')==-1) window.location.href = FULL_URL;
    
    $("a[href^='#']").click(function(e){
        e.preventDefault();
    });
	
	$('input[type=checkbox],input[type=radio]').css('border','none');
    
    var menu_time = "fast";
    $(".admin-menu .navigation ul").addClass("submenu");
    $(".admin-menu .navigation ul ul").removeClass("submenu").addClass("submenusub");
    
    $(".admin-menu a.has-sub").append(' &darr;');
    $(".admin-menu a.has-sub").mouseenter(function(){
        clearTimeout($(this).data('timeoutId'));
        $(this).parent().find("ul.submenu").slideDown(menu_time);
    }).mouseleave(function(){
        var someelement = this;
        var timeoutId = setTimeout(function(){ $(someelement).parent().find("ul.submenu").slideUp(menu_time);}, 200);
        $(someelement).data('timeoutId', timeoutId);
    });
    $("ul.submenu").mouseenter(function(){
        clearTimeout($(this).parent().find("a.has-sub").data('timeoutId'));
    }).mouseleave(function(){
        $(this).slideUp(menu_time);
    });
    $('ul.submenu').each(function(){
        var $this = $(this);
        $this.attr('id','sub'+$this.prev('a').attr('id'));
    });

    // $(".admin-menu a.has-sub-sub").append(' &rarr;');
    // $(".admin-menu a.has-sub-sub").mouseenter(function(){
    $(".admin-menu > ul > li ul li:has(ul.submenusub)").find('a.has-sub-sub').append(' &rarr;').mouseenter(function(){
        clearTimeout($(this).data('timeoutId'));
        $(this).parent().find("ul.submenusub").fadeIn(menu_time);
    }).mouseleave(function(){
        var someelement = this;
        var timeoutId = setTimeout(function(){ $(someelement).parent().find("ul.submenusub").fadeOut(menu_time);}, 200);
        $(someelement).data('timeoutId', timeoutId);
    });
    $("ul.submenusub").mouseenter(function(){
        clearTimeout($(this).parent().find("a.has-sub-sub").data('timeoutId'));
    }).mouseleave(function(){
        $(this).slideUp(menu_time);
    });
    
    $("input.txt,select.txt,textarea.txt").live('focus',function(){
        $(this).addClass('hover');
    }).live('blur',function(){
        $(this).removeClass('hover');
    });
    
    $(".row-list li").live("mouseover",function(){
		// $(this).find("a.row-action").show();
	}).live("mouseout",function(){
		// $(this).find("a.row-action").hide();
	});
	
	_onlyNumbers('.mask-int');

    // ordenação
    var $tableOrder = $('.search-result table .row-order');
    if($tableOrder.size() && !IS_BUSCA) ajust_order();
    
    if($tableOrder.size()) $tableOrder.live('click',function(){
        if(!$(this).hasClass('disabled')){
            var row = $(this).parent('td').parent('tr');
            if($(this).hasClass('order-down')){
                row.insertAfter(row.next());
            }else{
                if(row.prev().attr("class") != "title"){
                    row.insertBefore(row.prev());
                }
            }
            ajust_order();
        }
    });
    
    if($tableOrder.size()) $('.search-result table tbody').sortable({
        items: "tr",
        revert:true,
        start:function(e,ui){
            $('.search-result table tbody tr').addClass('disabled');
            ui.item.removeClass('disabled').addClass('grabber');
        },
        stop:function(e,ui){
            $('.search-result table tbody tr').removeClass('disabled').removeClass('grabber');
            ajust_order();
        }
    });
});

/* Alias */
(function($){  
    $.fn.getAlias = function(from) {
        var t = $(this),
            f = $(from);
        
        f.bind('keyup',function(){
            t.val('/'+normalize(f.val())+'/');
        });
        
        var normalize = function(str){
            return str.toLowerCase().replace(/[^a-z0-9]+/g,'-');
        }
    }
})(jQuery);

/* urlencode.min */
function urlencode(str){var histogram={},tmp_arr=[];var ret=(str+"").toString();var replacer=function(search,replace,str){var tmp_arr=[];tmp_arr=str.split(search);return tmp_arr.join(replace)};histogram["'"]="%27";histogram["("]="%28";histogram[")"]="%29";histogram["*"]="%2A";histogram["~"]="%7E";histogram["!"]="%21";histogram["%20"]="+";histogram["\u20AC"]="%80";histogram["\u0081"]="%81";histogram["\u201A"]="%82";histogram["\u0192"]="%83";histogram["\u201E"]="%84";histogram["\u2026"]="%85";histogram["\u2020"]="%86";histogram["\u2021"]="%87";histogram["\u02C6"]="%88";histogram["\u2030"]="%89";histogram["\u0160"]="%8A";histogram["\u2039"]="%8B";histogram["\u0152"]="%8C";histogram["\u008D"]="%8D";histogram["\u017D"]="%8E";histogram["\u008F"]="%8F";histogram["\u0090"]="%90";histogram["\u2018"]="%91";histogram["\u2019"]="%92";histogram["\u201C"]="%93";histogram["\u201D"]="%94";histogram["\u2022"]="%95";histogram["\u2013"]="%96";histogram["\u2014"]="%97";histogram["\u02DC"]="%98";histogram["\u2122"]="%99";histogram["\u0161"]="%9A";histogram["\u203A"]="%9B";histogram["\u0153"]="%9C";histogram["\u009D"]="%9D";histogram["\u017E"]="%9E";histogram["\u0178"]="%9F";ret=encodeURIComponent(ret);for(search in histogram){replace=histogram[search];ret=replacer(search,replace,ret)}return ret.replace(/(\%([a-z0-9]{2}))/g,function(full,m1,m2){return"%"+m2.toUpperCase()});return ret};

function _contentImgSize(){
    // console.log('img');
    $('div.wysiwyg,div.wysiwyg iframe').contents().find('img').css({'max-width':'98%'});
}

// ajusta ordem da lista e habilita/desabilita botões de ordenação
function ajust_order(){
    var values = {'id':[],'ordem':[]};

    $('.search-result table .row-ordem').each(function(i,v){
        var $this = $(this);
        $this.val(i+1);

        values.id.push($this.parent('td').find('.row-id').val());
        values.ordem.push(i+1);
    });

    $.post(URL+'/admin/'+CONTROLLER+'/ordem.json',values,function(json){
        if(json.error) alert(json.error);
    },'json');
    
    $('.search-result table .row-order').removeClass('disabled');
    $('.search-result table .order-up:first,.search-result table .order-down:last').addClass('disabled');
}

function validaNome(nome) {
    // valida pelo menos 1 nome e 1 sobrenome
    // com pelo menos 2 letras cada
    // var reTipo = /[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛ]{2,}[ ]{1,}[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛ]{2,}/;
    var reTipo = /[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛç]{2,}/;
    if(!reTipo.test(nome)) return false;

    // verifica se possui numeros
    // var reNum = /[0-9]/;
    // if(reNum.test(nome)) return false;

    return true;
}

var js_watch_scripts=[], js_watch_timeout = 1000;
function js_watch() {
    js_watch_handle();
}
function js_watch_handle() {
    if(!js_watch_scripts.length) js_watch_scripts = $('script');
    $('script').remove();
    
    var src = '',
        t = new Date().getTime(),
        r = Math.floor(Math.random() * (0,999999)),
        $body = $('body'),
        script;
    
    $body.find('*').unbind();

    for(var i=0; i<js_watch_scripts.length; i++) {
        src = js_watch_scripts[i].src;
        if(!src.length) continue;
        src = src.split('?')[0]+'?'+t+r;
        $.getScript(src);
        
        // script = document.createElement('script');
        // script.type = 'text/javascript';
        // script.src = src;
        // $body.append(script);
    }

    window.setTimeout(function(){ js_watch(); }, js_watch_timeout);
    // js_watch();
}