var lastChecked = null;

$(document).ready(function(){
    // abas
    head.js(JS_PATH+'/is.simple.tabs.js',function(){
        if($('div.tabs').size()){
            $('div.tabs').isSimpleTabs();
        }
    });

    $('#del-file').click(function(e){
        return confirm('Deseja realmente excluir o arquivo?');
    });

    $("#search-by").change(function(){
        if($(this).val() == "categoria_id"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(categorias){
                for(i in categorias){
                    $combo.append('<option value="'+i+'">'+categorias[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($("#search-txt").attr("type").indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
    });
    
    $("#frm-"+DIR).submit(function(e){
        if($.trim($("#categoria_id").val()).length == 0
          || $.trim($("#categoria_id").val()) == '__none__'){
            alert("Escolha uma categoria.");
            $('a[href*="frm-dados"]','ul.tabs-navigation').click();
            $("#categoria_id").focus();
            return false;
        }
        
        if($.trim($("#titulo").val()).length == 0){
            alert("Preencha o nome do arquivo.");
            $('a[href*="frm-dados"]','ul.tabs-navigation').click();
            $("#titulo").focus();
            return false;
        }
        // if($.trim($("#descricao").val()).length == 0){
        //     alert("Preencha o nome do documento.");
        //     $('a[href*="frm-arquivo"]','ul.tabs-navigation').click();
        //     $("#descricao").focus();
        //     return false;
        // }
        if($.trim($("#clientes").val()).length == 0){
            alert("Insira ao menos um cliente na lista.");
            $('a[href*="frm-clientes"]','ul.tabs-navigation').click();
            $("#buscacli").focus();
            return false;
        }

        return true;
    });
    
    // $("input[type=submit].bt").click(function(e){
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });

    // busca de clientes
    $('#frm-clientes-search').submit(function(e){
        e.preventDefault();
        return false;
    });

    $('#catcli').change(function(e){
        var catcli = $(this).val();
        $('input[name=check_all]').attr('checked',false);

        if(catcli=='__none__') return $('#clientes-result li').removeClass('hidden').show();
        $('#clientes-result li').addClass('hidden').hide();
        $('#clientes-result li.cat-'+catcli).removeClass('hidden').show();
    });

    var pesquisa = {
        selector:"#clientes-result li:not(.hidden)",
        field:".cliente"
    };
    $('#buscacli').keyup(function(event) {
        if (event.keyCode == 27) { //if press Esc
            $(this).val("").blur();
        }
        if(event.keyCode == 27 || $.trim(this.value) == ''){ //if press Esc or no text
            $(pesquisa.selector).removeClass('visible').show().addClass('visible');
        } else { //if there is text, lets filter
            $('input[name=check_all]').attr('checked',false);
            filter(pesquisa.selector,pesquisa.field,$(this).val());
        }
    });

    // handler das listas de clientes
    $('.del','#clientes-list').live('click',function(e){
        ListaCliente.del($(this).data('id'));
    });
    $('.chk-cli','#clientes-result').change(function(e){
        var $this = $(this);
        if(this.checked)
            ListaCliente.add($this.data('id'),$this.data('nome'));
        else
            ListaCliente.del($this.data('id'));
    });

    // funções de check para exclusão ------------------------//
    // var $chkboxes = $('input.chk-cli');

    // seleciona todos os checkbox
    $('input[name=check_all]').change(function(e){
        var $this = $(this),
            checked = $this.attr('checked');
        
        // _checkAll(checked);
        ListaCliente.checkAll(checked);
    });
    $('.del-all').click(function(e){
        // ListaCliente.checkAll(false);
        $('input[name=check_all]').attr('checked',false).trigger('change');
    });
    window.setTimeout(function(){
        ListaCliente.update();
    },100);
});

function _checkAll(checked) {
    $('.chk-cli').each(function(i,elm){
        elm.checked = checked;
        $(elm).trigger('change');
    });
}

function filter(selector,field,query) {
    query = $.trim(query);
    //query = query.replace(/ /gi, '|'); //add OR for regex
  
    $(selector).each(function() {
        (($(this).find(field).val() ? $(this).find(field).val().search(new RegExp(query, "i")) : $(this).find(field).text().search(new RegExp(query, "i"))) < 0) ?
            $(this).hide().removeClass('visible') :
            $(this).show().addClass('visible');
    });
}

var ListaCliente = {
    checkAll: function(checked){
        // $('#clientes-list li').remove();
        // $('.chk-cli').attr('checked',false);
        $('.chk-cli:visible').each(function(i,elm){
            elm.checked = checked;
            $(elm).trigger('change');
        });
    },
    add: function(cid,nome){
        $('#clir-'+cid+' .chk-cli').attr('checked',true);
        if(!document.getElementById('cli-'+cid))
            $("#tmplRowCliente").tmpl({
                'id': cid,
                'nome': nome
            }).appendTo("#clientes-list");
        ListaCliente.update();
    },
    del: function(cid){
        $('#cli-'+cid).remove();
        // $('#cli-'+cid).fadeOut(function(){ $(this).remove(); });
        $('#clir-'+cid+' .chk-cli').attr('checked',false);
        $('input[name=check_all]').attr('checked',false);
        ListaCliente.update();
    },
    hideList: function(){ $('#clientes-list-wrapper,.clititlist').hide(); },
    showList: function(){ $('#clientes-list-wrapper,.clititlist').show(); },
    update: function(){
        var lista = [];

        $('#clientes-list li .del').each(function(i,elm){
            var $this = $(elm), id = $this.data('id');
            lista.push(id)
        });

        // if(!lista.length) 
        //     ListaCliente.hideList();
        // else if($('#clientes-list-wrapper').is(':hidden')) 
        //     ListaCliente.showList();
        
        $('#clientes').val(lista.join(','));

        (lista.length<=0) ?
            $('.del-all').hide():
            $('.del-all').show();
    }
};