var MAX_FOTOS = 1, MAX_SIZE = 5242880, allow_photos,
    PATT_IMG  = /.*(jpeg|jpg|png|gif|bmp|mp4)$/i;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();
    
    $("#frm-"+DIR).submit(function(e){
        // if($.trim($("#titulo").val()).length == 0){
        //     $("#titulo").focus();
        //     Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
        //     return false;
        // }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });

    window.setTimeout(function(){
        Destaques.checkTipo();
    },ENV_DEV ? 500 : 100);
    $('input[name=tipo]').change(function(e){
    // $('#tipo').change(function(e){
        Destaques.checkTipo();
    });
});

var Destaques = {};
Destaques.checkTipo = function() {
    var $tipo = $('input[name=tipo]:checked'),
    // var $tipo = $('#tipo'),
        tipo = $tipo.val();

    if(tipo==1) Destaques.hideVideo();
    else Destaques.showVideo();
}
Destaques.showVideo = function() {
    $('li.li-video').show();
    $('.content-right #fotos').hide();
}
Destaques.hideVideo = function() {
    $('li.li-video').hide();
    $('.content-right #fotos').show();
}



