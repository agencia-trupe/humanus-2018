/*global $ */
$(document).ready(function () {
    $('#file_upload2').fileUploadUI({
        uploadTable: $('#file_upload2_list'),
        downloadTable: $('#paginas_arquivos'),
        buildUploadRow: function (files, index) {
            //alert(files[index].size);
			//return $('<tr><td class="filename">' + files[index].name + '<\/td>' +
            return $('<tr>'+
					//'<td class="filename file_upload_preview"><\/td>' +
					'<td class="file_upload_progress"><div></div></td>' +
					'<td class="filesize">'+Files2.formatBytes(files[index].size)+'</td>' +
                    '<td class="file_upload_cancel">' +
                    '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                    '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                    '<\/button>' +
					'<\/td>' +
					//'<td class="file_delete">' +
					//'<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
					//'<span class="ui-icon ui-icon-trash"><\/span>' +
					//'<\/button>' +
					//'<\/td>' +
					'<\/tr>');
        },
        buildDownloadRow: function (file) {
            //return $('<tr><td>' + file.name + '<\/td><\/tr>');
            if(file.error){
				return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
			} else {
                var n = file.descricao || noname;

				return $('<li>'+
                         '<a href=\''+URL+'/public/files/'+DIR2+'/'+file.name+'\' class=\'file-link\' title=\'Baixar arquivo\'>'+
                         '&nbsp;</a>'+
						 '<input type="text" class="arquivo_descricao" name="descricao[]" value="'+n+'"  title="Alterar nome do arquivo (Tecle [Enter] para salvar)" />'+
                         '<input type="hidden" class="arquivo_descricao_original" name="descricao_original[]" value="'+n+'" />'+
						 '<button title="Delete" class="ui-state-default ui-corner-all file-delete2">' +
						 '<span class="ui-icon ui-icon-trash"><\/span>' +
						 '<\/button>' +
						 '<input type="hidden" name="arquivo_id[]" value="'+file.id+'" class="arquivo_id" />' +
						 '<\/li>');
			}
        },
		beforeSend: function(event, files, index, xhr, handler, callBack){
			$('#file_upload2_list tr.error').remove();
            
            var patt_img  = /.*(gif|jpeg|png|jpg)$/i,///(jpeg|jpg|png|gif|bmp)/gi,
                file_name = files[index].name,
                max_size  = (MAX_SIZE)  ? MAX_SIZE  : 2097152,
                max_files = (MAX_FILES) ? MAX_FILES : 0,
                file_count= $(".list_files li").size() + $('#file_upload2_list tr').size(),
                max_count = (max_files>0) ? file_count <= max_files : true,
                msg       = "Erro ao enviar arquivo";
            
            if(max_count){
                callBack();
                return true;
            }
            
            // msg = !patt_img.test(Files.getExt(file_name)) ? "Imagem inválida" : msg;
            // msg = files[index].size >= max_size ? "Tamanho máx. permitido: "+Files.formatBytes(max_size) : msg;
            msg = !max_count ? "Você pode enviar no máximo "+max_files+" arquivo(s)" : msg;
            
            handler.cancelUpload(event, files, index, xhr, handler);
            handler.addNode($('#file_upload2_list'),
                $('<tr class="error"><td colspan="3">'+msg+' ('+file_name+')</td></tr>')
            );
            //callBack();
		},
		onComplete: function (event, files, index, xhr, handler) {
			// var json = handler.response;
			//Gallery.show();
		}
    });
    
    $("button.file-delete2").live('click',function(){
        var row = $(this).parents("li");
		Files2.del(row);
    });
    $("button.file-insert").live('click',function(){
        var row = $(this).parents("li");
		Files2.insert(row);
    });
    
    $("#files tr").mouseover(function(){
        $(this).addClass("hover");
    }).mouseout(function(){
        $(this).removeClass("hover");
    });
    
    $("#paginas_arquivos input.arquivo_descricao").live("focus",function(){
        if($(this).val() == noname){ $(this).val(""); }
        $(this).css("border","1px solid #ccc");
    }).live("blur",function(){
        if($(this).val() == ""){ $(this).val(noname); }
        $(this).css("border","1px solid #fff");
    }).live("keydown",function(e){
        if(e.which == 13 || e.which == 27){
            e.preventDefault();
            var row = $(this).parents("li");
            if(e.which == 13){
                Files2.rename(row);
            } else {
                $(this).val(row.find('.arquivo_descricao_original').val()).blur();
            }
        }
    });
});

Files2 = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },
    
    del: function(row,dir){
        dir = dir || false;
        var url = URL+"/admin/"+DIR+"/arquivos-del.json?file="+row.find("input.arquivo_id").val();

        if(confirm("Deseja deletar o arquivo selecionado?")){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
    },
    
    rename: function(row){
        var desc   = row.find("input.arquivo_descricao"),
            desc_o = row.find("input.arquivo_descricao_original"),
            id     = row.find("input.arquivo_id");
        var url = URL+"/admin/"+DIR+"/arquivos-rename.json?file="+id.val()+"&descricao="+urlencode(desc.val());
        
        desc.attr('disabled',true).css('background-color','#eee');
        
        $.getJSON(url,function(data){
            if(data.error){
                alert(data.error);
            } else {
                desc_o.val(desc.val());
                desc.blur();
                Msg.add("O arquivo foi renomeado.").show(null,3000);
                //alert('O arquivo foi renomeado');
            }
            desc.attr('disabled',false).css('background-color','#fff');
        });
    },
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}
