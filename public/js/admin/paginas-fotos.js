var MAX_FOTOS = MAX_FOTOS || 1, MAX_SIZE = 5242880, allow_photos=false;

/*global $ */
$(document).ready(function () {
	fileUploadOpts = {
        uploadTable: $('#file_upload_list'),
        downloadTable: $('#paginas_fotos')
    };
    
    fileUploadOpts.buildUploadRow = function (files, index) {
        //alert(files[index].size);
		//return $('<tr><td class="filename">' + files[index].name + '<\/td>' +
        return $('<tr>'+
				//'<td class="filename file_upload_preview"><\/td>' +
				'<td class="file_upload_progress"><div></div></td>' +
				'<td class="filesize">'+Files.formatBytes(files[index].size)+'</td>' +
                '<td class="file_upload_cancel">' +
                '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                '<\/button>' +
				'<\/td>' +
				//'<td class="file_delete">' +
				//'<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
				//'<span class="ui-icon ui-icon-trash"><\/span>' +
				//'<\/button>' +
				//'<\/td>' +
				'<\/tr>');
    };

    fileUploadOpts.buildDownloadRow = function (file) {
        //return $('<tr><td>' + file.name + '<\/td><\/tr>');
        if(file.error){
			return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
		} else {
			var hasLink = fileUploadOpts.downloadTable.hasClass('has-link'),
				hasOrder = fileUploadOpts.downloadTable.hasClass('order_fotos'),
				hasParam = hasLink;
			console.log(hasLink);
			return $('<li><a href=\'' + URL+'/img/'+DIR+":"+file.name + '\' rel=\'gallery_group\'>'+
					 '<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=90&h=90\'><\/a>'+
					 
					 ((hasLink) ? 
					 	'<input type="text" name="foto_link[]" class="txt foto_link" id="foto_link_'+file.id+'" value="" data-id="'+file.id+'" data-name="param" />' :
					 	'')+

					 '<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
					 '<span class="ui-icon ui-icon-trash"><\/span>' +
					 '<\/button>' +
					 // '<button title="Inserir esta imagem no texto" class="ui-state-default ui-corner-all file-insert">' +
					 // '<span class="ui-icon ui-icon-arrowthick-1-w"><\/span>' +
					 // '<\/button>' +
					 
					 ((hasParam) ? 
					 	'<input type="hidden" name="foto_id[]" value="'+file.id+'" class="foto_id" data-name="id" /><span class="foto_status"></span>' :
				        '<input type="hidden" name="foto_id[]" value="'+file.id+'" class="foto_id" />') +
					 
					 ((hasOrder) ?
					 '<input type="hidden" name="foto_ordem[]" value="1" class="foto_ordem" />' :
					 '') +
					 
					 '<\/li>');
		}
    };
	
	fileUploadOpts.beforeSend = function(event, files, index, xhr, handler, callBack){
		$('#file_upload_list tr.error').remove();
		
		var patt_img  = /.*(gif|jpeg|png|jpg)$/i,///(jpeg|jpg|png|gif|bmp)/gi,
			file_name = files[index].name,
			max_size  = (MAX_SIZE)  ? MAX_SIZE  : 2097152,
			max_files = (MAX_FOTOS) ? MAX_FOTOS : 0,
			file_count= $(".list_fotos li").size() + $('#file_upload_list tr').size(),
			max_count = (max_files>0) ? file_count <= max_files : true,
			msg       = "Erro ao enviar imagem";
		//console.log(file_name+","+Files.getExt(file_name)+","+patt_img.test(Files.getExt(file_name)));
		//console.log(files[index].size < max_size);
		//console.log(max_count);
		//return false;
		if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size && max_count){
			callBack();
			return true;
		}
		
		msg = !patt_img.test(Files.getExt(file_name)) ? "Imagem inválida" : msg;
		msg = files[index].size >= max_size ? "Tamanho máx. permitido: "+Files.formatBytes(max_size) : msg;
		msg = !max_count ? "Você pode enviar no máximo "+max_files+" fotos" : msg;
		
		handler.cancelUpload(event, files, index, xhr, handler);
		handler.addNode($('#file_upload_list'),
			$('<tr class="error"><td colspan="3">'+msg+' ('+file_name+')</td></tr>')
		);
		//callBack();

		/* OLD
		var patt_img  = /.*(gif|jpeg|png|jpg)$/;///(jpeg|jpg|png|gif|bmp)/gi,
			file_name = files[index].name,
			max_size  = 2213814;
		
		if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size){
			callBack();
		} else {
			var msg = !patt_img.test(Files.getExt(file_name)) ?
					  "Imagem inválida" :
					  "Tamanho máx. permitido: "+Files.formatBytes(max_size);
			
			handler.cancelUpload(event, files, index, xhr, handler);
			handler.addNode($('#file_upload_list'),
				$('<tr><td colspan="4">'+msg+' ('+file_name+')</td></tr>')
			);
			//callBack();
		}*/
	};
	
	fileUploadOpts.onComplete = function (event, files, index, xhr, handler) {
		// var json = handler.response;
		Gallery.show();

		if(fileUploadOpts.downloadTable.hasClass('has-link')) _prevalueLinks();
		var hasOrder = fileUploadOpts.downloadTable.hasClass('order_fotos');
		if(hasOrder) ajust_order_fotos();
	};

	$('#file_upload').fileUploadUI(fileUploadOpts);
    
    $("button.file-delete").live('click',function(){
        var row = $(this).parents("li");
		Files.del(row);
    });
    $("button.file-insert").live('click',function(){
        var row = $(this).parents("li");
		Files.insert(row);
    });
    $("button.save-all").live('click',function(){
        var row = $(this).parents("li");
		Files.saveAll(row);
    });
    
    $("#files tr").mouseover(function(){
        $(this).addClass("hover");
    }).mouseout(function(){
        $(this).removeClass("hover");
    });

    // links
	_prevalueLinks();

    $('input.foto_link').live('keyup',function(e){
        if(e.which==13) Files.saveAll($(this).parents('li'));
    });

    // ordenacao de galeria de fotos
    var $order_fotos = $('.list_fotos.order_fotos');
    if($order_fotos.length){
	    ajust_order_fotos();
	    $order_fotos.sortable({
	        items: "li",
	        revert:true,
	        start:function(e,ui){
	        	var $rows = $order_fotos.find('li');
	            $rows.addClass('disabled');
	            ui.item.removeClass('disabled').addClass('grabber');
	        },
	        stop:function(e,ui){
	        	var $rows = $order_fotos.find('li');
	        	$rows.removeClass('disabled');//.removeClass('grabber');
	        	window.setTimeout(function(){
	        		$rows.removeClass('grabber');
	        	},500);
	            ajust_order_fotos();
	        }
	    });
	}
});

// ajusta ordem da lista e habilita/desabilita botões de ordenação
function ajust_order_fotos(){
    var values = {'id':[],'ordem':[]};

    $('.list_fotos.order_fotos .foto_ordem').each(function(i,v){
        var $this = $(this);
        $this.val(i+1);

        values.id.push($this.parent('li').find('.foto_id').val());
        values.ordem.push(i+1);
    });

    $.post([URL,'admin',CONTROLLER,'ordem-fotos.json'].join('/'),values,function(json){
        if(json.error) alert(json.error);
    },'json');
    
    $('.list_fotos.order_fotos li').removeClass('disabled');
}

function _prevalueLinks(){
	$('input.foto_link').each(function(i,v){
        var $t = $(this);
        if($.trim($t.val())=='') $t.prevalue('URL...');
    });
}

Files = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },
    
    saveAll: function(row){
    	var data         = {},
    		inputs       = row.find('input'),
    		selects      = row.find('select'),
    		url          = URL+'/admin/'+DIR+'/'+'save-all.json',
            status       = row.find('.foto_status');
    		invalid_vals = [
    						'',
    						'...',
			    			'título...',
			    			'descrição...',
			    			'selecione...',
			    			'url...'
			    		];

    	inputs.each(function(){
    		var $t = $(this);
    		if($t.data('name') && $.inArray($.trim($t.val().toLowerCase()),invalid_vals)==-1)
    			data[$t.data('name')] = $.trim($t.val());
    	});

    	selects.each(function(){
    		var $t = $(this);
    		if($t.data('name') && $.inArray($t.val().toLowerCase(),invalid_vals)==-1)
    			data[$t.data('name')] = $t.val();
    	});

        status.html('Aguarde...').show();
        
        $.post(url,data,function(json){
            if(json.error){
                alert(json.error);
                status.html('').hide();
                return false;
            }
            
            status.html(json.msg||'Concluído').delay(3000).fadeOut('slow');
        },'json');
    },

    del: function(row,dir){
        dir = dir || false;
        var url = URL+"/admin/"+DIR+"/fotos-del.json?file="+row.find("input.foto_id").val();
        
        if(confirm("Deseja deletar a foto selecionada?")){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
    },
	
	insert: function(row){
		var path = row.find('a').attr('href');
		$('textarea.wysiwyg').wysiwyg('insertImage', path);
	},
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}
