// head.js(JS_PATH+'/meu-cadastro.js');
$.getScript(JS_PATH+'/meu-carrinho.js');

$.getScript(JS_PATH+'/Cep.js',function(){
    $('#cep').blur(function(){
        Cep.search($(this).val(),function(){
            $('.campos-cep').removeClass('hidden');
            if($('#logradouro').val().length > 0) $('#numero').focus();
        });
    }).on("keyup",function(e){
        if(e.which == 13){ // [ENTER]
            e.preventDefault();
            $(this).blur();
        }
        if($(this).val().length == 9){ // terminou de digitar
            $(this).blur();
        }
    });
});

$.getScript(JS_PATH+'/jquery.mask.min.js',function(){
	$(".mask-cpf").mask("999.999.999-99", {clearIfNotMatch: true});
	$(".mask-cnpj").mask("99.999.999/9999-99", {clearIfNotMatch: true});
	$(".mask-cep").mask("99999-999", {clearIfNotMatch: true});
	$(".mask-tel").mask("(99)9999-9999", {clearIfNotMatch: true});
	$(".mask-cel").mask("(99)09999-9999", {clearIfNotMatch: true});
});

// bloqueia envio se cep n estiver preenchido
// ou nome for inválido
$('.dados-pessoais-form, .dados-pagamento-form').submit(function(e){
    var $this = $(this),
        $nome = $this.find('input[name=nome]');
    if((ACTION=='index2' ||
        ACTION=='index31' ||
        ACTION=='index32' ||
       (MODULE=='portal' && CONTROLLER=='index')) &&
       !validaNome($nome.val())){
        e.preventDefault();
        alert('Campo Nome inválido');
        $nome.focus();
        return false;
    }

    var $cep = $('#cep');
    if($cep.size()) if($cep.hasClass('hidden')) {
        e.preventDefault();
        if($cep.val()!='') $cep.blur();
        return false;
    }

    if($this.hasClass('empresa-admin')) {
        // e.preventDefault();
        var noModify = ['email','id','senhaa','senha','senhac'],
            name = '', $elm;
        $this.find('input,select,textarea').each(function(i,elm){
            $elm = $(elm);
            name = $elm.attr('name');
            if(noModify.indexOf(name)==-1) $elm.attr('name','empresa['+name+']');
        });
        // return false;
    }
});

// confirma inscrição (passo final)
var $formConfirmaInscricao = $('#form-inscricao-passo3');

if($formConfirmaInscricao.length){
    $formConfirmaInscricao.find('.form-submit').click(function(e){
        e.preventDefault();
        return false;
    });

    $formConfirmaInscricao.submit(function(e){
    	if($('input[name=responsavel-pagamento]').size()) if(!$('input[name=responsavel-pagamento]').is(':checked')){
    		e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
			alert('Escolha um responsável pelo pagamento');
			return false;
    	}

        var $termos = $('#termos');
    	if($termos.length) if(!$('#termos').is(':checked')){
    		e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
			alert('Você precisa aceitar os Termos e condições para prosseguir');
			return false;
    	}

    	// $('#confirma-dados').slideUp();
    	// $('#form_pagseguro').addClass('open');
    	return true;
    });
}