var fbLoaded = false, cycleLoaded = false, scrolltoLoaded = false, intLess,
    windowWidth, isDesktop, isMobile, isTablet, isSmartphone,
    _menuEventOver = 'mouseenter', _menuEventOut = 'mouseleave',
    vmplayer = vmplayer || null,
    vdplayer = vdplayer || null,
    ytplayer = ytplayer || null,
    ifplayer = ifplayer || null;

var Site = {};
Site.index = function() {
    Site.slideDeps();
    Site.slideAgenda();
    Site.slideBanner();

    // if($('.banner').hasClass('video')) {
    //     window.setTimeout(function(){
    //         vidRescale();
    //     },500);
    // }
}
Site.sejahumanus = function() {
    Site.menuScroll({
        'missao-visao-valores' : 'pagina2',
        'reconhecimento' : 'pagina3',
        'historia' : 'pagina1',
        'projeto' : 'pagina4'
    });
}
Site.clientesIndex = function(){
    Site.menuScroll({
        'nossos-clientes' : 'clientes',
        'cases' : 'pagina-cases',
        'diferenciais' : 'diferenciais'
    });
    
    head.js(JS_PATH+'/salvattore.min.js',function(){
        
    });
}
Site.clientesNossosclientes = function(){ Site.clientesIndex(); }
Site.clientesCases = function(){ Site.clientesIndex(); }
Site.clientesDiferenciais = function(){ Site.clientesIndex(); }
Site.solucoesIndex = function(){
    var scrolls = {
        'produtos': 'produtos',
        // 'consultoria-empresarial': 'pagina-1',
        // 'centro-de-capacitacao': 'pagina-2',
        'webinars': 'pagina-3'
    };
    $('.solucao-servico,.solucao-webinar').each(function(i,elm){
        var $this = $(this);
        scrolls[$this.data('alias')] = $this.attr('id').replace('solucao','pagina');
    });
    Site.menuScroll(scrolls);
}
Site.solucoesConsultoriaempresarial = function(){ Site.solucoesIndex(); }
Site.solucoesCentrodecapacitacao = function(){ Site.solucoesIndex(); }
Site.solucoesProdutos = function(){ Site.solucoesIndex(); }
Site.solucoesProduto = function(){
    head.js(JS_PATH+'/salvattore.min.js',function(){
        
    });
}

Site.slide = function(elm,time,config2){
    if(typeof(time)=='undefined') time = 7000;
    
    var config = {
        slides: '> .slide',
        pager: '#pager',
        prev: '#pager-prev',
        next: '#pager-next',
        pagerTemplate: '<a href="#">{{slideNum}}</a>',
        fx: 'scrollHorz',
        autoHeight: 'calc',
        timeout: time,
        speed: 1200,
        manualSpeed: 1200
    };

    if(!!config2) $.extend(config,config2);

    _loadCycle(function(){
        $(elm).cycle(config);

        // add swipe on mobile
        if(isMobile) js('hammer.min',function(){
            Hammer($('#eventos').get(0)).on('swipeleft',function(e){
                $(elm).cycle('next');
            }).on('swiperight',function(e){
                $(elm).cycle('previous');
            });
        });
    });
}
Site.slideAgenda = function(){
    var slideLength = $('.slide-eventos .slide').length;
        hasSlide = (slideLength > ((isSmartphone)?1:2));
    
    if(hasSlide) return Site.slide('.slide-eventos',0);
    $('.pager-prev, .pager-next', '#eventos').remove();
}
Site.slideBanner = function(){
    // if(isMobile) $('.banner.video').remove();

    var hasSlide = $('#banner-wrapper .slide').length,
        hasVideo = $('#banner-wrapper .slide.video').length,
        slideSpeed = ENV_DEV ? 2000 : 5000;
    return (hasSlide <= 1) ? true : Site.slide('#banner-wrapper',slideSpeed,{
        autostop: isMobile||!hasVideo ? 0 : 1,
        after: function(currSlideElement, nextSlideElement, options, forwardFlag){
            var isVideo = $(nextSlideElement).hasClass('video');
            if(isVideo) window.setTimeout(function(){
                vidRescale();
            },ENV_DEV ? 100 : 100);
        },
        fx: 'fade'
    });
}

Site.slideDeps = function(time) {
    var depCount = $('#depoimentos-list .depoimento').size(),
        depTimeout = time || 20000;

    $('#depoimentos-list > li:gt(0)').hide();
    if(depCount<=1 || APPLICATION_ENV=='development1') return;

    setInterval(function(){
        $('#depoimentos-list > li:first')
            .fadeOut(1000)
            .next().delay(1000)
            .fadeIn(1000)
            .end()
            .appendTo('#depoimentos-list');

    }, depTimeout);
    
}

Site.menuScroll = function(scrolls,controller) {
    var c = controller || CONTROLLER;
    
    _loadScrollTo(function(){
        if(PAGINA_ATIVA) setTimeout(function(){
            $.scrollTo($('#'+scrolls[PAGINA_ATIVA]),500,{
                axis:'y'
            });
        },700);
        
        var s;
        $('#top-menu ul.submenu a').click(function(e){
            for(s in scrolls) {
                if(this.href.indexOf(s)!=-1){
                    e.preventDefault();
                    // console.log(scrolls[s]);
                    // return false;
                    
                    $.scrollTo($('#'+scrolls[s]),500,{
                        axis:'y'
                    });

                    return false;
                }
            }
        });
    });
}


var Accordion = {
    wrapper: $('.accordion-wrapper'),

    open: function(elm){
        elm.addClass('open').addClass('active');
        $(elm.attr('href')).slideDown();
    },

    close: function(elm){
        elm.removeClass('open').removeClass('active');
        $(elm.attr('href')).slideUp();
    },

    closeAll: function(){
        if(this.wrapper.hasClass('accordion-keep-opens')) return;
        $('.accordion-item',this.wrapper).removeClass('open').removeClass('active');
        $('.accordion-content',this.wrapper).slideUp();
    },

    init: function(){
        var _acc = this;
        
        this.wrapper.find('.accordion-item').each(function(i,elm){
            var $elm = $(elm);
            $elm.click(function(e){
                e.preventDefault();
                
                if($elm.hasClass('open')||$elm.hasClass('active'))
                    return _acc.close($elm);

                _acc.closeAll();
                _acc.open($elm);
            });
        });
    }
}

$(document).ready(function(){
    if(APPLICATION_ENV == 'development') head.js(JS_PATH+'/less.js',function(){
        // less.watch();
        intLess = window.setInterval(function(){
            localStorage.clear();
            less.refresh();
        },1500);
    }); // load less in dev
    if(APPLICATION_ENV=='development') head.js(JS_PATH+'/tests.js');

    checkMobile();

    // ativa menus via javascript
    var i, j, menus_ativar = {
        'top-menu' : {
            '/contato/' : 1
        }
    };
    for(j in menus_ativar)
        for(i in menus_ativar[j])
            if(location.href.indexOf(i)!=-1){
                var $li = $('#'+j+' ul li').eq(menus_ativar[j][i]);
                $li.addClass('active').find('a').addClass('active');
            }
    var i, j, menus_ativar = {
        'top-menu-social' : {
            '/login' : 0,
            '/portal' : 0
        }
    };
    for(j in menus_ativar)
        for(i in menus_ativar[j])
            if(location.href.indexOf(i)!=-1){
                var $li = $('#'+j+' a').eq(menus_ativar[j][i]);
                $li.addClass('active');
            }

    $('#mobile-menu').click(function(e){
        $('#top-navs').toggleClass('show');
    });
    
    // máscaras padrões
    $("input.mask-cpf").mask("999.999.999-99");
    $("input.mask-cnpj").mask("99.999.999/9999-99");
    $("input.mask-cep").mask("99999-999");
    $("input.mask-data,input.mask-date").mask("99/99/9999");
    $("input.mask-hour").mask("99:99");
    $("input.mask-hours").mask("99:99:99");
    $("input.mask-datetime").mask("99/99/9999 99:99:99");
    $("input.mask-ddd").mask("99");
    $("input.mask-tel").mask("(99)9999-9999");
    $("input.mask-cel").mask("(99)9999-9999?9");
    $("input.mask-tel-no-ddd").mask("9999-9999");
    $("input.mask-currency").maskCurrency();

    $('[autoselect]').live('focus',function(){ this.select(); });
    
    $(".prevalue").each(function(){ $(this).prevalue(); }); // prevalue em campos com a class .prevalue
    // $(".prevalue").prevalue(); // prevalue em campos com a class .prevalue
    
    $("a[href^='#']").live("click",function(e){ // remove click de links vazios (que começam por #)
        e.preventDefault();
    });
    $("a.js-back").live("click",function(e){ // adiciona ação de voltar a links com a classe js-back
        history.back();
    });
    
    if($(".pagination").size()){ // se tiver paginação
        $(".pagination .navigation.left").html("&laquo;").attr('title','Página anterior'); // arruma texto da navegação
        $(".pagination .navigation.right").html("&raquo;").attr('title','Próxima página');
        if($(".pagination a").size() <= 1){
            $(".pagination").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }
    
    if($(".paginacao").size()){ // se tiver paginação
        if($(".paginacao a").size() < 1){
            $(".paginacao").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }

    if(Accordion.wrapper.size()) Accordion.init();
    
    if(SCRIPT) head.js(JS_PATH+'/'+SCRIPT+'.js'); // carrega scripts de páginas
    
    var CONTROLLER2 = CONTROLLER.replace(/-/g,''),
        ACTION2 = ACTION.replace(/-/g,''),
        ACTION3 = ACTION2.charAt(0).toUpperCase()+ACTION2.slice(1);
    if(typeof(Site[CONTROLLER2])==='function') Site[CONTROLLER2]();
    if(typeof(Site[CONTROLLER2+'_'+ACTION2])==='function') Site[CONTROLLER2+'_'+ACTION2]();
    if(typeof(Site[CONTROLLER2+ACTION3])==='function') Site[CONTROLLER2+ACTION3]();

    // newsletter
    $('.newsletter-form').submit(function(e){
        e.preventDefault();
        var $form = $(this), $status = $('.newsletter-form .status').removeClass('error').html('Enviando...');
        
        $.post(URL+'/index/newsletter.json',$form.serialize(),function(json){
            if(json.error) $status.addClass('error');
            $status.html(json.message);
        },'json');
    });
    
    // busca
    var b = $("#busca");
    $('#frm-busca').submit(function(e){
        e.preventDefault();
        
        if(b.val() == '' || b.val() == b.data('prevalue') || b.val() == b.attr('placeholder')){
            alert2('Escreva algum produto para buscar');
            b.focus();
        } else {
            window.location = URL+'/busca/'+b.val();
        }
        
        return false;
    });

    // habilitando zoom no mapa ao clicar
    $('#mapa').click(function () {
        $('#mapa iframe').css("pointer-events", "auto");
    });

    $( "#mapa" ).mouseleave(function() {
        $('#mapa iframe').css("pointer-events", "none"); 
    });
    
    // flash-messages
    if($("#flash-messages").size() && CONTROLLER!='admin'){
        var $flash = $("#flash-messages");
        var y = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
        $flash.css({top:y+10}).delay(1000).show().live('click',function(){
            $(this).fadeOut("slow");
        });
    }
    
    // scroll to
    var $scrollTos = $('a.scroll-to');
    if($scrollTos.length){
        head.js(JS_PATH+'/jquery.scrollTo-min.js',function(){
            $scrollTos.live('click',function(e){
                var $this = $(this);
                $.scrollTo($($this.attr('href')),1000,{axis:'y'});
            });
        });
    }

    // fancyboxes
    var $linksVideosFBox = $('.video-fancybox');
    if($linksVideosFBox.length) {
        _loadFancybox(function(){
            $linksVideosFBox.fancybox({
                maxWidth    : 950,
                maxHeight   : 600,
                fitToView   : false,
                width       : '80%',
                height      : '80%',
                autoSize    : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none',
                type: 'iframe'
            });
        });
    }

    // checando features
    if(!head.placeholder){
        var $elm;

        $('[placeholder]').each(function(){
            $elm = $(this);

            $elm.addClass('prevalue')
                .data('prevalue',$elm.attr('placeholder'))
                .prevalue(); 
        });
    }
    
    if(!head.number){
        var $elm;

        $('input[type=number]').each(function(){
            $elm = $(this);

            $elm.addClass('prevalue').addClass('mask-int')
                .data('prevalue',$elm.attr('placeholder'))
                .prevalue();

            _onlyNumbers('.mask-int');
        });
    }

    // old ie
    if($.browser.msie && $.browser.version < 9){
        // old ie hacks
    }

    $('.input-number').live('blur',function(){
        var $this = $(this),
            val = Number($this.val());

        if($this.attr('max')) {
            var max = Number($this.attr('max'));
            
            if(max < val) {
                alert2('O valor está acima do permitido ('+max+')');
                window.setTimeout(function(){$this.focus();},100);
            }
        }

        if($this.attr('min')) {
            var min = Number($this.attr('min'));

            if(min > val) {
                alert2('O valor está abaixo do permitido ('+min+')');
                window.setTimeout(function(){$this.focus();},100);
            }
        }
    });

    $(window).resize(function(e){
        checkMobile();
    });

});

// Color Hex <-> RGB
function hexToR(h){return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h){return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h){return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h){return (h.charAt(0)=="#") ? h.substring(1,7):h}
function hexToRGB(h){return {'R':hexToR(h),'G':hexToG(h),'B':hexToB(h)}}
function RGBToHex(r,g,b){var dec = r+256*g+65536*b;return dec.toString(16);}

// Math
function getRand(n){n=n||0;return Math.floor(Math.random()*(n+1));}

// ValidateForm
function validadeForm(f){var $f=$(f),err=0;$f.find("input").each(function(i,v){if($(this).data("validate")&&$.trim($(this).val())==""){alert2($(this).data("errmsg"));$(this).focus();err++;return false}});if(err==0){$f.submit()}};

// Currency
function currency(val){return 'R$ '+(val+'').replace('.',',');}

// valida campo vazio
function isEmpty(elm) {
    var v = $.trim(elm.val());

    return v=='' || v==elm.data('prevalue') || v=='__none__';
}

// limpa form
function _resetForm(formSelector){
    $(formSelector).find('input[type=text],input[type=tel],input[type=email],textarea').val('');
    $(formSelector).find('select option:first-child').attr('selected',true);
}

// helper console.log
function _log(log) {
    if(typeof(window.console)=='undefined'){
        return alert(log);
    }

    return console.log(log);
}
function _d(log){ _log(log); } // alias para _log()

// 9 digitos
function digito9(elm,options){
    var ddd9digit = ['11','12','13','14','15','16','17','18','19'], // ddd's que precisam de 9 dig
        tp9digit  = ['cel'], // tipos que precisam de 9 dig
        dddEmpty = ['','__','ddd: *','ddd','ddd:'],
        $this = $(elm),
        opts = {
            'premask' : false,
            'ddd'     : 'input.mask-ddd',
            'tipotel' : 'select.tipotel'
        },
        opts2 = options || {};
        

    var opt = $.extend(opts,opts2);
    
    var dddField      = $(opt.ddd),
        ddd           = dddField.val(),
        tipotelField  = opt.tipotel ? $(opt.tipotel) : null,
        tipotel       = tipotelField ? tipotelField.val() : 'cel';
    
    // função que checa se precisa adicionar o dig
    var has9digit = function(){
        if($.inArray(dddField.val(),ddd9digit)!=-1){
            if(tipotelField){
                if($.inArray(tipotelField.val(),tp9digit)!=-1){
                    return true;
                }
            } else {
                if($.inArray(tipotel,tp9digit)!=-1){
                    return true;
                }
            }
        }
            
        return false;
    };

    if(opt.premask) $this.mask(has9digit()?'99999-9999':'9999-9999');

    $this.live('focus',function(){
            // console.log([$.trim(dddField.val().toLowerCase()),opt.tipotel]);
            $this.unmask();

            if($.inArray($.trim(dddField.val().toLowerCase()),dddEmpty)!=-1) {
                    $this.blur();
                    dddField.focus();
                    alert2('Digite seu DDD');
                    return false;
            }

            $this.mask(has9digit()?'99999-9999':'9999-9999');
    });
}

/* onlyNumbers */
function _onlyNumbers(selector){
    $(selector).keypress(function(e){
        return (e.which >= 48 && e.which <= 57) || ($.inArray(e.which,[0,13,8]) != -1);
    });
}

// head.js features
head.feature('placeholder', function() {
    var t = document.createElement('textarea'); // #=> <textarea></textarea>
    return (t.placeholder !== undefined); // #=> true || false
});

head.feature('number', function() {
    var t = document.createElement('input'); 
    // if(!$.browser.msie) t.type = 'number'; // #=> <input type="number">
    return (t.max !== undefined); // #=> true || false
});

// loaders
function _loadFancybox(callback){
    if(fbLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        $.getCss(CSS_PATH+'/fancybox/jquery.fancybox-1.3.4.css');
        head.js(JS_PATH+'/jquery.fancybox-1.3.4.pack.js',function(){
            fbLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _loadCycle(callback){
    if(cycleLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        head.js(JS_PATH+'/cycle.min.js',function(){
            cycleLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _loadScrollTo(callback){
    if(scrolltoLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        head.js(JS_PATH+'/jquery.scrollTo-min.js',function(){
            scrolltoLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

var js_loaded=[];
function js(script,callback){
    if(js_loaded.indexOf(script)!=-1){
        if(typeof(callback)=='function') (callback)();
    } else {
        head.js(JS_PATH+'/'+script+'.js',function(){
            js_loaded.push(script);
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _menusDesktop() {
    var menu_time = "fast";
    $("#top-menu > ul > li > ul").addClass("submenu");
    $("#top-menu a.has-sub").bind('mouseenter.menu1',function(){
        clearTimeout($(this).data('timeoutId'));
        $(this).parent().find("ul.submenu").fadeIn(menu_time);
    }).bind('mouseleave.menu1',function(){
        var someelement = this;
        var timeoutId = setTimeout(function(){ $(someelement).parent().find("ul.submenu").fadeOut(menu_time);}, 200);
        $(someelement).data('timeoutId', timeoutId);
    });
    $("ul.submenu").bind('mouseenter.menu1',function(){
        clearTimeout($(this).parent().find("a.has-sub").data('timeoutId'));
    }).bind('mouseleave.menu1',function(){
        $(this).fadeOut(menu_time);
    });
}

function _menusDesktopUndo() {
    $("#top-menu a.has-sub").unbind('mouseenter.menu1');
    $("#top-menu a.has-sub").unbind('mouseleave.menu1');
    $("ul.submenu").unbind('mouseenter.menu1');
    $("ul.submenu").unbind('mouseleave.menu1');
}

function _menusMobile() {
    var menu_time = "fast";
    $("#top-menu > ul > li > ul").addClass("submenu");
    $("#top-menu a.has-sub").bind('click.menu',function(e){
        var $this = $(this),
            $submenu = $this.parent().find("ul.submenu");
        
        if($submenu.hasClass('show')) return true;
        
        e.preventDefault();
        $submenu.toggleClass('show');
        return false;
    });
}

function _menusMobileUndo() {
    $("#top-menu a.has-sub").unbind('click.menu');
}

function vidRescale(){
    // var win = $(window),
    var win = $('.banner.video .video-background'),
        w = win.width(),//+200,
        h = win.height();//+200;
    
    // if(isMobile) return win.remove();
    if(isMobile) if(ytplayer) $('#content').click(function(e){
        // ytplayer.stopVideo();
        // ytplayer.seekTo(0);
        // ytplayer.playVideo();
    });

    if(ifplayer) ifplayer = $(ifplayer);
    // if(ifplayer) return console.log(ifplayer);
    if(ifplayer) if (w/h > 16/9){
        // ifplayer.setSize(w, w/16*9);
        ifplayer.css({width: w, height: w/16*9+'px'});
        ifplayer.css({'left': '0px'});
    } else {
        // ifplayer.setSize(h/9*16, h);
        ifplayer.css({width:h/9*16, height: h});
        ifplayer.css({'left': -(ifplayer.outerWidth()-w)/2});
    }

    if(ytplayer) if (w/h > 16/9){
        ytplayer.setSize(w, w/16*9);
        $('#ytplayer').css({'left': '0px'});
    } else {
        ytplayer.setSize(h/9*16, h);
        $('#ytplayer').css({'left': -($('#ytplayer').outerWidth()-w)/2});
    }

    if(vmplayer) {
        var vmiframe = $('#vmplayer')[0];
        vmplayer = $f(vmiframe);

        // vmplayer.addEvent('ready', function() {
        $('#vmplayer').load(function() {
            vmplayer.api('setVolume', 0);
            vmplayer.api('play');
        });

        if (w/h > 16/9){
            // vmplayer.setSize(w, w/16*9);
            $('#vmplayer').css({width:w, height:w/16*9});
            $('#vmplayer').css({'left': '0px'});
        } else {
            // vmplayer.setSize(h/9*16, h);
            $('#vmplayer').css({width:h/9*16, height:h});
            $('#vmplayer').css({'left': -($('#vmplayer').outerWidth()-w)/2});
        }
    }

    if(vdplayer) {
        w+= 200;
        h+= 200;
        console.log([w,h,w/h > 16/9]);
        var $vdplayer = $('#vdplayer');

        if (w/h > 16/9){
            // vmplayer.setSize(w, w/16*9);
            $vdplayer.css({width:w, height:w/16*9});
            $vdplayer.css({'left': '0px'});
        } else {
            // vmplayer.setSize(h/9*16, h);
            $vdplayer.css({width:h/9*16, height:h});
            $vdplayer.css({'left': -($vdplayer.outerWidth()-w)/2});
        }
    }
}

// mobile
function checkMobile(){
    windowWidth = $(window).width(); // window.innerWidth
    isDesktop = windowWidth >= 1199;
    isMobile = windowWidth < 1199;
    isTablet = windowWidth < 1199 && windowWidth > 730;
    isSmartphone = windowWidth < 730;

    if($('.banner').hasClass('video')) {
        window.setTimeout(function(){
            vidRescale();
        },500);
    }

    var $html = $('html');
    $html.removeClass('desktop').removeClass('mobile').removeClass('tablet').removeClass('smartphone');
    // if(isDesktop) $html.addClass('desktop');
    // if(isMobile) $html.addClass('mobile');
    // if(isTablet) $html.addClass('tablet');
    // if(isSmartphone) $html.addClass('smartphone');

    var log = windowWidth;
    if(isDesktop) log+= ':desktop';
    if(isMobile) log+= ':mobile';
    if(isTablet) log+= ':tablet';
    if(isSmartphone) log+= ':smartphone';

    if(isMobile){
        $('.desktop').hide();
        $('.mobile').show();
        _menusDesktopUndo();
        _menusMobile()
    }
    
    if(isSmartphone){
        // switch(CONTROLLER){ }
    }

    if(isDesktop){
        $('.mobile').hide();
        $('.desktop').show();
        _menusMobileUndo();
        _menusDesktop();
    }

    if(APPLICATION_ENV=='development') console.log(log);
}