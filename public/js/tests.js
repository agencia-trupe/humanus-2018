var Test = {};

Test.form = function(form,values,autosubmit){
	var autosub = autosubmit || false, v;
	for(v in values) $(v,form).val(values[v]);
	if(autosub) $(form).submit();
}

Test.formTrabalhe = function(){
	Test.form('form.trabalhe',{
		'#nome': 'Teste',
		'#email': 'teste'+SITE_NAME+'@mailinator.com',
		'#telefone': '(11)10101-1010',
		'#mensagem': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto nemo assumenda sapiente enim a repellendus labore sed quam nesciunt, provident laborum, molestias veniam dolores perspiciatis quibusdam illum voluptatibus! Nihil, autem!',
	});
}