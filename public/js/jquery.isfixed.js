$.fn.extend({
    isfixed : function(opt){
        var options = {appender:null},
            conf = opt ? $.extend(options,opt) : options;
        
        //var top = $(this).offset().top - parseFloat($(this).css('marginTop').replace(/auto/,0)),
        var top = $(this).offset().top,
            $el = $(this).clone();
        
        $el.attr("id",$el.attr("id")+"_clone");
        
        $(window).scroll(function(){
            var y = $(this).scrollTop();
            if(y >= top){
                //$el.addClass('fixed');
                $el.appendTo(conf.appender);
            } else {
                //$el.removeClass('fixed');
                $el.remove();
            }
        });
    }
});