<?php
/**
 * Formatação de Datas
 * Auxiliar da Camada de Visualização
 * @author Wanderson Henrique Camargo Rosa
 * @see APPLICATION_PATH/views/helpers/Date.php
 */
class Zend_View_Helper_DateExt extends Zend_View_Helper_Abstract
{
    /**
     * Manipulador de Datas
     * @var Zend_Date
     */
    protected static $_date = null;
    protected static $_locale = null;
 
    /**
     * Método Principal
     * @param string $value Valor para Formatação
     * @param string $format Formato de Saída
     * @return string Valor Formatado
     */
    public function dateExt($data)
    {
        $data2 = reset(explode(' ', $data));
        list($ano,$mes,$dia) = explode('-', $data2);
        $date = $dia.' '.
                $this->view->date2($data,'MONTH_NAME').' '.
                $ano;
        return $date;
    }
 
}