<?php

class Admin_ProdutosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Admin_Model_Login::checkAuth($this);
        
        $this->view->titulo = "PRODUTOS";
        $this->view->section = $this->section = "produtos";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
        
        $this->view->MAX_FOTOS = 1;
        
        // models
        $this->produtos = new Application_Model_Db_Produtos();
        // $this->formatos = new Application_Model_Db_Formatos();
        // $this->gramaturas = new Application_Model_Db_Gramaturas();
        // $this->produtos_infos = new Application_Model_Db_ProdutosInfos();
        // $this->cores = new Application_Model_Db_Cores();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        // $this->bitly = new Php_Bitly(BITLY_USER,BITLY_KEY);
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 20;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
            $rows = $this->produtos->fetchAll($where,$post['search-by'],$limit,$offset);
            
            $total = $this->view->total = $this->produtos->count($where);
        } else {
            $rows = $this->produtos->fetchAll(null,array('ordem'),$limit,$offset);
            $total = $this->view->total = $this->produtos->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $form = new Admin_Form_Produtos();
        // $this->view->categorias = $form->getElement('categoria_id')->options;
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        $form = new Admin_Form_Produtos();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            /*$data['peso'] = !strstr($data['peso'],',') && strlen(trim($data['peso'])) > 0 ?
                                $this->view->currency($data['peso'],array('display'=>Zend_Currency::NO_SYMBOL)) :
                                $data['peso'];
            $data['valor'] = !strstr($data['valor'],',') && strlen(trim($data['valor'])) > 0 ?
                                $this->view->currency($data['valor'],array('display'=>Zend_Currency::NO_SYMBOL)) :
                                $data['valor'];
            $data['valor_promo'] = !strstr($data['valor_promo'],',') && strlen(trim($data['valor_promo'])) > 0 ?
                                $this->view->currency($data['valor_promo'],array('display'=>Zend_Currency::NO_SYMBOL)) :
                                $data['valor_promo'];*/
            $this->view->id = $this->produto_id = $data['id'];
            
            $form->addElement('hidden','id');
            $this->view->fotos = $this->fotosAction();
            // $this->view->sugestoes = $this->sugestoesAction();
            // $this->view->sugestoes_list = $this->produtos->getSugestoes($data['id']);
            // unset($data['info']);
            // $this->view->formatos = $this->formatosAction();
            // $this->view->formatos_list = $this->produtos->getFormatos($data['id']);
            // $this->view->gramaturas = $this->gramaturasAction();
            // $this->view->gramaturas_list = $this->produtos->getGramaturas($data['id']);
            
            // checagem de estoque
            if($data['estoque'] < 1){
                // $this->messenger->addMessage('Este produto está sem estoque.','error');
            } else if($data['estoque'] <= $data['estoque_minimo']){
                // $this->messenger->addMessage('O estoque deste produto está se esgotando.','error');
                
            }
        } else {
            // return $this->_redirect('admin/produtos');
            $data = array('status_id'=>'1');
            
            if(isset($_SERVER['HTTP_REFERER'])){
                if(strstr($_SERVER['HTTP_REFERER'],'categoria_id') && strstr($_SERVER['HTTP_REFERER'],VALEPRESENTEID)){
                    $data['categoria_id'] = VALEPRESENTEID;
                }
            }
        }
        
        $form->populate($data);
        $this->view->form = $form;
        
        ////$this->view->cores = array_merge(array('0'=>'Cor...'),$this->cores->getKeyValues('descricao',false,'ordem'));
        //$this->view->cores = $this->cores->getKeyValues('descricao',array('0'=>'Cor...'),'ordem');
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->produtos->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $post = $this->_request->getParams();
            $data = array_map('utf8_decode',$post);
            $data['alias']       = Is_Str::toUrl($post['titulo'].((bool)$post['cod'] ? ' '.$post['cod'] : ''));
            $data['info']   = strip_tags($data['info'],'<b><a><i><u><br><ul><ol><li><img><p><div>');
            $data['descricao_completa']   = strip_tags($data['descricao_completa'],'<b><a><i><u><br><ul><ol><li><img><p><div>');
            // $data['peso']        = str_replace(',','.',$data['peso']);
            // $data['valor']       = str_replace(',','.',$data['valor']);
            // $data['valor_promo'] = str_replace(',','.',$data['valor_promo']);
            $data['user_edit']   = $this->login->user->id;
            $data['data_edit']   = date("Y-m-d H:i:s");
            
            if(!$row){
                $data['user_cad']  = $this->login->user->id;
                $data['data_cad']  = date("Y-m-d H:i:s");
                $data['ordem'] = $this->produtos->getNextOrdem();

                // Bit.ly
                if(APPLICATION_ENV=='production2') $data['bitly_url'] = $this->bitly->short(URL.'/produto/'.$data['alias'].'/');
            } else {
                // Bit.ly
                if(APPLICATION_ENV=='production2' && ($data['alias'] != $row->alias || !(bool)$row->bitly_url)){
                    $data['bitly_url'] = $this->bitly->short(URL.'/produto/'.$data['alias'].'/');
                }
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->produtos->update($data,'id='.$id) : $id = $this->produtos->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $data = Is_Array::utf8All($data);
            $data['valor'] = str_replace('.',',',$data['valor']);
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>$data));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ?
                     'Já existe um produto com o mesmo título e peso, escolha um diferente.' :
                     $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->produtos->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/'));return false; }
        
        $data = Is_Array::utf8All($row->toArray());
        // $row_info = $this->produtos_infos->fetchRow('produto_id='.$data['id']);
        // $data['info'] = $row_info ? Is_Array::utf8All($row_info->toArray()) : NULL;
        //Is_Var::dump($data);
        $this->_forward('new',null,null,array('data'=>$data));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Produtos();
        
        try {
            $table->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function sugestoesAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('produtos')/*->where('categoria_id=30')*/->order('descricao')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }
    
    public function sugestoesAddAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('sugestao_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutosSugestoes();
            try{
                $produtos_sugestoes->insert(array(
                    'produto_id'=>$this->_getParam('produto_id'),
                    'sugestao_id'=>$this->_getParam('sugestao_id')
                ));
                return array();
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Esta sugestão já foi inserida." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar sugestão.');
        }
    }
    
    public function sugestoesDelAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('sugestao_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutosSugestoes();
            $produtos_sugestoes->delete('produto_id="'.$this->_getParam('produto_id').'"'.
                                        'and sugestao_id="'.$this->_getParam('sugestao_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover sugestão.');
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('produtos_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->produto_id)){
            $select->where('f2.produto_id = ?',$this->produto_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size.'kb'))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = new Application_Model_Db_ProdutosFotos();
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->insert(array("foto_id"=>$foto_id,"produto_id"=>$produto_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function fotoCorAction()
    {
        $produto = $this->_getParam("produto");
        $foto = $this->_getParam("foto");
        $cor  = $this->_getParam("cor");
        
        if(!(bool)$foto || !(bool)$cor) return array("erro"=>'Erro ao enviar');
        
        $fotos = new Application_Model_Db_ProdutosFotos();
        
        try {
            $fotos->update(array('cor_id'=>$cor),"produto_id=".$produto.' and foto_id='.$foto);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }

    public function formatosAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('formatos')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function formatosAddAction()
    {
        if($this->_hasParam('produto_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoFormato();
            try{
                $formato_id = $this->_getParam('formato_id');

                if($this->_hasParam('formato_formato')){
                    $formato_formato = trim($this->_getParam('formato_formato'));
                    $formato_alias = Is_Str::toUrl(trim($formato_formato));

                    if($rowformato = $this->formatos->fetchRow('alias="'.$formato_alias.'"')){
                        $formato_id = $rowformato->id;
                    } else {
                        $formato_data = array(
                            'formato' => (utf8_decode($formato_formato)),
                            'alias' => $formato_alias
                        );
                        $formato_id = $this->formatos->insert($formato_data);
                    }

                }

                $produtos_sugestoes->insert(array(
                    'produto_id'=>$this->_getParam('produto_id'),
                    'formato_id'=>$formato_id
                ));
                return array();
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar o formato.');
        }
    }
    
    public function formatosDelAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('formato_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoFormato();
            $produtos_sugestoes->delete('produto_id="'.$this->_getParam('produto_id').'"'.
                                        'and formato_id="'.$this->_getParam('formato_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover o formato.');
        }
    }

    public function gramaturasAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('gramaturas')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function gramaturasAddAction()
    {
        if($this->_hasParam('produto_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoGramatura();
            try{
                $gramatura_id = $this->_getParam('gramatura_id');

                if($this->_hasParam('gramatura_gramatura')){
                    $gramatura_gramatura = trim($this->_getParam('gramatura_gramatura'));
                    $gramatura_alias = Is_Str::toUrl(trim($gramatura_gramatura));

                    if($rowgramatura = $this->gramaturas->fetchRow('alias="'.$gramatura_alias.'"')){
                        $gramatura_id = $rowgramatura->id;
                    } else {
                        $gramatura_data = array(
                            'gramatura' => (utf8_decode($gramatura_gramatura)),
                            'alias' => $gramatura_alias
                        );
                        $gramatura_id = $this->gramaturas->insert($gramatura_data);
                    }

                }

                $produtos_sugestoes->insert(array(
                    'produto_id'=>$this->_getParam('produto_id'),
                    'gramatura_id'=>$gramatura_id
                ));
                return array();
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar a gramatura.');
        }
    }
    
    public function gramaturasDelAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('gramatura_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoGramatura();
            $produtos_sugestoes->delete('produto_id="'.$this->_getParam('produto_id').'"'.
                                        'and gramatura_id="'.$this->_getParam('gramatura_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover a gramatura.');
        }
    }

    public function ordenarAction()
    {
        $_produtos = $this->produtos->fetchAll(null,'ordem');
        
        if(count($_produtos)){
            $produtos = Is_Array::utf8DbResult($_produtos);
        } else {
            $produtos = null;
        }
        
        $this->view->produtos = $produtos;
    }
    
    public function saveOrdenarAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url));
            return;
        }
        
        $duplicate_count = 0;
        $duplicates = array();
        $params = $this->_request->getParams();
        
        try {
            for($i=0;$i<sizeof($params['id']);$i++){
                $data = array();
                $row = $this->produtos->fetchRow('id='.$params['id'][$i]); // verifica registro para atualização
                
                $data['ordem']        = $params['ordem'][$i];
                $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
                $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
                
                if($row){
                    $up = 0;
                    if($row->ordem != $data['ordem']){ $row->ordem = $data['ordem']; $up++; }
                    
                    if($up > 0){
                        $row->data_edit = $data['data_edit'];
                        $row->save();
                    }
                } else {
                    $this->categorias->insert($data);
                }
            }
            
            // se há registros duplicados, adiciona mensagem
            ($duplicate_count > 0) ?
                $this->messenger->addMessage($duplicate_count.' registros possuem duplicidade. Por favor, altere-os e salve novamente:<br/>'.implode('<br/>',$duplicates),'error') :
                $this->messenger->addMessage('Registros atualizados.');
            
            $this->_redirect('admin/'.$this->section.'/ordenar/');
            //$this->_forward('index');
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
        }
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->produtos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}
