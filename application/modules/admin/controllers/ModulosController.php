<?php

class Admin_ModulosController extends ZendPlugin_Controller_Ajax
{
    public $portfolio_produto_id = 6;
    public $modulo_arquivos = 'arquivos-deslizantes-acessorios';
    public $_paginas = array(
        2 => 'CONSULTORIA',
        20 => 'IN COMPANY',
    );

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "MÓDULOS";
        $this->view->section = $this->section = "modulos";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = $this->_getTitulo();
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;
        if($this->_hasParam('dump')) _d($this->img_path);

        $this->view->MAX_FOTOS = 1;
        
        // models
        $this->portfolio = new Application_Model_Db_Modulos();
        $this->fotos_fixas = new Application_Model_Db_PaginasFotosFixas();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 200;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('produto_id')){
            $this->_setParam('search-by','produto_id');
            $this->_setParam('search-txt',$this->_getParam('produto_id'));
        }

        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
            if($post['search-by']=='produto_id') {
                $this->view->produto_id = $post['search-txt'];
                $where = "produto_id in(".$post['search-txt'].")";
            }
            if($post['search-by']=='categoria_id')
                $where = "categoria_id in(".$post['search-txt'].")";
            $rows = $this->portfolio->fetchAll($where,'ordem',$limit,$offset);
            
            $total = $this->view->total = $this->portfolio->count($where);
        } else {
            // return $this->_redirect('admin/modulos/?search-by=produto_id&search-txt=2');
            $rows = $this->portfolio->fetchAll(null,'ordem',$limit,$offset);
            $total = $this->view->total = $this->portfolio->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);

        $form = new Admin_Form_Modulos('modulos');
        // $categorias = ($post['search-by']=='produto_id' && (bool)trim($post['search-txt']))?
        //     $form->getElement('categoria_id_'.$post['search-txt'])->options:
        //     $form->getElement('categoria_id_2')->options+
        //     $form->getElement('categoria_id_20')->options;
        // if(isset($categorias['__none__'])) unset($categorias['__none__']);
        // $this->view->categorias = $categorias;
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Modulos('modulos');

        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->portfolio_id = $data['id'];
            $this->view->portfolio_row = $this->portfolio_row = Is_Array::toObject($data);
            
            $form->addElement('hidden','id');
            $this->view->fotos = $this->fotosAction();
            if(isset($data['info'])) unset($data['info']);

            $this->view->allow_photos_fixas = true;
            $this->view->portfolio_produto_id = $this->portfolio_produto_id;
            $this->view->produto_id = $data['produto_id'];
            $this->fotosAction();
            // $this->fotosFixasAction();

            if((bool)@$data['categoria_id'])
                $data['categoria_id_'.@$data['produto_id']] = $data['categoria_id'];
        } else {
            $data = $this->_hasParam('linha_id') ?
                    array('status_id'=>'0','linha_id'=>$this->_getParam('linha_id')):
                    array('status_id'=>'1');

            $produto_id = ($this->_hasParam('produto_id')) ? $this->_getParam('produto_id') : null;
            if($produto_id) $data['produto_id'] = $produto_id;
        }

        if($this->_hasParam('linha_id') || (bool)@$data['linha_id']){
            $form->removeElement('status_id');
            $form->addElement('hidden','status_id');
            $form->addElement('hidden','linha_id');
        }
        
        $form->populate($data);
        $this->view->form = $form;
        $this->view->hide_form = @$data['alias']==$this->modulo_arquivos;

        if($this->view->hide_form && isset($data['id'])){
            $this->view->titulo = $data['titulo'];
        }
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->portfolio->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $post = $this->_request->getParams();
            $data = $post;
            // $data['descricao']   = isset($data['descricao']) ? cleanHtml($data['descricao']) : null;
            // $data['depoimento']  = isset($data['depoimento']) ? cleanHtml($data['depoimento']) : null;
            $data['descricao']   = isset($data['descricao']) ? cleanHtml($data['descricao']) : null;
            $data['depoimento']  = isset($data['depoimento']) ? strip_tags($data['depoimento']) : null;
            $data['alias']       = Is_Str::toUrl($post['titulo']);
            $data['user_edit']   = $this->login->user->id;
            $data['data_edit']   = date("Y-m-d H:i:s");
            $data = array_map('utf8_decode',$data);
            
            if(!$row){
                $data['user_cad']  = $this->login->user->id;
                $data['data_cad']  = date("Y-m-d H:i:s");
                $data['ordem']     = $this->portfolio->getNextOrdem($data['produto_id']);
            }

            if((bool)@$data['linha_id']){
                if(isset($data['data_cad'])){ unset($data['data_cad']); }
                if(isset($data['data_edit'])){ unset($data['data_edit']); }
            }
            
            // remove dados desnecessários
            $unsets = 'submit,module,controller,action';
            foreach(explode(',', $unsets) as $u) if(isset($data[$u])) unset($data[$u]);
            foreach($data as $k => $v) if(strstr($k, 'categoria_id_')) unset($data[$k]);
            if($data['categoria_id']=='') unset($data['categoria_id']);
            // _d($data);
            
            ($row) ? $this->portfolio->update($data,'id='.$id) : $id = $this->portfolio->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $data = Is_Array::utf8All($data);
            $data['valor'] = str_replace('.',',',$data['valor']);
            $this->_redirect('admin/'.$this->section.'/edit/'.$id);
            //$this->_forward('new',null,null,array('data'=>$data));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ?
                     'Já existe um projeto com o mesmo título, escolha um diferente.' :
                     $e->getMessage();
            // _d($error);
            $this->messenger->addMessage($error,'error');
            $this->_redirect('admin/'.$this->section.'/new',array('data'=>$this->_request->getParams()));
            // $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = $this->_getParam('id');
        $row   = $this->portfolio->fetchRow('alias = "'.$id.'" or id="'.$id.'"');
        
        if(!$row){ $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section.'/'));return false; }
        
        $data = Is_Array::utf8All($row->toArray());
        //Is_Var::dump($data);
        $this->_forward('new',null,null,array('data'=>$data));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Modulos();
        
        try {
            $table->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }

    public function fotosFixasAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos_fixas as tf')
            ->order('tf.id asc');
        
        if(isset($this->portfolio_produto_id)){
            $select->where('produto_id = ?',$this->portfolio_produto_id);
        }
        
        $_fotos = $select->query()->fetchAll();
        
        array_walk($_fotos,'Func::_arrayToObject');
        
        $fotos = array();
        
        for($i=0;$i<sizeof($_fotos);$i++){
            $fotos[] = $_fotos[$i];
        }
        
        //_d(array($fotos));
        $this->view->fotos_fixas = count($fotos) ? $fotos[0] : null;
    }

    /**
     * Salva ordenação de fotos via ajax
     */
    public function ordemFotosAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            if(!$this->fotos)
                $this->fotos = new Application_Model_Db_Fotos();

            for($i=0;$i<sizeof($values['id']);$i++) $this->fotos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('modulos_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f.ordem');
            // ->order('f2.id asc');
        
        if(isset($this->portfolio_id)){
            $select->where('f2.modulo_id = ?',$this->portfolio_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    public function saveFotosFixasAction()
    {
        if(!$this->_hasParam('fotos_fixas_id')) return $this->_forward('denied','error','default',array('url'=>URL.'/admin/'));
        
        $post = $this->_request->getParams();
        //$data_resposta = array_map('utf8_decode',$post['question']);
        //unset($post['question']);
        $data = array_map('utf8_decode',$post);
        $fotos = array();
        
        // remove dados desnecessários
        $unsets = 'submit,module,controller,action,fotos,enviar';
        foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
        
        // upload de arquivos
        $file = null; $rename = null;
        $table = $this->fotos_fixas;
        
        $check_uploads = array();
        for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i;
        $check_uploads = implode(',',$check_uploads);

        foreach(explode(',',$check_uploads) as $cu)
            if((bool)@$_FILES[$cu]) 
                if(!(bool)$_FILES[$cu]['name']) 
                    unset($_FILES[$cu]);
        
        foreach(explode(',',$check_uploads) as $cu) {
            if((bool)@$_FILES[$cu] && (bool)$_FILES[$cu]['name']){
                $file = $_FILES[$cu];
                $path = $this->img_path;
                $error = 'Imagem inválida';
                $ext = 'jpg,jpeg,png,bmp,gif,tiff';
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Size', false, array('max' => '50mb'))
                       ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,$cu)
                       ->setDestination($path);

                if($upload->isValid($cu)){
                    $upload->receive($cu);

                    $fotos[$cu] = $rename;
                } else {
                    $err = $error;
                    if(APPLICATION_ENV!='production') $err.= "<br/>\n".var_dump($upload->getErrors());
                    $this->messenger->addMessage($err,'error');
                    
                    $this->_redirect('admin/modulos/edit/'.$data['produto_id']);
                }
            }
        }
        
        if(count($fotos)) {
            $table->update($fotos,'id = '.$data['fotos_fixas_id']);
            
            // atualizando thumbnail
            /*$this->portfolio->update(array(
                'thumbnail' => @$fotos['foto1']
            ),'id = '.$data['produto_id']);*/
            $row = $this->portfolio->fetchRow('id = '.$data['produto_id']);

            if($row){
                $thumbnail = $this->img_path.'/'.$row->thumbnail;
                
                if((bool)trim($row->thumbnail) && file_exists($thumbnail)){
                    Is_File::del($thumbnail);
                    Is_File::delDerived($thumbnail);
                }

                $row->thumbnail = @$fotos['foto1'];
                $row->save();
            }
        }

        $this->messenger->addMessage('Fotos enviadas com sucesso');
        return $this->_redirect('admin/modulos/edit/'.$data['produto_id']);
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            // tratamento de imagem padrão
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = new Application_Model_Db_ModulosFotos();
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->insert(array("foto_id"=>$foto_id,"modulo_id"=>$produto_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function saveDescricaoAction()
    {
        if(!$this->_hasParam('portfolio_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('descricao')) {
            return array('error'=>'Acesso negado');
        }
        
        $f = new Application_Model_Db_Fotos();
        
        try{
            $f->update(
                array('descricao'=>utf8_decode($this->_getParam('descricao'))),
                'id='.$this->_getParam('foto_id')
            );
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function saveCapaAction()
    {
        if(!$this->_hasParam('portfolio_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('flag')) {
            return array('error'=>'Acesso negado');
        }
        
        $pid = $this->_getParam('portfolio_id');
        $fid = $this->_getParam('foto_id');
        $flag = $this->_getParam('flag');
        
        $f = new Application_Model_Db_Fotos();
        $pf = new Application_Model_Db_ModulosFotos();
        $p = new Application_Model_Db_Modulos();
        
        try{
            if($flag==1){
                $pfs = $pf->fetchAll('modulo_id='.$pid);
                
                if(count($pfs)){
                    $ids = array();
                    foreach($pfs as $pf) $ids[] = $pf->foto_id;
                    $f->update(array('flag'=>0),'id in ('.implode(',',$ids).')');
                }
            }
            
            $p->update(array('capa_id'=>((bool)$flag?$fid:null)),'id='.$pid);
            $f->update(array('flag'=>$flag),'id='.$fid);
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->portfolio->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    public function _getTitulo()
    {
        $data = ($this->_hasParam('data')) ? $this->_getParam('data') : null;
        $produto_id = ($this->_hasParam('produto_id')) ? $this->_getParam('produto_id') : $this->_getParam('search-txt');
        $url_params = '?search-by=produto_id&search-txt='.(
                        ($data) ? $data['produto_id'] : $produto_id
                        );
        if(!$data && !$this->_hasParam('search-txt') && !$this->_hasParam('produto_id')) $url_params = null;
        $titulo = "<a href='".$this->_url.$url_params."'>".
                    $this->view->titulo.' '.
                    ($this->_hasParam('search-txt') ? @$this->_paginas[$this->_getParam('search-txt')] : '').
                    "</a>";

        return $titulo;
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
}
