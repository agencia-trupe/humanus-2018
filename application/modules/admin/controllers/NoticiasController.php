<?php

class Admin_NoticiasController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "ARTIGOS";
        $this->view->section = $this->section = "noticias";
        $this->view->section2 = $this->section2 = "noticias";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        
        // models
        $this->noticias = new Application_Model_Db_Noticias();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        
        // Admin_Model_Login::setControllerPermissions($this,$this->section);
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $rows = $this->noticias->fetchAll($where,'data_edit desc',$limit,$offset);
            
            $total = $this->view->total = $this->noticias->count($where);
        } else {
            $rows = $this->noticias->fetchAll(null,'data_edit desc',$limit,$offset);
            $total = $this->view->total = $this->noticias->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Noticias();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->noticia_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            $this->view->fotos = $this->fotosAction();
            $this->arquivosAction();
            $this->view->allow_photos = $data['allow_photos'];
            $this->view->allow_files = false;
        } else {
            $form->removeElement('body');
            $form->removeElement('fonte');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->noticias->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            $data['alias'] = Is_Str::toUrl($this->_getParam('titulo'));
            $data['body']  = isset($data['body']) ? cleanHtml($data['body']) : null;
            $data['fonte']  = isset($data['fonte']) ? ($data['fonte']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            $data = array_map('utf8_decode',$data);
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->noticias->update($data,'id='.$id) : $id = $this->noticias->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->noticias->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->noticias->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }

    public function saveCapaAction()
    {
        if(!$this->_hasParam('portfolio_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('flag')) {
            return array('error'=>'Acesso negado');
        }
        
        $pid = $this->_getParam('portfolio_id');
        $fid = $this->_getParam('foto_id');
        $flag = $this->_getParam('flag');
        
        $f = new Application_Model_Db_Fotos();
        $pf = new Application_Model_Db_NoticiasFotos();
        $p = $this->noticias;//new Application_Model_Db_Noticias();
        $rowf = $f->get($fid);
        
        try{
            if($flag==1){
                $pfs = $pf->fetchAll('noticia_id='.$pid);
                
                if(count($pfs)){
                    $ids = array();
                    foreach($pfs as $pf) $ids[] = $pf->foto_id;
                    $f->update(array('flag'=>0),'id in ('.implode(',',$ids).')');
                }
            }
            
            $p->update(array(
                'capa_id'=>((bool)$flag ? $fid : null),
                'capa_path'=>((bool)$flag&&$rowf ? $rowf->path : null),
            ),'id='.$pid);
            $f->update(array('flag'=>$flag),'id='.$fid);
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    /**
     * Salva ordenação de fotos via ajax
     */
    public function ordemFotosAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            if(!$this->fotos)
                $this->fotos = new Application_Model_Db_Fotos();

            for($i=0;$i<sizeof($values['id']);$i++) $this->fotos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('noticias_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f.ordem');
            // ->order('f2.id asc');
        
        if(isset($this->noticia_id)){
            $select->where('f2.noticia_id = ?',$this->noticia_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        // $max_size = '5120'; // '2048'
        // $max_size = $this->view->MAX_SIZE.'MB'; //'5120'; //'2048';
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/noticias/'));
            return;
        }
        
        $file = $_FILES['file'];
        $isFile = $this->_hasParam('arquivo');
        $filename = str_replace('.'.Is_File::getExt($file['name']),'',$file['name']);
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        
        if($isFile){
            $upload->addFilter('Rename',$this->file_path.'/'.$rename)
                   ->setDestination($this->file_path);
        } else {
            $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
                   ->addValidator('Size', false, array('max' => $max_size))
                   ->addValidator('Count', false, 1)
                   ->addFilter('Rename',$this->img_path.'/'.$rename)
                   ->setDestination($this->img_path);
       }
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            if(!$isFile){
                $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
                $thumb->resize('1000','1000');
                $thumb->save($this->img_path.'/'.$rename);
            }
            
            //$fotos
            $table  = $isFile ? new Application_Model_Db_Arquivos() : new Application_Model_Db_Fotos();
            //$paginas_fotos
            $table2 = $isFile ? new Application_Model_Db_NoticiasArquivos() : new Application_Model_Db_NoticiasFotos();
            $type   = $isFile ? "arquivo" : "foto";
            $pagina_id = $this->_getParam('id');
            
            //$data_fotos
            $data_insert = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s"),
                'flag'     => $this->_hasParam('flag') ?
                              $this->_getParam('flag') : null
            );

            if($isFile){
                $data_insert['descricao'] = $filename;
            }
            
            if(!$insert_id = $table->insert($data_insert)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            
            $table2->insert(array(
                $type."_id" => $insert_id,
                "noticia_id" => $pagina_id
            ));
            
            return array("name"=>$rename,"id"=>$insert_id,"descricao"=>$filename);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function arquivosDelAction()
    {
        $id = $this->_getParam("file");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->delete("id=".(int)$id);
            Is_File::del($this->file_path.'/'.$arquivo->path);
            Is_File::delDerived($this->file_path.'/'.$arquivo->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosRenameAction()
    {
        $id = $this->_getParam("file");
        $descricao = $this->_getParam("descricao");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->update(array('descricao'=>utf8_decode(urldecode($descricao))),"id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('noticias_arquivos as tf')
            ->join('arquivos as f','f.id=tf.arquivo_id')
            ->order('f.descricao asc');
        
        // if(isset($this->pagina_id)) $select->where('pagina_id = ?',$this->pagina_id);
        if(isset($this->noticia_id)) $select->where('noticia_id = ?',$this->noticia_id);
        
        $arquivos = $select->query()->fetchAll();
        
        array_walk($arquivos,'Func::_arrayToObject');
        
        $this->view->arquivos = $arquivos;
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}