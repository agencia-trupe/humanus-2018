<?php

class Admin_Clientes2Controller extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        if($this->_hasParam('ini')){
            // ini_set("system_tmp_dir","/home/millagro/temp");
            // _d(sys_get_temp_dir());
        }

        $this->view->titulo = "CLIENTES";
        $this->view->section = $this->section = "clientes2";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;
        if($this->_hasParam('dump')) _d($this->img_path);
        
        // models
        $this->noticias = new Application_Model_Db_Clientes2();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        $this->view->MAX_FOTOS = 1;

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        $order = array('ordem','titulo');
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $rows = $this->noticias->fetchAll($where,$order);//,$limit,$offset);
            
            $total = $this->view->total = $this->noticias->count($where);
        } else {
            $rows = $this->noticias->fetchAll(null,$order);//,$limit,$offset);
            $total = $this->view->total = $this->noticias->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        // $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Clientes2();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->noticia_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            $this->view->fotos = $this->fotosAction();
            $this->view->allow_photos = $data['allow_photos'];

            $data['data'] = (bool)$data['data'] ? Is_Date::am2br($data['data']) : null;
            // $data['body'] = stripslashes($data['body']);
        } else {
            // $form->removeElement('body');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->noticias->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            $data['alias'] = Is_Str::toUrl($this->_getParam('titulo'));
            // $data['body']  = isset($data['body']) ? strip_tags($data['body'],'<b><a><i><u><br><ul><ol><li><img><p><div>') : null;
            // $data['body']  = isset($data['body']) ? strip_tags($data['body'],'<b><a><i><u><ul><ol><li><img><h2><br>') : null;
            // $data['body']  = isset($data['body']) ? cleanHtml($data['body']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            $data['data'] = (bool)$data['data'] ? Is_Date::br2am($data['data']) : null;
            if(!$row) $data['ordem'] = $this->noticias->getNextOrdem();
            $data = array_map('utf8_decode',$data);

            // ajustando bbcodes
            if((bool)@$data['body']){
                // replace audio bbcode
                $p = '(.*)\[audio\](.*)\[\/audio](.*)';
                $r = '\\2';
                $bbAudio = ereg_replace($p,$r,$data['body']);

                if(strstr($bbAudio,'soundcloud.com')){
                    $scid = Admin_Model_SoundCloud::getIdFromUrl($bbAudio);
                    $p = '\[audio\](.*)\[\/audio]';
                    $r = '[audio]'.($scid?$scid:'\\1').'[/audio]';
                    $data['body'] = ereg_replace($p,$r,$data['body']);
                }

                // replace video bbcode
                $p = '(.*)\[video\](.*)\[\/video](.*)';
                $r = '\\2';
                $bbVideo = ereg_replace($p,$r,$data['body']);

                if(strstr($bbVideo,'youtube.com')){
                    $scid = Youtube::getCode($bbVideo);
                    $p = '\[video\](.*)\[\/video]';
                    $r = '[video]'.($scid?$scid:'\\1').'[/video]';
                    $data['body'] = ereg_replace($p,$r,$data['body']);
                }
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->noticias->update($data,'id='.$id) : $id = $this->noticias->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe em cliente com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->noticias->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->noticias->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('clientes2_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->noticia_id)){
            $select->where('f2.cliente_id = ?',$this->noticia_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/clientes2/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $noticias_fotos = new Application_Model_Db_Clientes2Fotos();
            $noticia_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $noticias_fotos->insert(array("foto_id"=>$foto_id,"cliente_id"=>$noticia_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->noticias->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}