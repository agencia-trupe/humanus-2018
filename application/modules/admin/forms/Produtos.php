<?php

class Admin_Form_Produtos extends ZendPlugin_Form
{
    public function init()
    {
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/produtos/save/')
             ->setAttrib('id','frm-produtos')
             ->setAttrib('name','frm-produtos');
		
        // $categs = new Application_Model_Db_Categorias();
        
        // elementos
        //$this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getKeyValues()));
        // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getParentsKV($categs->getWithChildren())));
		// $this->addElement('text','cod',array('label'=>'Código','class'=>'txt'));
		$this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('text','peso',array('label'=>'Peso','class'=>'txt txt3 mask-currency'));
		// $this->addElement('text','valor',array('label'=>'Valor','class'=>'txt txt3 mask-currency'));
		// $this->addElement('text','valor_promo',array('label'=>'Valor Promo.','class'=>'txt txt3 mask-currency'));
		// $this->addElement('text','estoque',array('label'=>'Estoque','class'=>'txt txt3 mask-int'));
		// $this->addElement('text','estoque_minimo',array('label'=>'Estoque mín.','class'=>'txt txt3 mask-int'));
        //$this->addElement('radio','genero',array('label'=>'Gênero','class'=>'radio','multiOptions'=>array(0=>'Feminino',1=>'Masculino'),'separator'=>' '));
        ////$this->addElement('select','genero',array('label'=>'Gênero','class'=>'txt','multiOptions'=>array(0=>'Feminino',1=>'Masculino')));
        $this->addElement('textarea','descricao',array('label'=>'Chamada','class'=>'txt'));
        $this->addElement('textarea','descricao_completa',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        // $this->addElement('textarea','info',array('label'=>'Informações','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        $this->getElement('descricao_completa')->setAttrib('rows',1)->setAttrib('cols',1);
        // $this->getElement('info')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('categoria_id')->setRequired();
        $this->getElement('titulo')->setRequired();
        // $this->getElement('cod')->setRequired();
        //$this->getElement('genero')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

