<?php

class Admin_Form_Parceiros extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/parceiros/save')
             ->setAttrib('id','frm-parceiros')
             ->setAttrib('name','frm-parceiros');
        
        // elementos
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('text','link',array('label'=>'Link do site','class'=>'txt'));
        $this->addElement('hidden','alias');
        // $this->addElement('text','autor',array('label'=>'Autor','class'=>'txt'));
        // $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','body',array('label'=>'Conteúdo','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

