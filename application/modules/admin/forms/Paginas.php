<?php

class Admin_Form_Paginas extends Zend_Form
{
    public $row = null;

    public function __construct($row=null)
    {
        $this->row = $row;
        parent::__construct();
    }

    public function init()
    {
        $this->setMethod('post')->setAction('')->setAttrib('id','frm-paginas')->setAttrib('name','frm-paginas');
		
        if(@$this->row->parent_id==9) $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        
        if(@$this->row->has_url) $this->addElement('text','url',array('label'=>'Link '.$this->row->url_text,'class'=>'txt'));
        if(@$this->row->has_olho) $this->addElement('textarea','olho',array('label'=>'Texto olho','class'=>'txt'));
        
        // $this->addElement('textarea','body',array('label'=>'Conteúdo:','class'=>'txt'));
        $this->addElement('hidden','alias',array('class'=>'txt'));
        //$this->addElement('text','alias',array('label'=>'Alias:','class'=>'txt alias disabled','disabled'=>true));
        if(@$this->row->has_body) {
            $this->addElement('textarea','body',array('class'=>'txt wysiwyg'));
            $this->getElement('body')->setAttrib('rows','20')->setAttrib('cols','103');
        }
        
        if(@$this->row->has_body2) $this->addElement('textarea','body2',array('class'=>'txt wysiwyg'));
        if(@$this->row->has_body3) $this->addElement('textarea','body3',array('class'=>'txt wysiwyg'));

        if(@$this->row->has_extra) {
            $this->addElement('text','extra',array('label'=>$this->row->extra_text,'class'=>'txt txt-full'));
            // $this->getElement('extra')->setAttrib('rows','5')->setAttrib('cols','10');
        }
        
        //$this->addElement('checkbox','status_id',array('label'=>'Ativo:','class'=>'','checked'=>true));
        $this->addElement('submit','submit',array('label'=>'Salvar','class'=>'bt'));
        
        //$this->getElement('status_id')->setRequired();
        
        $this->removeDecs(array('label','htmlTag','description','errors'));
    }
    
    public function removeDecs($decorators = array('label','htmlTag','description','errors'),$elms=array())
    {
        $_elms = &$this->getElements();
        $elms = count($elms) ? $elms : $_elms;
        foreach($elms as $elm){
            foreach($decorators as $decorator){
                $elm->removeDecorator($decorator);
            }
        }
        return $this;
    }
}