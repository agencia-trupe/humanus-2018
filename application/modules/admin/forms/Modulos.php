<?php

class Admin_Form_Modulos extends ZendPlugin_Form
{
    public $section = 'projetos';

    public function __construct($section='projetos')
    {
        $this->section = $section;
        return parent::__construct();
    }

    public function init()
    {
		$section = $this->section;
		
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/'.$section.'/save')
             ->setAttrib('id','frm-'.$section.'')
             ->setAttrib('name','frm-'.$section.'');
		
        // elementos
        // if($this->section=='cases'){
        if($this->section=='modulos'){
            // combo produtos
            $db_produtos = new Application_Model_Db_Produtos();
            $produtos = $db_produtos->getKeyValues('titulo',true,'ordem');
            $this->addElement('select','produto_id',array('label'=>'Produto:','class'=>'txt','multiOptions'=>$produtos));
            
            // combo categorias
            // $_categorias = $db_produtos->q('select * from categorias_cases where produto_id is not null order by ordem');
            // $categorias = array();
            
            // foreach($_categorias as $cat) {
            //     if(!isset($categorias[$cat->produto_id])) $categorias[$cat->produto_id] = array('__none__'=>'Selecione...');
            //     $categorias[$cat->produto_id][$cat->id] = $cat->descricao;
            // }
            
            // foreach($categorias as $k => $cat)
            //     $this->addElement('select','categoria_id_'.$k,array('label'=>'Categoria:','class'=>'txt hidden cat-id','multiOptions'=>$cat));
            
            // $this->addElement('hidden','categoria_id');
        }

        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));

        if($this->section=='cases'){
            $this->addElement('textarea','olho',array('label'=>'Descrição','class'=>'txt'));
            $this->addElement('textarea','descricao',array('label'=>'Texto','class'=>'txt'));
            // $this->addElement('textarea','descricao',array('label'=>'Descrição','class'=>'txt'));
            // $this->addElement('textarea','depoimento',array('label'=>'Depoimento do cliente','class'=>'txt'));
            // $this->addElement('text','depoimento_nome',array('label'=>'Nome do cliente','class'=>'txt'));
        } else {
            $this->addElement('textarea','descricao',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        }
        
        if($this->section=='portfolio'){
            $this->addElement('text','cliente',array('label'=>'Cliente','class'=>'txt'));
            $this->addElement('text','ano',array('label'=>'Ano','class'=>'txt mask-int','maxlength'=>4));
        }

        if($this->section=='projetos'){
            $this->addElement('textarea','processo1',array('label'=>'Texto "Inscreva seu projeto"','class'=>'txt'));
            $this->addElement('textarea','processo2',array('label'=>'Texto "Estimativa"','class'=>'txt'));
            $this->addElement('textarea','processo3',array('label'=>'Texto "Proposta e execução"','class'=>'txt'));
            $this->addElement('textarea','textofinal',array('label'=>'Texto final','class'=>'txt'));
        }
        
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

