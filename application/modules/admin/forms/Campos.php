<?php

class Admin_Form_Campos extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/campos/save')
             ->setAttrib('id','frm-campos')
             ->setAttrib('name','frm-campos');
        
        // elementos
        // $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('hidden','alias');
        // $this->addElement('text','autor',array('label'=>'Autor','class'=>'txt'));
        // $this->addElement('text','link',array('label'=>'Link','class'=>'txt'));
        // $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','body',array('label'=>'Conteúdo','class'=>'txt wysiwyg'));
        // $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        // $this->removeDecs();
    }

    public function campos($campos)
    {
        foreach ($campos as $campo) {
            $this->addElement('text','valor',array('label'=>$campo->titulo.':','value'=>$campo->valor,'class'=>'txt'));
            $this->addElement('hidden','titulo',array('value'=>$campo->titulo));
            $this->addElement('hidden','id',array('value'=>$campo->id));
        }

        return $this;
    }

}

