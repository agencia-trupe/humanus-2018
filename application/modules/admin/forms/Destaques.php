<?php

class Admin_Form_Destaques extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/destaques/save/')
             ->setAttrib('id','frm-destaques')
             ->setAttrib('name','frm-destaques');
        
        $destaques = new Application_Model_Db_Destaques();

        // elementos
        // $this->addElement('select','tipo',array('label'=>'Tipo','class'=>'txt','multiOptions'=>$destaques->getTipos()));
        $this->addElement('radio','tipo',array('label'=>'Tipo','class'=>'','multiOptions'=>$destaques->getTipos()));
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('hidden','alias');
        $this->addElement('text','video',array('label'=>'Video','class'=>'txt','placeholder'=>'Youtube / Vimeo'));
        // $this->addElement('text','url',array('label'=>'Link','class'=>'txt'));
        // $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt'));
        // $this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        // $this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

