<?php

class Admin_Form_Portfolio extends ZendPlugin_Form
{
    public $section = 'projetos';

    public function __construct($section='projetos')
    {
        $this->section = $section;
        return parent::__construct();
    }

    public function init()
    {
		$section = $this->section;
		
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/'.$section.'/save')
             ->setAttrib('id','frm-'.$section.'')
             ->setAttrib('name','frm-'.$section.'');
		
        // elementos
        if($this->section=='cases1'){
            // combo paginas
            $db_paginas = new Application_Model_Db_Paginas();
            $paginas = $db_paginas->getKeyValues('titulo',true,'ordem','has_cases=1');
            $this->addElement('select','pagina_id',array('label'=>'Página:','class'=>'txt','multiOptions'=>$paginas));
            
            // combo categorias
            $_categorias = $db_paginas->q('select * from categorias_cases where pagina_id is not null order by ordem');
            $categorias = array();
            
            foreach($_categorias as $cat) {
                if(!isset($categorias[$cat->pagina_id])) $categorias[$cat->pagina_id] = array('__none__'=>'Selecione...');
                $categorias[$cat->pagina_id][$cat->id] = $cat->descricao;
            }
            
            foreach($categorias as $k => $cat)
                $this->addElement('select','categoria_id_'.$k,array('label'=>'Categoria:','class'=>'txt hidden cat-id','multiOptions'=>$cat));
            
            $this->addElement('hidden','categoria_id');
        }

        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));

        if($this->section=='cases'){
            $this->addElement('textarea','olho',array('label'=>'Descrição','class'=>'txt'));
            $this->addElement('textarea','descricao',array('label'=>'Texto','class'=>'txt wysiwyg'));
            // $this->addElement('textarea','descricao',array('label'=>'Descrição','class'=>'txt'));
            // $this->addElement('textarea','depoimento',array('label'=>'Depoimento do cliente','class'=>'txt'));
            // $this->addElement('text','depoimento_nome',array('label'=>'Nome do cliente','class'=>'txt'));
        } else {
            $this->addElement('textarea','descricao',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        }
        
        if($this->section=='portfolio'){
            $this->addElement('text','cliente',array('label'=>'Cliente','class'=>'txt'));
            $this->addElement('text','ano',array('label'=>'Ano','class'=>'txt mask-int','maxlength'=>4));
        }

        if($this->section=='projetos'){
            $this->addElement('textarea','processo1',array('label'=>'Texto "Inscreva seu projeto"','class'=>'txt'));
            $this->addElement('textarea','processo2',array('label'=>'Texto "Estimativa"','class'=>'txt'));
            $this->addElement('textarea','processo3',array('label'=>'Texto "Proposta e execução"','class'=>'txt'));
            $this->addElement('textarea','textofinal',array('label'=>'Texto final','class'=>'txt'));
        }
        
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

