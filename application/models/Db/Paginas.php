<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_Paginas extends ZendPlugin_Db_Table 
{
    protected $_name = "paginas";
    
    protected $_dependentTables = array('Aluno_Model_Pessoa');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pessoa' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pessoa',
            'refColumns'    => 'usuario_id'
        )
    );

    /**
     * getLastOrdem - seleciona último valor de ordenação para o menu
     * 
     * @param int $parent_id - [optional] valor da página pai
     * 
     * @return int - n da última ordem inserida naquele contexto
     */
    public function getLastOrdem($parent_id='0')
    {
        $q = 'select max(ordem) as ord from paginas where parent_id in ('.$parent_id.');';
        $row = $this->q($q);
        return (int)$row[0]->ord;
    }

    /**
     * getNextOrdem - seleciona próximo valor de ordenação para o menu com base em getLastOrdem
     * 
     * @param int $parent_id - [optional] valor da página pai
     * 
     * @return int - n da próxima ordem no contexto
     */
    public function getNextOrdem($parent_id='0')
    {
        return $this->getLastOrdem($parent_id) + 1;
    }

    public function getPaginaWithFotos($id,$parent_id='0')
    {
        $row = Is_Array::utf8DbRow($this->fetchRow(
            'status_id = 1 '.
            'and parent_id = "'.$parent_id.'" '.
            'and (id = "'.$id.'" or alias = "'.$id.'")'
        ));

        if(!(bool)$row) return null;

        $row->fotos = $this->getFotos($row->id);
        $row->links = $this->getLinks($row->id);
        $row->paginas = Is_Array::utf8DbResult($this->fetchAll(
            'status_id=1 and parent_id = "'.$row->id.'"',
            'titulo'
        ));

        return $row;
    }

    /**
     * isSubOf - verifica se a página é filha em qualquer nível
     * 
     * @param int   $parent_id - id pai para verificação
     * @param array $data      - row com dados para verificacao
     * 
     * @return bool
     */
    public function isSubOf($parent_id,$data)
    {
        if(!is_object($data)) $data = (object)$data;
        $is_sub = false;
        
        // verificando sub de primeiro nível
        // if(@$data->parent_id == $parent_id) $is_sub = true;
        if(in_array(@$data->parent_id, explode(',',$parent_id))) $is_sub = true;

        // verificando sub de segundo nível
        $sub_ids = array();
        $rows = $this->q('select id from paginas where parent_id in ('.$parent_id.')');
        foreach ($rows as $row) $sub_ids[] = $row->id;
        if(in_array(@$data->parent_id, $sub_ids)) $is_sub = true;

        return $is_sub;
    }

    public function getMenuSub($pagina)
    {
        $config_menu = array(); $ids_sub = array();
        $pagina_id = $pagina->id;
        $rows_sub = $this->q(
            'select p.* from paginas p '.
            'where p.status_id = 1 and parent_id = "'.$pagina_id.'" '.
            'order by p.parent_id, p.ordem, p.titulo '
        );

        if((bool)$rows_sub) foreach($rows_sub as $rs) $ids_sub[] = $rs->id;
        $rows_subsub = count($ids_sub) ? $this->q(
            'select p.* from paginas p '.
            'where p.status_id = 1 and parent_id in ('.implode(',', $ids_sub).') '.
            'order by p.parent_id, p.ordem, p.titulo '
        ) : array();
        
        $i = 0;

        if((bool)$rows_sub) foreach($rows_sub as $rs) {
            $has_sub = false; $_pages = array();
            foreach($rows_subsub as $rss) if($rss->parent_id == $rs->id) {
                $has_sub = true;
                $_pages['paginas-'.$rs->id.'-'.$rss->id] = array(
                    'label' => ($rss->titulo),
                    'title' => ($rss->titulo),
                    //'uri'   => '#',
                    'uri'   => URL.'/'.$pagina->alias.'/'.$rs->alias.'/'.$rss->alias,
                    'id'    => 'paginas-'.$rs->id.'-'.$rss->id,
                    'alias' => $rss->alias,
                );
            }

            $config_menu['paginas-'.$rs->id] = array(
                'label' => ($rs->titulo),
                'title' => ($rs->titulo),
                //'uri'   => '#',
                'uri'   => URL.'/'.$pagina->alias.'/'.$rs->alias,
                'id'    => 'paginas-'.$rs->id,
                'alias' => $rs->alias,
                'class' => $rs->alias,
            );
            if($has_sub) $config_menu['paginas-'.$rs->id]['class'].= ' has-sub';
            if($has_sub) $config_menu['paginas-'.$rs->id]['pages'] = $_pages;
        }

        $config_menu = new Zend_Navigation($config_menu);
        return $config_menu;
    }

    public function getParentsAlias($row)
    {
        $aliases = array($row->alias);

        if((bool)(int)$row->parent_id) {
            while((int)$row->parent_id!=0){
                $row = $this->fetchRow('id = "'.$row->parent_id.'"');
                $aliases[] = $row->alias;
            }
        }

        return array_reverse($aliases);
    }

    public function getPaginas($ids,$withFotos=true,$parent_id=null)
    {
        if(is_array($ids)) $ids = implode(',', $ids);
        if(!(bool)trim($ids)) $ids = '0';

        return $this->getPaginasWhere(
            'status_id = 1 and (id in ('.$ids.')) '.
                (($parent_id) ? 'and parent_id = "'.$parent_id.'" ' : ''),
            $withFotos,
            $parent_id
        );
    }

    public function getPaginasWhere($where='status_id=1',$withFotos=true,$parent_id=null)
    {
        $_paginas = Is_Array::utf8DbRows($this->fetchAll($where,'ordem'));
        if(!$_paginas) return null;

        if($withFotos){
            $ids = array(); // pegando ids de paginas
            foreach ($_paginas as $pag) $ids[] = $pag->id;
            $ids = (count($ids)) ? implode(',', $ids) : '0';
            $_pfs = $this->getFotos($ids); // fotos
            $_pffs = $this->getFotosFixas($ids,true); // fotos_fixas multi

            $pf = array(); // paginas_fotos
            foreach($_pfs as $_pf) {
                if(!isset($pf[$_pf->pagina_id])) $pf[$_pf->pagina_id] = array();
                $pf[$_pf->pagina_id][] = $_pf;
            }

            $pff = array(); // paginas_fotos_fixas
            foreach($_pffs as $_pff) {
                if(!isset($pff[$_pff->pagina_id])) $pff[$_pff->pagina_id] = array();
                $pff[$_pff->pagina_id][] = $_pff;
            }
        }
        
        $paginas = array();
        foreach($_paginas as $_pag) {
            $pag = $_pag;
            $pag->fotos = @$pf[$pag->id];
            $pag->fotos_fixas = @$pff[$pag->id];
            if((bool)$pag->fotos_fixas) $pag->foto_fixa = $pag->fotos_fixas[0];

            $paginas[$_pag->id] = $pag;
        }

        return $paginas;
    }

    public function getPagina($alias,$withFotos=true,$parent_id=null)
    {
        $pagina = Is_Array::utf8DbRow($this->fetchRow(
            'status_id = 1 and (alias = "'.$alias.'" or id = "'.$alias.'") '.
            (($parent_id) ? 'and parent_id = "'.$parent_id.'" ' : '')
        ));
        if(!$pagina) return null;

        if($withFotos){
            // $pagina->fotos = ($pagina->allow_photos) ? $this->getFotos($pagina->id) : null;
            $pagina->fotos = $this->getFotos($pagina->id);
            $pagina->fotos_fixas = $this->getFotosFixas($pagina->id);
        }

        return $pagina;
    }

    public function getFotos($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('f.ordem');
            // ->order('tf.id asc');
        
        if($id) $select->where('pagina_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    public function getFotosFixas($id=null,$multi=false)
    {
    	$select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos_fixas as tf')
            ->order('tf.id asc');
        
        if($id) $select->where('pagina_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $multi ? $fotos : $fotos[0];
    }

    public $model_row_alias = '__empty__';
    public $model_row9 = array(
        'has_body' => 1,
        'has_olho' => 1,
        'status_id'=> 1,
    );
    public $model_row9_fotos = array(
        'qtde_fotos' => 1,
        'foto1_text' => 'Imagem Logo',
    );

    public function insertFromModel($pid=null)
    {
        if(!$pid) return null;
        if(!@$this->fotos_fixas) $this->fotos_fixas = new Application_Model_Db_PaginasFotosFixas();

        if($pid==9){
            $dados = $this->model_row9;
            $dados['parent_id'] = $pid;
            $dados['alias']     = $this->model_row_alias;
            $dados['ordem']     = $this->getNextOrdem($pid);
            $dados['data_cad']  = date('Y-m-d H:i:s');
            $dados['data_edit'] = date('Y-m-d H:i:s');
            $id = $this->insert($dados);
            
            $dados_fotos = $this->model_row9_fotos;
            $dados_fotos['pagina_id'] = $id;
            $this->fotos_fixas->insert($dados_fotos);

            return $id;
        }
        
        return null;
    }

    public function checkEmpty()
    {
        return $this->get($this->model_row_alias,'alias');
    }
}