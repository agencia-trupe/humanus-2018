<?php

class Application_Model_Db_Tecnologias extends ZendPlugin_Db_Table 
{
    protected $_name = "tecnologias";
    
    /**
     * Retorna registro por alias
     * 
     * @param array $rows - rowset para buscar, caso não seja passado busca na tabela
     */
    public function getByAlias($alias,$rows=null)
    {
        if(!$rows) return $this->fetchRow('alias = "'.$alias.'"');

        $find = null;
        
        foreach($rows as $row) if($row->alias == $alias) $find = $row;

        return $find;
    }

    /**
     * Retorna as fotos do da tecnologia
     *
     * @param int $id - id da tecnologia
     *
     * @return array - rowset com fotos da tecnologia
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('tecnologias_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('tecnologia_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos tecnologias
     * 
     * @param array $tecnologias - rowset de tecnologias para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - tecnologias com fotos
     */
    public function getFotos($tecnologias,$withObjects=false)
    {
        $pids = array(); // ids de tecnologias

        // identificando tecnologias
        foreach($tecnologias as $tecnologia) $pids[] = $tecnologia->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('tecnologias_fotos as fc',array('fc.tecnologia_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.tecnologia_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $eTecnologias_fotos = new Application_Model_Db_TecnologiasFotos();
            $fotos = $eTecnologias_fotos->fetchAll('tecnologia_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($tecnologias as &$tecnologia){
            $tecnologia->fotos = $this->getFotosSearch($tecnologia->id,$fotos);
        }

        return $tecnologias;
    }

    /**
     * Monta rowset de fotos com base no tecnologia_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->tecnologia_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}