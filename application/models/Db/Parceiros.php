<?php

class Application_Model_Db_Parceiros extends ZendPlugin_Db_Table 
{
    protected $_name = "parceiros";
    protected $_foto_join_table = 'parceiros_fotos';
    protected $_foto_join_table_field = 'parceiro_id';
    
    /**
     * Retorna registro por alias
     * 
     * @param array $rows - rowset para buscar, caso não seja passado busca na tabela
     */
    public function getByAlias($alias,$rows=null)
    {
        if(!$rows) return $this->fetchRow('alias = "'.$alias.'"');

        $find = null;
        
        foreach($rows as $row) if($row->alias == $alias) $find = $row;

        return $find;
    }

    /**
     * Retorna as fotos do da parceiro
     *
     * @param int $id - id da parceiro
     *
     * @return array - rowset com fotos da parceiro
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('parceiros_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('parceiro_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos parceiros
     * 
     * @param array $parceiros - rowset de parceiros para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - parceiros com fotos
     */
    public function getFotos($parceiros,$withObjects=false)
    {
        $pids = array(); // ids de parceiros

        // identificando parceiros
        foreach($parceiros as $parceiro) $pids[] = $parceiro->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('parceiros_fotos as fc',array('fc.parceiro_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.parceiro_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $eParceiros_fotos = new Application_Model_Db_ParceirosFotos();
            $fotos = $eParceiros_fotos->fetchAll('parceiro_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($parceiros as &$parceiro){
            $parceiro->fotos = $this->getFotosSearch($parceiro->id,$fotos);
        }

        return $parceiros;
    }

    /**
     * Monta rowset de fotos com base no parceiro_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->parceiro_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}