<?php

class Application_Model_Db_ModulosFotos extends Zend_Db_Table
{
    protected $_name = "modulos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Modulos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Modulos' => array(
            'columns' => 'modulo_id',
            'refTableClass' => 'Application_Model_Db_Modulos',
            'refColumns'    => 'id'
        )
    );
}
