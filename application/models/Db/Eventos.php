<?php

class Application_Model_Db_Eventos extends ZendPlugin_Db_Table 
{
    protected $_name = "eventos";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }

    /**
     * Retorna próximos eventos
     */
    public function getNexts($limit=null,$where_opt=null,$datai=null)
    {
        $data = $datai ? date('Y-m-d',strtotime($datai)) : date('Y-m-d');
        $where = 'data >= "'.$data.'" ';
        if($where_opt) $where.= $where_opt;

        return $this->fetchAll($where,'data',$limit);
    }

    /**
     * Retorna as fotos do da evento
     *
     * @param int $id - id da evento
     *
     * @return array - rowset com fotos da evento
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('eventos_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('evento_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos eventos
     * 
     * @param array $eventos - rowset de eventos para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - eventos com fotos
     */
    public function getFotos($eventos,$withObjects=false)
    {
        $pids = array(); // ids de eventos

        // identificando eventos
        foreach($eventos as $evento) $pids[] = $evento->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('eventos_fotos as fc',array('fc.evento_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.evento_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $eEventos_fotos = new Application_Model_Db_EventosFotos();
            $fotos = $eEventos_fotos->fetchAll('evento_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($eventos as &$evento){
            $evento->fotos = $this->getFotosSearch($evento->id,$fotos);
        }

        return $eventos;
    }

    /**
     * Monta rowset de fotos com base no evento_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->evento_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}