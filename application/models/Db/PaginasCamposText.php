<?php

class Application_Model_Db_PaginasCamposText extends ZendPlugin_Db_Table 
{
    protected $_name = "paginas_campos_text";
    // protected $_foto_join_table = 'campos_fotos';
    // protected $_foto_join_table_field = 'campos_id';
    
    /**
     * Retorna registro por alias
     * 
     * @param array $rows - rowset para buscar, caso não seja passado busca na tabela
     */
    public function getByAlias($alias,$rows=null)
    {
        if(!$rows) return $this->fetchRow('alias = "'.$alias.'"');

        $find = null;
        
        foreach($rows as $row) if($row->alias == $alias) $find = $row;

        return $find;
    }

    /**
     * Retorna as fotos do da campo
     *
     * @param int $id - id da campo
     *
     * @return array - rowset com fotos da campo
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('campos_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('campo_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos campos
     * 
     * @param array $campos - rowset de campos para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - campos com fotos
     */
    public function getFotos($campos,$withObjects=false)
    {
        $pids = array(); // ids de campos

        // identificando campos
        foreach($campos as $campo) $pids[] = $campo->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('campos_fotos as fc',array('fc.campo_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.campo_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $eClientes_fotos = new Application_Model_Db_ClientesFotos();
            $fotos = $eClientes_fotos->fetchAll('campo_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($campos as &$campo){
            $campo->fotos = $this->getFotosSearch($campo->id,$fotos);
        }

        return $campos;
    }

    /**
     * Monta rowset de fotos com base no campo_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->campo_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}