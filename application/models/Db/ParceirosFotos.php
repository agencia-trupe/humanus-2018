<?php

class Application_Model_Db_ParceirosFotos extends Zend_Db_Table
{
    protected $_name = "parceiros_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Parceiros');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Parceiros' => array(
            'columns' => 'parceiro_id',
            'refTableClass' => 'Application_Model_Db_Parceiros',
            'refColumns'    => 'id'
        )
    );
}
