<?php

class Application_Model_Db_Videos extends ZendPlugin_Db_Table 
{
    protected $_name = "videos";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
    /**
     * Retorna as fotos do video
     *
     * @param int $id - id do video
     *
     * @return array - rowset com fotos do video
     */
    public function getFotos($id)
    {
        if(!$video = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($video_fotos = $video->findDependentRowset('Application_Model_Db_VideosFotos')){
            foreach($video_fotos as $video_foto){
                $fotos[] = Is_Array::utf8DbRow($video_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    public function getLast()
    {
        $video = $this->getLasts(1);
        return $video[0];
    }

    public function getLasts($limit=3,$order='data_edit desc')
    {
        $_videos = $this->fetchAll('status_id=1',$order,$limit);
        
        if(count($_videos)){
            $videos = $this->_autoUtf8 ? $_videos :
                Is_Array::utf8DbResult($_videos);
            
            foreach($videos as &$video){
                $video->fotos = $this->getFotos($video->id);
                $video->foto = @$video->fotos[0]->path;
            }
            
            return $videos;
        }

        return null;
    }
    
}