<?php

class Application_Model_Db_EventosFotos extends Zend_Db_Table
{
    protected $_name = "eventos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Eventos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Eventos' => array(
            'columns' => 'evento_id',
            'refTableClass' => 'Application_Model_Db_Eventos',
            'refColumns'    => 'id'
        )
    );
}
