<?php

class Application_Model_Db_Cases extends ZendPlugin_Db_Table 
{
    protected $_name = "cases";
    protected $_foto_join_table = 'cases_fotos';
    protected $_foto_join_table_field = 'case_id';
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Categorias',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Categorias' => array(
            'columns' => 'categoria_id',
            'refTableClass' => 'Application_Model_Db_Categorias',
            'refColumns'    => 'id'
        ),
    );
    
    /**
     * Retorna produto com suas imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias - valor do alias ou id do produto
     *
     * @return object|bool - objeto contendo o produto com suas imagens e categoria ou false se não for encontrado
     */
    public function getWithFotos($alias)
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        $object = $this->q('select * from '.$this->_name.' where '.$column.' = "'.$alias.'"');
        
        if(!$object) return null;
        $object = $object[0];
        
        $_fotos = $this->q('select foto_id from '.$this->_foto_join_table.' where '.$this->_foto_join_table_field.' = "'.$object->id.'"');
        $fids = array();

        if((bool)$_fotos) foreach($_fotos as $f) $fids[] = $f->foto_id;
        else $fids = array(0);

        $object->fotos = $this->q('select * from fotos where id in('.implode(',',$fids).')');
        return $object;
    }

    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos($id)
    {
        if(is_object($id)){
            $produto = $id;
        } else {
            if(!$produto = $this->fetchRow('id="'.$id.'"')){
                return array();
            }
        }
        
        $fotos = $this->q( // pegando fotos
            'select f.*, pf.case_id from cases_fotos pf '.
            'left join fotos f on f.id = pf.foto_id '.
            'where pf.case_id = '.$produto->id.' '.
            // 'where pf.case_id in ('.(count($pids) ? implode(',',$pids) : '0').') '.
            'order by f.ordem'
        );
        
        return $fotos;
    }
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos1($id)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }
    
    public function hasChildren($id)
    {
        return (bool) $this->fetchRow('parent_id='.$id);
    }
    
    public function getChildren($id,$order='ordem')
    {
        return $this->fetchAll('parent_id='.$id,$order);
    }

    public function getLastProjects($count=null,$where=null)
    {
        return $this->q(
            // 'select p.titulo, p.alias, p.cliente, f.path from portfolio p '.
            'select p.titulo, p.alias, p.olho, p.thumbnail from cases p '.
            // 'left join portfolio_fotos pf on pf.portfolio_id = p.id '.
            // 'left join fotos f on f.id = pf.foto_id '.
            'where p.status_id = 1 '.
            ($where!==null ? 'and '.$where.' ' : ' ').
            'group by p.id '.
            'order by p.destaque desc, p.ano desc, p.data_edit desc '.
            ($count!==null ? 'limit '.$count.' ' : ' ')
        );
    }

    /**
     * getLastOrdem - seleciona último valor de ordenação para o menu
     * 
     * @param int $parent_id - [optional] valor da página pai
     * 
     * @return int - n da última ordem inserida naquele contexto
     */
    public function getLastOrdem($parent_id=null)
    {
        $q = 'select max(ordem) as ord from cases where ';
        $q.= ((bool)$parent_id) ? 
            'pagina_id in ('.$parent_id.');':
            'pagina_id is null;';
        $row = $this->q($q);
        return (int)$row[0]->ord;
    }

    /**
     * getNextOrdem - seleciona próximo valor de ordenação para o menu com base em getLastOrdem
     * 
     * @param int $parent_id - [optional] valor da página pai
     * 
     * @return int - n da próxima ordem no contexto
     */
    public function getNextOrdem($parent_id=null)
    {
        return $this->getLastOrdem($parent_id) + 1;
    }
}