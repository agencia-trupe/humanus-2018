<?php

class Application_Model_Db_SegmentosFotos extends Zend_Db_Table
{
    protected $_name = "segmentos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Segmentos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Segmentos' => array(
            'columns' => 'segmento_id',
            'refTableClass' => 'Application_Model_Db_Segmentos',
            'refColumns'    => 'id'
        )
    );
}
