<?php

class Application_Model_Db_Treinamentos extends ZendPlugin_Db_Table 
{
    protected $_name = "treinamentos";
    public static $generos = array(
        0 => 'Feminino',
        1 => 'Masculino',
        2 => 'Unissex',
        3 => 'Casais'
    );
    public $status_pago = '3,4,8,9,10,11';
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Categorias','Application_Model_Db_TreinamentosSugestoes','Application_Model_Db_SugestoesItems');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Categorias' => array(
            'columns' => 'categoria_id',
            'refTableClass' => 'Application_Model_Db_Categorias',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_TreinamentosSugestoes' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_TreinamentosSugestoes',
            'refColumns'    => 'sugestao_id'
        ),
        'Application_Model_Db_SugestoesItems' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_SugestoesItems',
            'refColumns'    => 'treinamento_id'
        ),
    );

    public function getNexts($ativos=1,$order=null,$limit=null,$offset=null)
    {
        $produtos = $this->q(
            'select t2.data, t2.treinamento_id, t2.titulo as produto_titulo, '.
                't2.id as produto_id, t2.cidade as produto_cidade, t2.uf as produto_uf '.
            'from produtos t2 '.
                'left join treinamentos t1 on t1.id=t2.treinamento_id '.
            'where 1=1 '.
                (($ativos)?
                'and t2.data >= "'.date('Y-m-d').'" '.
                'and t2.data_final >= "'.date('Y-m-d').'" '.
                // 'and t2.status_id = "'.((int)$ativos).'" ' : '').
                'and t1.status_id in ('.($ativos).') '.
                'and t2.status_id in ('.($ativos).') ' : '').
            // 'group by t2.treinamento_id '.
            // ($order ? 'order by '.(is_array($order) ? implode(',',$order) : $order).' ' : '').
            // 'order by id asc '.
            'order by '.($order ? (is_array($order) ? implode(',',$order) : $order).' ' : 't2.id asc ').
            ($limit ? 'limit '.($offset ? $offset.',' : '').$limit : '')
        );
        // _d($produtos);

        $tids = array(); $pids = array();
        if($produtos) foreach($produtos as $p) if((bool)trim($p->treinamento_id)){
            $tids[] = $p->treinamento_id;
            if(!isset($pids[$p->treinamento_id])) $pids[$p->treinamento_id] = array();
            $pids[$p->treinamento_id][] = $p;
        }
        // _d($tids);
        // _d($pids);

        if(!count($tids)) return array();

        $treinamentos = $this->q(
            'select t1.* '.
            'from '.$this->_name.' as t1 '.
            'where 1=1 '.
                'and t1.id in ('.implode(',', $tids).') '.
            'order by t1.id '.
            // ($order ? 'order by '.(is_array($order) ? implode(',',$order) : $order).' ' : '').
            ($limit ? 'limit '.($offset ? $offset.',' : '').$limit : '')
        );
        // _d($treinamentos);

        $rows = array();
        // if($treinamentos) foreach($treinamentos as $t){
        if($treinamentos) foreach($treinamentos as $_t) foreach($pids[$_t->id] as $_pt){
        // if($treinamentos) foreach($pids as $_k => $_pt) foreach($treinamentos as $_t) if($_k==$_t->id){
        // if($treinamentos) foreach($pids as $_pt){
            // $p = $pids[$t->id];
            // _d($_t->id.':'.$_pt->produto_id,false);
            $p = new StdClass(); $t = new StdClass();
            // $p = $_pt;
            // $t = $_t;
            $t->treinamento = $_t;
            $t->treinamento_titulo = $_t->titulo;
            $t->data = $_pt->data;
            $t->produto_titulo = $_pt->produto_titulo;
            $t->produto_id = $_pt->produto_id;
            $t->produto_cidade = $_pt->produto_cidade;
            $t->produto_uf = $_pt->produto_uf;
            
            // $rows[] = $t;
            $_row_order = date('Ymd', strtotime($_pt->data));
            $_row_order_cnt = 0;
            $row_order = $_row_order.$_row_order_cnt;
            while(array_key_exists($row_order, $rows)){
                $_row_order_cnt++;
                $row_order = $_row_order.$_row_order_cnt;
            }
            $rows[$row_order] = $t;
        }
        ksort($rows);
        // _d($rows);

        return $rows;

        // return $this->q(
        //     'select distinct t1.*, t2.data '.
        //     'from '.$this->_name.' as t1 '.
        //         'left join produtos t2 on t2.treinamento_id = t1.id '.
        //     'where 1=1 '.
        //         'and t2.data >= "'.date('Y-m-d').'" '.
        //         'and t2.data_final >= "'.date('Y-m-d').'" '.
        //         'and t1.status_id = "'.((int)$ativos).'" '.
        //     'group by t2.treinamento_id '.
        //     ($order ? 'order by '.(is_array($order) ? implode(',',$order) : $order).' ' : '').
        //     ($limit ? 'limit '.($offset ? $offset.',' : '').$limit : '')
        // );
    }

    public function getGeneros()
    {
        return self::$generos;
    }

    public function getGenerosAssoc($tolower=false)
    {
        $generos = array();

        foreach(self::$generos as $k=>$v) $generos[($tolower)?strtolower($v):$v] = $k;

        return $generos;
    }

    public function getGenero($key=0)
    {
        return self::$generos[$key];
    }
    
    /**
     * Retorna treinamento com suas imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias - valor do alias ou id do treinamento
     *
     * @return object|bool - objeto contendo o treinamento com suas imagens e categoria ou false se não for encontrado
     */
    public function getWithFotos($id)
    {
        $column = is_numeric($id) ? 'id' : 'alias';
        if(!$treinamento = $this->fetchRow($column.'="'.$id.'"')){
            return false;
        }

        $treinamento = Is_Array::utf8DbRow($treinamento);
        $treinamento->fotos = $this->getFotos($treinamento->id);
        $treinamento->categorias = $this->getCategorias($treinamento->id,true);
        $treinamento->categoria = @$treinamento->categorias[0];
        
        // descontos
        $treinamento = self::checkDesconto($treinamento);
        
        return $treinamento;
    }
    
    public function getDestaques($limit=3)
    {
        if($treinamentos = $this->fetchAll('destaque = 1','data_edit desc',$limit)){
            $destaques = array();
            
            foreach($treinamentos as $treinamento){
                $p = Is_Array::utf8DbRow($treinamento);
                $p->fotos = $this->getFotos($p->id);
                $destaques[] = $p;
            }
            
            return $destaques;
        }
        
        return false;
    }
    
    /**
     * Retorna as fotos do treinamento v1 (somente fotos)
     *
     * @param int $id - id do treinamento
     *
     * @return array - rowset com fotos do treinamento
     */
    public function getFotos1($id,$cor=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $column = is_numeric($id) ? 'id' : 'alias';
        $rows = array();

        $select->from('treinamentos_fotos as pf',array())
            ->joinLeft('fotos as f','f.id = pf.foto_id',array('*'))
            ->order(null);

        if($column == 'alias'){
            $select->joinLeft('treinamentos as p','p.id = pf.treinamento_id',array())
                ->where('p.alias = "'.$id.'"');
        } else {
            $select->where('pf.treinamento_id = '.$id);
        }

        $_rows = $select->query()->fetchAll();
        foreach($_rows as $k=>&$v) $rows[] = Is_Array::toObject(Is_Array::utf8All($v));
        // _d($rows);
        return $rows;
    }
    
    /**
     * Retorna as fotos do treinamento v2 (fotos com cores)
     *
     * @param string $id - ids dos treinamentos para select in ()
     *
     * @return array - rowset com fotos
     */
    public function getFotos2($id,$cor=null)
    {
        $_pfotos = new Application_Model_Db_TreinamentosFotos();
        $_fotos  = new Application_Model_Db_Fotos();
        $_cores  = new Application_Model_Db_Cores();
        $fotos   = array();

        if($treinamento_fotos = $_pfotos->fetchAll('treinamento_id in ('.$id.')')){
            foreach($treinamento_fotos as $treinamento_foto){
                $f = Is_Array::utf8DbRow($treinamento_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
                $f->cor_id = $treinamento_foto->cor_id ? $treinamento_foto->cor_id : null;
                $f->cor    = $treinamento_foto->cor_id ? Is_Array::utf8DbRow($treinamento_foto->findDependentRowset('Application_Model_Db_Cores')->current()) : null;
                
                if($cor!==null) {
                    if ($cor==$f->cor_id) $fotos[] = $f;
                } else {
                    $fotos[] = $f;
                }
                
                $fotos[] = $f;
            }
        }
        _d($fotos);
        return $fotos;
    }

    /**
     * Retorna as fotos do treinamento v3 (fotos com cores otimizado)
     *
     * @param string $id - ids dos treinamentos para select in ()
     *
     * @return array - rowset com fotos
     */
    public function getFotos($id,$cor=null)
    {
        $fotos = array();
        $frows = $this->q(
            'select f.*, pf.cor_id, '.
                'c.id as cor_id, c.descricao as cor_descricao, c.alias as cor_alias, c.cod as cor_cod '.
            'from treinamentos_fotos as pf '.
                'left join fotos as f on f.id = pf.foto_id '.
                'left join cores as c on c.id = pf.cor_id '.
            'where pf.treinamento_id = '.$id.' '.
                ($cor ? 'and pf.cor_id = '.$cor.' ' : '').
            'order by pf.id '
        );

        foreach($frows as $frow){
            $cor = null;
            
            if((bool)$frow->cor_id){
                $cor = new stdClass();
                
                foreach (get_object_vars($frow) as $key => $value) {
                    if(strstr($key,'cor_')) {
                        $cor->{str_replace('cor_','',$key)} = $value;
                    }
                }
            }

            $frow->cor = $cor;
            $fotos[] = $frow;
        }
        
        return $fotos;
    }
    
    /**
     * Retorna as sugestoes do treinamento
     *
     * @param int  $id        - id do treinamento
     * @param bool $get_fotos - selecionar também fotos?
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do treinamento
     */
    public function getSugestoes($id,$get_fotos=false,$limit=null,$rand=false)
    {
        // saímos se o treinamento não existir
        if(!$treinamento = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $sugestoes = array();
        
        // montando a array de sugestões
        $select = $rand ? // randomizamos os resultados se for solicitado
                  $this->select()->order(new Zend_Db_Expr('RAND()'))->limit($limit) :
                  null;
        if($treinamento_sugestoes = $treinamento->findDependentRowset('Application_Model_Db_TreinamentosSugestoes',null,$select)){
            foreach($treinamento_sugestoes as $treinamento_sugestao){
                $sugestao = Is_Array::utf8DbRow($treinamento_sugestao->findDependentRowset('Application_Model_Db_Treinamentos')->current());
                $sugestao = $this->checkDesconto($sugestao);
                $sugestoes[] = $sugestao;
            }
        }

        // pegando categorias p/ adicionar ao objeto do treinamento
        $this->treinamentos_categorias = new Application_Model_Db_TreinamentosCategorias();
        $pids = array(); // identificando treinamentos
        foreach($sugestoes as $sugestao) $pids[] = $sugestao->id;

        $pcategorias = $this->treinamentos_categorias->fetchAll('treinamento_id in ('.(count($pids) ? implode(',',$pids) : '0').')');

        $pcids = array();
        foreach($pcategorias as $pcategoria) $pcids[] = $pcategoria->categoria_id;
        // $categorias = $this->categorias->fetchAll('id in ('.implode(',',$pcids).')');
        
        foreach($sugestoes as &$sugestao){ // pegando categorias
            $sugestao->categorias = array();

            foreach($pcategorias as $pcategoria){
                if($pcategoria->treinamento_id == $sugestao->id){
                    $sugestao->categorias[] = $pcategoria;
                }
            }
        }
        
        if($get_fotos){ // pegamos as fotos se for solicitado
            $treinamentos_fotos = new Application_Model_Db_TreinamentosFotos();
            
            foreach($sugestoes as &$sugestao){
                $sugestao->fotos = array();
                
                if($treinamento_fotos = $treinamentos_fotos->fetchAll('treinamento_id='.$sugestao->id)){    
                    foreach($treinamento_fotos as $treinamento_foto){
                        $sugestao->fotos[] = $treinamento_foto->findDependentRowset('Application_Model_Db_Fotos')->current();
                    }
                }
            }
        }
        
        // randomizamos os resultados
        // - retirado para melhor performance caso haja muitas sugestões, solução adicionada acima
        //if($rand){ shuffle($sugestoes); }
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_name,array('count(*) as cnt'));
        $count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
        return $count[0]['cnt'];
    }
    
    /**
     * Checa descontos do treinamento (recursivo)
     *
     * $param array|object $object - Array/Objeto do treinamento para ser checado os descontos.
     *                               Não pode ser uma instância do Zend Table.
     *
     * @return array|object
     */
    public function checkDesconto($object=null)
    {
        if($object===null) return null;
        
        if(is_array($object)){ // adiciona resursão à função
            if(count($object)){
                foreach($object as &$o) $o = self::checkDesconto($o);
                return $object;
            }
            return null;
        }
        
        if((bool)(float)$object->valor_promo){ // se possui valor promocional
            $object->valor_desconto = $object->valor; // adiciona valor_desconto como o valor original
            $object->valor = $object->valor_promo; // seta valor com desconto, para ser usado como o valor do treinamento
        } else {
            $prod_cats = new Application_Model_Db_TreinamentosCategorias();
            $cats = new Application_Model_Db_Categorias();

            $pgs = $prod_cats->fetchAll('treinamento_id='.$object->id);
            $cat_ids = array();
            $descs = array();

            foreach($pgs as $pg) $cat_ids[] = $pg->categoria_id;

            $categs = $cats->fetchAll('id in ('.(count($cat_ids) ? implode(',',$cat_ids) : '0').')');

            foreach($categs as $cat) if((bool)$cat->desconto) $descs[] = $cat->desconto;

            if(count($descs)){
                $object->valor_desconto = $object->valor;
                $object->valor = Is_Math::percentDec(self::maxDesc($descs),$object->valor);
            } else {
                $object->valor_desconto = null;
            }

            /* old
            $cats = new Application_Model_Db_Categorias();
            $cat  = $cats->fetchRow('id='.$object->categoria_id);
            
            if((bool)$cat->desconto){ // se a categoria está com desconto
                $object->valor_desconto = $object->valor;
                $object->valor = Is_Math::percentDec($cat->desconto,$object->valor);
            } else {
                $object->valor_desconto = null;
            }*/
        }
        
        return $object;
    }
 
    /**
     * Atualiza contagem de views
     * 
     * @param int $id - ID do treinamento
     */
    public function addView($id)
    {
        $q = "update treinamentos set views = views + 1 where id = '".$id."'";

        $this->q($q);
        return $this;
    }

    /**
     * Retorna o maior desconto
     */
    public function maxDesc($descs)
    {
        $desc = 0;

        foreach($descs as $d) if((int) $d > $desc) $desc = (int) $d;

        return $desc;
    }

    /**
    * Retorna categorias de um rowset
    */
    public function getCategoriasFromRows($rows)
    {
        $cids = array();
        
        foreach($rows as $r) if($r->categoria_id) $cids[$r->id] = $r->categoria_id;

        if(count($cids)) {
            $t = new Application_Model_Db_Treinamentos();
            $rows_c = Is_Array::utf8DbResult($t->fetchAll(
                'id in ('.implode(',',$cids).')'
            ));

            if($rows_c) {
                // if($withCat) $rows_c = $t->getCategoriasFromRows($rows_c);

                foreach ($rows_c as $rt)
                    foreach($cids as $pid => $cid)
                        foreach($rows as &$r)
                            if($r->id==$pid && $r->categoria_id==$cid)
                                $r->categoria = $rt;
            }
        }

        return $rows;
    }

    /**
     * Retorna treinamentos por categoria selecionada
     */
    public function getByCategoria($cid,$where=null,$order=null,$limit=null,$offset=null)
    {
        return $this->q(
            'select t1.* from treinamentos_categorias t2 '.
                'left join '.$this->_name.' t1 on t1.id = t2.treinamento_id '.
            'where '.
                't2.categoria_id = "'.$cid.'" '.
                ($where ? ' and ('.$where.') ' : '').
            ($order ? 'order by '.$order : '').
            ($limit ? 'limit '.($offset ? $offset.',' : '').$limit : '')
        );
    }

    public function getCurrentByCategoria($cid,$order='id',$limit=null,$offset=null)
    {
        return $this->getByCategoria($cid,'status_id=1',$order,$limit,$offset);
    }

    /**
     * Retorna categorias do treinamento
     */
    public function getCategorias(&$treinamento,$getFullCategoria=false)
    {
        if(is_array($treinamento)){
            $treinamentos = array();

            foreach($treinamento as &$p) $treinamentos[] = $this->getCategorias($p,$getFullCategoria);

            return $treinamentos;
        }

        $treinamento_id = is_object($treinamento) ? $treinamento->id : $treinamento;
        $rows = array();

        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('treinamentos_categorias as pc',$getFullCategoria ? array('pc.treinamento_id') : array('*'))
            ->where('pc.treinamento_id in ('.$treinamento_id.')');

        if($getFullCategoria){
            $select->joinLeft('categorias as c','c.id = pc.categoria_id',array('*'));
        }

        $_rows = $select->query()->fetchAll();
        foreach($_rows as $k=>&$v) $rows[] = Is_Array::toObject(Is_Array::utf8All($v));

        if(is_object($treinamento)){
            $treinamento->categorias = $rows;
            $treinamento->categoria = $rows[0];
            return $treinamento;
        }

        return $rows;
    }

    /**
     * Retorna as tamanhos do treinamento
     *
     * @param int  $id        - id do treinamento
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do treinamento
     */
    public function getTamanhos($id,$limit=null,$rand=false)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = array();

        $select->from('treinamentos_estoques as pe',array())
            ->joinLeft('tamanhos as t','t.id = pe.tamanho_id',array('*'))
            ->where('pe.treinamento_id = '.$id)
            ->order($rand ? new Zend_Db_Expr('RAND()') : array('t.ordem','t.alias'))
            ->group('t.id')
            ->limit($limit);

        $_rows = $select->query()->fetchAll();
        foreach($_rows as $k=>&$v) $rows[] = Is_Array::toObject(Is_Array::utf8All($v));

        return $rows;
    }

    /**
     * Retorna as tamanhos do treinamento
     *
     * @param int  $id        - id do treinamento
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do treinamento
     */
    public function getTamanhosOld($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_TreinamentoTamanho();
        $_tags = new Application_Model_Db_Tamanhos();
        $sugestoes = array(); $sugestoes_ids = array();

        if(!(bool)$prods = $_sugestoes->fetchAll('treinamento_id="'.$id.'"')){
            return false;
        }

        foreach($prods as $ps) $sugestoes_ids[] = $ps->tamanho_id;

        $sugestoes = Is_Array::utf8DbResult($_tags->fetchAll('id in ('.(count($sugestoes_ids) ? implode(',',$sugestoes_ids) : 0).')','alias'));
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }

    /**
     * Retorna as voltagens do treinamento
     *
     * @param int  $id        - id do treinamento
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do treinamento
     */
    public function getVoltagens($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_TreinamentoVoltagem();
        $_tags = new Application_Model_Db_Voltagens();
        $sugestoes = array(); $sugestoes_ids = array();

        if(!(bool)$prods = $_sugestoes->fetchAll('treinamento_id="'.$id.'"')){
            return false;
        }

        foreach($prods as $ps) $sugestoes_ids[] = $ps->voltagem_id;

        $sugestoes = Is_Array::utf8DbResult($_tags->fetchAll('id in ('.(count($sugestoes_ids) ? implode(',',$sugestoes_ids) : 0).')','alias'));
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }

    /**
     * Retorna as cores do treinamento
     *
     * @param int  $id        - id do treinamento
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do treinamento
     */
    public function getCores($id,$limit=null,$rand=false)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = array();

        $select->from('treinamentos_estoques as pe',array())
            ->joinLeft('cores as c','c.id = pe.cor_id',array('*'))
            ->joinLeft('fotos as f','f.id = c.foto_id',array('path as foto_path'))
            ->where('pe.treinamento_id = '.$id)
            ->order($rand ? new Zend_Db_Expr('RAND()') : 'c.alias')
            ->group('c.id')
            ->limit($limit);

        $_rows = $select->query()->fetchAll();
        foreach($_rows as $k=>&$v) $rows[] = Is_Array::toObject(Is_Array::utf8All($v));

        return $rows;
    }

    /**
     * Retorna as cores do treinamento
     *
     * @param int  $id        - id do treinamento
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do treinamento
     */
    public function getCoresOld($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_TreinamentoCor();
        $_tags = new Application_Model_Db_Cores();
        $sugestoes = array(); $sugestoes_ids = array();

        if(!(bool)$prods = $_sugestoes->fetchAll('treinamento_id="'.$id.'"')){
            return false;
        }

        foreach($prods as $ps) $sugestoes_ids[] = $ps->cor_id;

        $sugestoes = Is_Array::utf8DbResult($_tags->fetchAll('id in ('.(count($sugestoes_ids) ? implode(',',$sugestoes_ids) : 0).')','alias'));
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }

    /**
     * Retorna estoques do treinamento
     *
     * @param int  $id        - id do treinamento
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do treinamento
     */
    public function getEstoques($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_TreinamentoEstoque();
        $sugestoes = array();

        $sql = 'select '.
               'e.id, e.tamanho_id, e.cor_id, e.estoque, '.
               't.descricao as tamanho, t.alias as tamanho_alias, t.ordem as tamanho_ordem, '.
               'c.descricao as cor, c.alias as cor_alias, c.cod as cor_cod '.
               'from treinamentos_estoques e '.
               'left join tamanhos t on t.id = e.tamanho_id '.
               'left join cores c on c.id = e.cor_id '.
               'where e.treinamento_id = "'.$id.'" ';

        $sql.= ($rand) ? ' order by rand() ' : ' order by t.ordem, t.alias ';

        if(!(bool)$sugestoes = $_sugestoes->q($sql)){
            return false;
        }

        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }

    /**
     * Retorna horarios do treinamento
     *
     * @param int  $id        - id do treinamento
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do treinamento
     */
    public function getHorarios($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_Produtos();
        $sugestoes = array();

        $sql = 'select '.
               'p.id, p.endereco, p.bairro, p.cidade, p.uf, '.
               'p.data, p.data_final, p.data_exibicao, p.valor, p.valor_grupo, p.treinamento_id, '.
               'p.vagas_disponiveis, p.vagas_preenchidas, p.status_id, p.data_cad, p.incompany, '.
               // 'p.titulo '.
               '(case when p.incompany=1 then concat("[IN COMPANY] ",p.titulo) else p.titulo end) as titulo '.
               'from produtos p '.
               'where p.treinamento_id = "'.$id.'" ';

        $sql.= ($rand) ? ' order by rand() ' : ' order by p.data ';

        if(!(bool)$sugestoes = $_sugestoes->q($sql)){
            return false;
        }

        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }

    /**
     * Retorna estoque do treinamento
     *
     * @param int  $id             - id do treinamento
     * @param int  $cor_id         - id da cor
     * @param int  $tamanho_id     - id do tamanho
     * @param bool $return_object  - retorna row do estoque?
     *
     * @return int|object - qtde estoque | row estoque
     */
    public function getEstoque($id,$cor_id=null,$tamanho_id=null,$return_object=false)
    {
        if($cor_id===null || $tamanho_id===null) return null;

        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = array();

        $select->from('treinamentos_estoques as pe',$return_object ? array('*') : array('estoque'))
            ->where('pe.treinamento_id = '.$id)
            ->where('pe.cor_id = '.$cor_id)
            ->where('pe.tamanho_id = '.$tamanho_id);

        $_rows = $select->query()->fetchAll();
        foreach($_rows as $k=>&$v) $rows[] = Is_Array::toObject(Is_Array::utf8All($v));

        if(!count($rows)) return 0;
        return $return_object ? $rows[0] : $rows[0]->estoque;
    }

    public function updateEstoque($id)
    {
        $estoque_total = $this->q('select sum(estoque) as cnt from treinamentos_estoques where treinamento_id = \''.$id.'\'');
        $estoque_total = (bool)$estoque_total ? $estoque_total[0]->cnt : 0;
        $this->update(array('estoque'=>$estoque_total),'id = \''.$id.'\'');
        return $estoque_total;
    }

    public function checkInscritosAll($treinamento_id=null)
    {
        $pids = $this->q(
            'select id from produtos where 1=1 '.
            ($treinamento_id ? 'and treinamento_id = "'.$treinamento_id.'" ' : '')
        );
        $amount = 0;

        foreach($pids as $pid) $amount+= $this->checkInscritos($pid->id);

        return $amount;
    }

    public function checkInscritos($produto_id)
    {
        $qtde = 0;
        
        $pedidos = $this->q1(
            'select sum(pi.qtde) as qtde '.
            'from pedidos p '.
            'left join pedidos_items pi on pi.pedido_id = p.id '.
            'where p.pedido_status_id in ('.$this->status_pago.') '.
            'and pi.produto_id = "'.$produto_id.'" '
        );

        if($pedidos) $qtde = $pedidos->qtde;

        $this->q('update produtos set vagas_preenchidas = "'.$qtde.'" '.
                 'where id = "'.$produto_id.'"');

        return $qtde;
    }

    public function getListaByInscrito($cliente_id=null,$produto_id=null,$pago=true,$onlyLast=false)
    {
        if(!$cliente_id) return null;

        $rows = $this->q(
            'select c.*, p.data_cad as pedido_data, max(p.id) as pedido_id'.
                ', p.pedido_forma_id, p.pedido_status_id, p.itau_DC as pedido_itau_DC'.
                ', pd.id as produto_id, pd.titulo as produto'.
                ', concat(pd.endereco," - ",pd.bairro," - ",pd.cidade,", ",pd.uf) as produto_local '.
                ', date_format(pd.data,"%d/%m/%Y") as produto_data'.
                ', date_format(pd.data_final,"%d/%m/%Y") as produto_data_final'.
                ', pd.horario as produto_horario'.
                ', t.id as treinamento_id, t.titulo as treinamento '.
                ', date_format(p.data_edit,"%d/%m/%Y") as pedido_data_edit '.
                ', pi.produto_valor, ps.descricao as pedido_status, max(pi.id) as pedido_item_id '.
                ', pd.incompany as produto_incompany, pd.incompany_email as produto_incompany_email '.
                ', t.incompany as treinamento_incompany, t.incompany_email as treinamento_incompany_email '.
            'from pedidos p '.
                'left join pedidos_items pi on pi.pedido_id = p.id '.
                'left join pedidos_status ps on ps.id = p.pedido_status_id '.
                'left join produtos pd on pd.id = pi.produto_id '.
                'left join treinamentos t on t.id = pd.treinamento_id '.
                'left join pedidos_grupos pg on pg.pedido_id = p.id '.
                'left join clientes c on (c.id = p.cliente_id or c.id = pg.cliente_id) '.
            'where (p.cliente_id = "'.$cliente_id.'" or pg.cliente_id = "'.$cliente_id.'") '.
                ($produto_id ? 'and pi.produto_id="'.$produto_id.'" ' : '').
                ($pago ? 'and p.pedido_status_id in ('.$this->status_pago.') ' : '').
            // 'group by c.id order by c.nome '.
            (($onlyLast) ?
                'group by p.id desc, pd.id order by p.id desc, pd.data ' :
                'group by pd.id order by pd.data ').
            'limit 1000'
        );

        return $rows;
    }

    public function getListaInscritos($produto_id=null,$pago=true)
    {
        if(!$produto_id) return null;

        $rows = $this->q(
            'select c.*, p.data_cad as pedido_data, p.id as pedido_id '.
                ', pd.titulo as produto'.
                ', date_format(pd.data,"%d/%m/%Y") as produto_data'.
                ', date_format(pd.data_final,"%d/%m/%Y") as produto_data_final'.
                ', t.titulo as treinamento '.
            'from pedidos p '.
                'left join pedidos_items pi on pi.pedido_id = p.id '.
                'left join produtos pd on pd.id = pi.produto_id '.
                'left join treinamentos t on t.id = pd.treinamento_id '.
                'left join pedidos_grupos pg on pg.pedido_id = p.id '.
                'left join clientes c on (c.id = p.cliente_id or c.id = pg.cliente_id) '.
            'where pi.produto_id="'.$produto_id.'" '.
                ($pago ? 'and p.pedido_status_id in ('.$this->status_pago.') ' : '').
            'group by c.id order by c.nome '.
            'limit 1000'
        );

        return $rows;
    }

    public function getInscritosConfirmados($produto_id)
    {
        $rows = array();

        $inscritos = $this->q(
            'select c.*, p.data_cad as pedido_data, p.id as pedido_id '.
                ', pd.titulo as produto'.
                ', date_format(pd.data,"%d/%m/%Y") as produto_data'.
                ', date_format(pd.data_final,"%d/%m/%Y") as produto_data_final'.
                ', t.titulo as treinamento '.
            'from pedidos p '.
                'left join clientes c on c.id = p.cliente_id '.
                'left join pedidos_items pi on pi.pedido_id = p.id '.
                'left join produtos pd on pd.id = pi.produto_id '.
                'left join treinamentos t on t.id = pd.treinamento_id '.
            'where p.pedido_status_id in ('.$this->status_pago.') '.
                'and pi.produto_id = "'.$produto_id.'" '.
            'group by c.id '.
            'order by c.nome '
        );
        // _d($inscritos);

        if($inscritos) foreach($inscritos as $row){
            $grupo = $this->q(
                'select c.*, c2.nome as parent_nome '.
                    ', pd.titulo as produto'.
                    ', date_format(pd.data,"%d/%m/%Y") as produto_data'.
                    ', date_format(pd.data_final,"%d/%m/%Y") as produto_data_final'.
                    ', t.titulo as treinamento '.
                'from pedidos_grupos pg '.
                    'left join clientes c on c.id = pg.cliente_id '.
                    'left join clientes c2 on c2.id = c.cliente_id '.
                    'left join pedidos_items pi on pi.pedido_id = pg.pedido_id '.
                    'left join produtos pd on pd.id = pi.produto_id '.
                    'left join treinamentos t on t.id = pd.treinamento_id '.
                'where pg.pedido_id = "'.$row->pedido_id.'" '.
                'group by c.id '.
                'order by c.nome '
            );

            if((bool)$grupo) {
                foreach($grupo as &$g) {
                    $g->pedido_id = $row->pedido_id;
                    $g->pedido_data = $row->pedido_data;
                    $rows[] = $g;
                }
            } else $rows[] = $row;
        }

        return $rows;
    }

    /**
     * Tranfere aluno de turma
     * 
     * @param array $data - dados para transferencia:
                            produto_id => id do produto/turma a transferir
                            cliente_id => id do cliente
                            pedido_id  => id do pedido a atualizar
                            produto_id_old => id do produto/turma antigo para arquivamento de historico
     * 
     * @return bool | exception
     */
    public function transfTurma($data)
    {
        if(is_array($data)) $data = (object)$data;
        
        // aplica row de turma para verificações
        if(!$this->produtos) $this->produtos = new Application_Model_Db_Produtos();
        $data->turma_de = $this->produtos->get($data->produto_id_old);
        $data->turma_para = $this->produtos->get($data->produto_id);

        // transf: check se pode alterar turma (aplicar regras)
        if(!$this->transfTurmaCheck($data))
            throw new Exception('Turma não autorizada a aceitar transferências.', 1);
        
        try {
            $this->saveHistoricoTurma($data);
            $this->updateTurma($data);
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
            return false;
        }
    }

    public function cadTurma($data)
    {
        if(is_array($data)) $data = (object)$data;
        
        // aplica row de turma para verificações
        if(!$this->produtos) $this->produtos = new Application_Model_Db_Produtos();
        $data->turma_para = $this->produtos->get($data->produto_id);

        // transf: check se pode alterar turma (aplicar regras)
        if(!$this->transfTurmaCheck($data))
            throw new Exception('Turma não autorizada a aceitar novos cadastros.', 1);
        
        try {
            $this->insertTurma($data);
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
            return false;
        }
    }

    /**
     * Checa se turma pode ser transferida / Aplica regras de transferência
     * 
     * Isso só poderá ocorrer se o curso ainda não estiver iniciado.
     * Para evitar que a pessoa tenha que fazer a inscrição toda novamente
     * quando ocorrer necessidade de mudança de data ou de curso mesmo.
     * 
     * Regras de transferência
     * - somente turmas não iniciadas
     *
     * @param array $data - dados para verificação da turma:
                            dados necessários em self::transfTurma() +
                            turma_de   => row do produto/turma atual
                            turma_para => row do produto/turma que irá transferir
     * 
     * @return bool
     */
    public function transfTurmaCheck($data)
    {
        if(is_array($data)) $data = (object)$data;
        if(!isset($data->turma_para)) {
            throw new Exception("Turma não encontrada", 1);
            return false;
        }

        $now_dt = date('Y-m-d');
        
        $valid = 1==1 &&
                (@$data->turma_para->data >= $now_dt) && // turma não pode ter sido iniciada (data < hj)
                (@in_array((int)$data->turma_para->status_id, array(0,1,2))) && // status da turma tem que ser: 0,1,2
                 1==1;

        return $valid;
    }

    /**
     * Atualiza turma / realiza transferência
     */
    public function updateTurma($data)
    {
        if(is_array($data)) $data = (object)$data;
        if(!isset($data->turma_de) || !isset($data->turma_para)) {
            throw new Exception("Turma não encontrada", 1);
            return false;
        }
        if(!$this->pedidos_items) $this->pedidos_items = new Application_Model_Db_PedidosItems();
        
        // atualiza pedido para a turma / realiza transferência
        $this->pedidos_items->update(array(
            'produto_id' => $data->produto_id,
            'produto_descricao' => $data->turma_para->titulo,
            'produto_valor' => $data->turma_para->valor,
        ),'pedido_id="'.$data->pedido_id.'" and produto_id="'.$data->produto_id_old.'"');

        // atualiza contagem de inscritos nas turmas nova e antiga
        $this->checkInscritos($data->produto_id_old);
        $this->checkInscritos($data->produto_id);

        return true;
    }
    
    public function insertTurma($data)
    {
        if(is_array($data)) $data = (object)$data;
        if(!isset($data->turma_para)) {
            throw new Exception("Turma não encontrada", 1);
            return false;
        }
        if(!$this->pedidos) $this->pedidos = new Application_Model_Db_Pedidos();
        if(!$this->pedidos_items) $this->pedidos_items = new Application_Model_Db_PedidosItems();
        if(!$this->login) $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');

        $now = date('Y-m-d H:i:s');

        // insere pedido
        $pedido_id = $this->pedidos->insert(array(
            'cliente_id' => $data->cliente_id,
            'user_cad' => @$this->login->user->id,
            'user_edit' => @$this->login->user->id,
            'pedido_status_id' => 11,
            'pedido_forma_id' => 2,
            'data_cad' => $now,
            'data_edit' => $now,
            'data_pago' => $now,
        ));
        
        // insere pedido_item/turma
        $this->pedidos_items->insert(array(
            'pedido_id'  => $pedido_id,
            'produto_id' => $data->produto_id,
            'produto_descricao' => $data->turma_para->titulo,
            'produto_valor' => $data->turma_para->valor,
            'qtde' => 1,
            'forma' => 'c',
        ));

        // atualiza contagem de inscritos nas turmas nova e antiga
        $this->checkInscritos($data->produto_id);

        return true;
    }

    /**
     * Salva histórico de tranferência de turma
     */
    public function saveHistoricoTurma($data)
    {
        if(is_array($data)) $data = (object)$data;
        if(!isset($data->turma_de) || !isset($data->turma_para)) {
            throw new Exception("Turma não encontrada", 1);
            return false;
        }
        if(!$this->pedidos_items) $this->pedidos_items = new Application_Model_Db_PedidosItems();
        if(!$this->pedidos_items_historico) $this->pedidos_items_historico = new Application_Model_Db_PedidosItemsHistorico();
        if(!$this->login) $this->login = new Zend_Session_Namespace(SITE_NAME.'_login'); 

        // seleciona item/turma antigo
        $pedido_item = $this->pedidos_items->fetchRow(
            'pedido_id="'.$data->pedido_id.'" and produto_id="'.$data->produto_id_old.'"'
        );

        // salva dados no historico
        $this->pedidos_items_historico->insert(array(
            'pedido_item_id' => @$pedido_item->id,
            'produto_id_de' => $data->produto_id_old,
            'produto_descricao_de' => @$pedido_item->produto_descricao,
            'produto_valor_de' => @$pedido_item->produto_valor,
            'produto_id_para' => $data->produto_id,
            'produto_descricao_para' => $data->turma_para->titulo,
            'produto_valor_para' => $data->turma_para->valor,
            'data' => date('Y-m-d H:i:s'),
            'user_cad' => @$this->login->user->id,
            'user_edit' => @$this->login->user->id,
        ));

        return true;
    }

    /**
     * Retorna cuidados do treinamento
     *
     * @param int $id - ID do treinamento para serem selecionados os cuidados
     *                  Se não for passado, pega listagem de todos os cuidados
     * @param bool $getAll - true = pega todos os registros mesmo sem treinamento
     *                       false = pega somente cuidados relacionados ao treinamento
     */
    public function getCuidados($id=null,$getAll=false,$order='c.ordem')
    {
        $sql = 'select c.id, c.descricao, c.alias, ';
        $sql.= 'f.path as foto_path ';
        
        if($id!==null){
            if($getAll) $sql.= ', (select treinamento_id from treinamentos_cuidados pc where pc.cuidado_id = c.id and pc.treinamento_id = '.$id.') as treinamento_id ';
            else $sql.= ', pc.treinamento_id ';
        }

        $sql.= 'from cuidados c ';
        $sql.= 'left join fotos as f on f.id = c.foto_id ';
        if($id!==null && !$getAll) $sql.= 'left join treinamentos_cuidados pc on pc.cuidado_id = c.id ';
        
        $sql.= 'where 1=1 ';
        if($id!==null && !$getAll) $sql.= 'and pc.treinamento_id = '.$id.' ';

        $sql.= 'order by '.$order;
        
        return $this->q($sql);
    }

    public function getInstrutor($id=null)
    {
        if(!$id) return null;

        $row = $this->q1(
            'select i.*, f.path as foto_path from instrutores i '.
                'left join instrutores_fotos insf on insf.instrutor_id = i.id '.
                'left join fotos f on f.id = insf.foto_id '.
            'where i.id = "'.$id.'" '.
            'limit 1'
        );

        return $row;
    }

    public function getInstrutores($id=null)
    {
        if(!$id) return array();

        $rows = $this->q(
            'select i.*, f.path as foto_path from treinamentos_instrutores ti '.
                'left join instrutores i on i.id = ti.instrutor_id '.
                'left join instrutores_fotos insf on insf.instrutor_id = i.id '.
                'left join fotos f on f.id = insf.foto_id '.
            'where ti.treinamento_id = "'.$id.'" '.
            'limit 1000'
        );

        return $rows;
    }

}