<?php

class Application_Model_Db_ClientesConsultorias extends ZendPlugin_Db_Table {
    protected $_name = "clientes_consultorias";
    
    protected $_dependentTables = array('Application_Model_Db_Clientes');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Clientes' => array(
            'columns' => 'cliente_id',
            'refTableClass' => 'Application_Model_Db_Clientes',
            'refColumns'    => 'id'
        )
    );

    public function getByCliente($id)
    {
        return $this->q(
            'select cc.*, '.
                'a.descricao as arquivo_descricao, '.
                'a.obs as arquivo_obs, '.
                'a.flag as arquivo_flag, '.
                'a.path as arquivo_path '.
                ',if(cc.enviado_consultoria=1 or c.data_last_view_admin > cc.data_cad, 0, 1) as is_unread '.
            'from clientes_consultorias cc '.
                'left join arquivos a on a.id = cc.arquivo_id '.
                'left join clientes c on c.id = cc.cliente_id '.
            'where 1=1 '.
                'and cc.cliente_id = "'.$id.'" '.
            // 'order by cc.id desc '.
            'order by is_unread desc, cc.data_cad desc '.
            'limit 1000'
        );
    }

    public function getAll($where=null,$order=null,$limit=null,$offset=null,$where2=null)
    {
        if($order) if(is_array($order)) $order = implode(', ', $order);
        if($limit || $offset) $limit = ($offset ? $offset.',' : '').$limit;

    	$rows = $this->q(
    		'select cc.*, '.
                'c.id as cliente_id, '.
                'c.nome as cliente_nome, '.
                'a.titulo as arquivo_titulo, '.
                'a.descricao as arquivo_descricao, '.
                'a.obs as arquivo_obs, '.
                'a.flag as arquivo_flag, '.
                'a.path as arquivo_path, '.
                'a.tamanho as arquivo_tamanho, '.
                'a.tipo as arquivo_tipo, '.
                'a.licenca as arquivo_licenca, '.
                'a.versao as arquivo_versao, '.
                'a.autor as arquivo_autor, '.
                'a.hits as arquivo_hits, '.
                'a.md5check as arquivo_md5check, '.
                'a.data_cad as arquivo_data_cad, '.
    			'a.data_edit as arquivo_data_edit '.
                // ',if(c.data_last_view_admin > a.data_cad, 0, 1) as is_unread '.
                ',if(cc.enviado_consultoria=1 or c.data_last_view_admin > cc.data_cad, 0, 1) as is_unread '.
    		'from clientes_consultorias cc '.
                'left join arquivos a on a.id = cc.arquivo_id '.
    			'left join clientes c on c.id = cc.cliente_id '.
    		'where 1=1 '.
                ($where ? 'and ('.$where.') ' : '').
			'group by cc.arquivo_id '.
            'order by '.($order ? $order : 'cc.id desc').' '.
    		'limit '.($limit ? $limit : '1000')
    	);

        // pegando dados de clientes para listagem
        $_rowsc = $this->q(
            'select cc.arquivo_id, c.id, c.nome from clientes_consultorias cc '.
            'left join clientes c on c.id = cc.cliente_id '.
            'where 1=1 '.($where2 ? 'and ('.$where2.') ' : '').
            // 'group by c.id '.
            'order by c.nome '.
            'limit 1000'
        );
        
        if($_rowsc) {
            $rowsc = array();
            foreach ($_rowsc as $rowc) {
                if(!isset($rowsc[$rowc->arquivo_id]))
                    $rowsc[$rowc->arquivo_id] = array();
                $rowsc[$rowc->arquivo_id][] = $rowc;
            }

            foreach ($rows as &$row)
                $row->clientes = @$rowsc[$row->arquivo_id];
        }

        return $rows;
    }
}