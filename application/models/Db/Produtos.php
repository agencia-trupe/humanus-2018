<?php

class Application_Model_Db_Produtos extends ZendPlugin_Db_Table 
{
    protected $_name = "produtos";
    protected $_foto_join_table = 'produtos_fotos';
    protected $_foto_join_table_field = 'produto_id';
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Categorias','Application_Model_Db_ProdutosSugestoes','Application_Model_Db_SugestoesItems');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Categorias' => array(
            'columns' => 'categoria_id',
            'refTableClass' => 'Application_Model_Db_Categorias',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_ProdutosSugestoes' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutosSugestoes',
            'refColumns'    => 'sugestao_id'
        ),
        'Application_Model_Db_SugestoesItems' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_SugestoesItems',
            'refColumns'    => 'produto_id'
        ),
    );
    
    /**
     * Retorna produto com suas imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias - valor do alias ou id do produto
     *
     * @return object|bool - objeto contendo o produto com suas imagens e categoria ou false se não for encontrado
     */
    public function getWithFotos($alias)
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        if(!$produto = $this->fetchRow($column.'="'.$alias.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos')){
            foreach($produto_fotos as $produto_foto){
                $pf = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
                
                $pf->cor_id = ($produto_foto->cor_id) ? $produto_foto->cor_id : null;
                $pf->cor = ($produto_foto->cor_id) ?
                            Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Cores')->current()) :
                            null;
                
                $fotos[] = $pf;
            }
        }
        
        $object = Is_Array::utf8DbRow($produto);
        $object->categoria = (bool)$produto->categoria_id ?
                            Is_Array::utf8DbRow($produto->findDependentRowset('Application_Model_Db_Categorias')->current()) :
                            null;
        $object->fotos = $fotos;
        
        // descontos
        $object = self::checkDesconto($object);
        
        return $object;
    }

    public function parseModulos($rows)
    {
        $ids = array();
        foreach($rows as $row) {
            $ids[] = $row->id;
            $row->modulos = array();
        }
        $modulos = $this->getModulos($ids);

        foreach ($modulos as $mod)
            foreach ($rows as $row)
                if($mod->produto_id == $row->id)
                    $row->modulos[] = $mod;

        return $rows;
    }

    public function getModulos($produto_id=null,$where_opt=null,$withPhoto=false)
    {
        if(is_array($produto_id)) $produto_id = implode(',', $produto_id);
        $where = (bool)trim($produto_id) ? 'produto_id in ('.$produto_id.') ' : null;
        if($where_opt) $where.= $where_opt;
        $rows = $this->s('modulos','id,alias,titulo,descricao,produto_id',$where,'ordem');
        
        if($rows && $withPhoto) {
            $pids = array(); foreach($rows as $r) $pids[] = $r->id;
            $rowsf = $this->q(
                'select f.*, mf.modulo_id from modulos_fotos mf '.
                'left join fotos f on f.id = mf.foto_id '.
                'where modulo_id in ('.implode(',',$pids).') '.
                'order by f.ordem, mf.id limit 100'
            );
            $fotos = array();
            foreach($rowsf as $rf) $fotos[$rf->modulo_id] = $rf;
            
            foreach($rows as $r) if(array_key_exists($r->id, $fotos)) {
                $r->foto = $fotos[$r->id];
                $r->foto_path = $fotos[$r->id]->path;
            } else {
                $r->foto = null;
                $r->foto_path = null;
            }
        }

        return $rows;
    }
    
    public function getDestaques($limit=3)
    {
        if($produtos = $this->fetchAll('destaque = 1','data_edit desc',$limit)){
            $destaques = array();
            
            foreach($produtos as $produto){
                $p = Is_Array::utf8DbRow($produto);
                $p->fotos = $this->getFotos($p->id);
                $destaques[] = $p;
            }
            
            return $destaques;
        }
        
        return false;
    }
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos($id,$cor=null)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')) return null;
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos')){
            foreach($produto_fotos as $produto_foto){
                $f = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
                $f->cor_id = $produto_foto->cor_id ? $produto_foto->cor_id : null;
                $f->cor    = $produto_foto->cor_id ? Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Cores')->current()) : null;
                
                if($cor!==null) {
                    if ($cor==$f->cor_id) $fotos[] = $f;
                } else {
                    $fotos[] = $f;
                }
            }
        }
        
        return $fotos;
    }
    
    /**
     * Retorna as sugestoes do produto
     *
     * @param int  $id        - id do produto
     * @param bool $get_fotos - selecionar também fotos?
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do produto
     */
    public function getSugestoes($id,$get_fotos=false,$limit=null,$rand=false)
    {
        // saímos se o produto não existir
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $sugestoes = array();
        
        // montando a array de sugestões
        $select = $rand ? // randomizamos os resultados se for solicitado
                  $this->select()->order(new Zend_Db_Expr('RAND()'))->limit($limit) :
                  null;
        if($produto_sugestoes = $produto->findDependentRowset('Application_Model_Db_ProdutosSugestoes',null,$select)){
            foreach($produto_sugestoes as $produto_sugestao){
                $sugestao = Is_Array::utf8DbRow($produto_sugestao->findDependentRowset('Application_Model_Db_Produtos')->current());
                $sugestao = $this->checkDesconto($sugestao);
                $sugestoes[] = $sugestao;
            }
        }
        
        // pegamos as fotos se for solicitado
        if($get_fotos){
            $produtos_fotos = new Application_Model_Db_ProdutosFotos();
            
            foreach($sugestoes as &$sugestao){
                $sugestao->fotos = array();
                
                if($produto_fotos = $produtos_fotos->fetchAll('produto_id='.$sugestao->id)){    
                    foreach($produto_fotos as $produto_foto){
                        $sugestao->fotos[] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current();
                    }
                }
            }
        }
        
        // randomizamos os resultados
        // - retirado para melhor performance caso haja muitas sugestões, solução adicionada acima
        //if($rand){ shuffle($sugestoes); }
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_name,array('count(*) as cnt'));
        $count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
        return $count[0]['cnt'];
    }
    
    /**
     * Checa descontos do produto (recursivo)
     *
     * $param array|object $object - Array/Objeto do produto para ser checado os descontos.
     *                               Não pode ser uma instância do Zend Table.
     *
     * @return array|object
     */
    public function checkDesconto($object=null)
    {
        if($object===null) return null;
        
        if(is_array($object)){ // adiciona resursão à função
            if(count($object)){
                foreach($object as &$o) $o = self::checkDesconto($o);
                return $object;
            }
            return null;
        }
        
        if((bool)(float)$object->valor_promo){ // se possui valor promocional
            $object->valor_desconto = $object->valor; // adiciona valor_desconto como o valor original
            $object->valor = $object->valor_promo; // seta valor com desconto, para ser usado como o valor do produto
        } else {
            if((bool)$object->categoria_id){
              $cats = new Application_Model_Db_Categorias();
              $cat  = $cats->fetchRow('id='.$object->categoria_id);
              
              if((bool)$cat->desconto){ // se a categoria está com desconto
                  $object->valor_desconto = $object->valor;
                  $object->valor = Is_Math::percentDec($cat->desconto,$object->valor);
              } else {
                  $object->valor_desconto = null;
              }
            }
        }
        
        return $object;
    }

    /**
     * Retorna as formatos do produto
     *
     * @param int  $id        - id do produto
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do produto
     */
    public function getFormatos($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_ProdutoFormato();
        $_tags = new Application_Model_Db_Formatos();
        $sugestoes = array(); $sugestoes_ids = array();

        if(!(bool)$prods = $_sugestoes->fetchAll('produto_id="'.$id.'"')){
            return false;
        }

        foreach($prods as $ps) $sugestoes_ids[] = $ps->formato_id;

        $sugestoes = Is_Array::utf8DbResult($_tags->fetchAll('id in ('.(count($sugestoes_ids) ? implode(',',$sugestoes_ids) : '0').')','formato'));
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;

        /* old
        foreach($prods as $ps){
            $s = Is_Array::utf8DbRow($_tags->fetchRow('id='.$ps->formato_id));
            $sugestoes[] = $s;
        }
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;*/
    }

    /**
     * Retorna as gramaturas do produto
     *
     * @param int  $id        - id do produto
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do produto
     */
    public function getGramaturas($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_ProdutoGramatura();
        $_tags = new Application_Model_Db_Gramaturas();
        $sugestoes = array(); $sugestoes_ids = array();

        if(!(bool)$prods = $_sugestoes->fetchAll('produto_id="'.$id.'"')){
            return false;
        }

        foreach($prods as $ps) $sugestoes_ids[] = $ps->gramatura_id;

        $sugestoes = Is_Array::utf8DbResult($_tags->fetchAll('id in ('.(count($sugestoes_ids) ? implode(',',$sugestoes_ids) : '0').')','gramatura'));
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;

        /* old
        foreach($prods as $ps){
            $s = Is_Array::utf8DbRow($_tags->fetchRow('id='.$ps->gramatura_id));
            $sugestoes[] = $s;
        }
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;*/
    }
}