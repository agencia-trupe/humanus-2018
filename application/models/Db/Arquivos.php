<?php

class Application_Model_Db_Arquivos extends ZendPlugin_Db_Table {
    protected $_name = "arquivos";
    
    protected $_dependentTables = array('Application_Model_Db_PaginasArquivos','Application_Model_Db_ProdutosItemsArquivos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_PaginasArquivos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PaginasArquivos',
            'refColumns'    => 'arquivo_id'
        ),
        'Application_Model_Db_ProdutosItemsArquivos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutosItemsArquivos',
            'refColumns'    => 'arquivo_id'
        ),
    );
}