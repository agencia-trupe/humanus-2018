<?php

class Application_Model_Db_TecnologiasFotos extends Zend_Db_Table
{
    protected $_name = "tecnologias_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Tecnologias');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Tecnologias' => array(
            'columns' => 'tecnologia_id',
            'refTableClass' => 'Application_Model_Db_Tecnologias',
            'refColumns'    => 'id'
        )
    );
}
