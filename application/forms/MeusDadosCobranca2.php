<?php

class Application_Form_MeusDadosCobranca2 extends ZendPlugin_Form
{
    protected $admin = false;

    public function __construct($admin=false)
    {
        $this->admin = $admin;
        parent::__construct();
    }

    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/meu-cadastro/save')->setAttrib('id','frm-meus-dados')->setAttrib('name','frm-meus-dados');
		
        // $locais = new Application_Model_Db_LocalEntrega();
        $estados = new Application_Model_Db_Estados();
        
        // elementos
        $this->addElement('text','cnpj',array('label'=>'CNPJ*:','class'=>'txt mask-cnpj'));
        $this->addElement('text','nome',array('label'=>'Razão social*:','class'=>'txt'));
        $this->addElement('text','insc_est',array('label'=>'Insc. Estadual*:','class'=>'txt'));
        $this->addElement('text','email',array('label'=>'E-mail*:','class'=>'txt'));
        $this->addElement('text','cep',array('label'=>'CEP*:','class'=>'txt mask-cep'));
        $this->addElement('text','logradouro',array('label'=>'End. empresa*:','class'=>'txt'));
        $this->addElement('text','numero',array('label'=>'Número*:','class'=>'txt'));
        $this->addElement('text','complemento',array('label'=>'Complemento*:','class'=>'txt'));
        $this->addElement('text','bairro',array('label'=>'Bairro*:','class'=>'txt'));
        $this->addElement('text','cidade',array('label'=>'Cidade*:','class'=>'txt'));
        $this->addElement('select',($this->admin?'uf_id':'uf'),array('label'=>'UF*:','class'=>'txt','multiOptions'=>Is_Array::utf8All($estados->getKeyValues('sigla'))));
        // $this->addElement('text','nf_end_envio',array('label'=>'CEP*:','class'=>'txt'));
        // $this->addElement('text','nf_resp',array('label'=>'Responsável NF*:','class'=>'txt'));
        // $this->addElement('text','nf_resp_email',array('label'=>'E-mail*:','class'=>'txt'));
        // $this->addElement('radio','nf_num_pedido',array('label'=>'Nº do pedido?*:','class'=>'chk','multiOptions'=>array('1'=>'Sim','0'=>'Não')));
        $this->addElement('text','telefone',array('label'=>'Telefone*:','class'=>'txt mask-tel'));
        // $this->addElement('text','funcao_area',array('label'=>'Função / área*:','class'=>'txt'));
        $this->addElement('hidden','tipo_pessoa',array('value'=>'1'));

        foreach ($this->getElements() as $elm) {
            // _d($elm->getType(),false);
            if($elm->getType()!='Zend_Form_Element_Hidden' 
            && $elm->getId()!='insc_est'
            && $elm->getId()!='numero'
            && $elm->getId()!='complemento'){
                $elm->setRequired()
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->setAttrib('data-validate',true)
                    ->setAttrib('data-errmsg', 'Campo '.$elm->getLabel().' inválido');

                switch ($elm->getId()) {
                    case 'email':
                        $elm->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'));
                        break;
                }
            }
        }
        
        /*// filtros / validações
        $this->getElement('nome')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu nome corretamente');
        // $this->getElement('email')->setRequired()
             // ->addValidator('NotEmpty',true,array('messages'=>'E-mail inválido'))
             // ->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'))
             // ->addFilter('StripTags')->addFilter('StringTrim')
             // ->setAttrib('data-validate',true)->setAttrib('data-errmsg','E-mail inválido');
        // $this->getElement('emailc')->setRequired()->addValidator('EmailAddress')
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme seu e-mail corretamente');
        // $this->getElement('senha')->setRequired()
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
        // $this->getElement('senhac')->setRequired()
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');*/
        
        // remove decoradores
        $this->removeDecs();
    }
    
    public function requireSenha()
    {
        $this->getElement('senha')->setRequired()
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
        $this->getElement('senhac')->setRequired()
            ->addValidator('Identical',false,array('token'=>'senha','messages'=>'As senhas não coincidem'))
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');
        return $this;
    }
    
    public function addId($value=null)
    {
        $this->addElement('hidden','id',array('value'=>$value));
        return $this;
    }
    
    public function addStatus($value=null)
    {
        $this->addElement('checkbox','status_id',array('label'=>'Ativo','value'=>$value));
        return $this;
    }

    public function isValid($values){
        if(!Is_Cpf::isValidCnpj($values['cnpj'])){
            $this->addErrorMessage('CNPJ inválido');
            // return false;
        }

        // validando campos select para seleção vazia == __none__ (título do campo)
        foreach ($this->getElements() as $elm) {
            if($elm->getType()=='Zend_Form_Element_Select'){
                if($values[$elm->getId()]=='__none__'){
                    $elm->markAsError();
                    $this->addErrorMessage('Campo '.$elm->getLabel().' inválido');
                }
            }
        }
        
        return parent::isValid($values);
    }
}

