<?php

class Application_Form_Contato extends ZendPlugin_Form
{
    protected $mensagem_id = 11;

    public $extra_emails = array(
        'production' => array(
            'fale_conosco' => array(
                // '11' => 'Certificação',
                // '17' => 'Treinamento',
                '18' => 'Fale Conosco',
            ),
        ),
        'testing' => array(
            'fale_conosco' => array(
                // '11' => 'Certificação',
                // '17' => 'Treinamento',
                '18' => 'Fale Conosco',
            ),
        ),
        'development' => array(
            'fale_conosco' => array(
                // '11' => 'Certificação',
                // '17' => 'Treinamento',
                '18' => 'Fale Conosco',
            ),
        ),
    );

    public function __construct($mensagem_id=null)
    {
        if($mensagem_id) $this->mensagem_id = $mensagem_id;
        return parent::__construct();
    }

    public function init()
    {
        $this->setMethod('post')
             ->setAttrib('id','frm-contato-'.$this->mensagem_id)
             ->setAttrib('name','frm-contato-'.$this->mensagem_id);;

        // $treinamentos = new Application_Model_Db_Treinamentos();
        // // $temas = $treinamentos->getNexts();
        // $temas = Is_Array::utf8DbResult($treinamentos->fetchAll('status_id > 0'));
        // $temas_array = array('__none__'=>'tema * [selecione]');
        // foreach($temas as $t) $temas_array[$t->id] = $t->titulo;

        // configurações do form
        switch ($this->mensagem_id) {
            case 17:
            case 18:
            case 11: // fale conosco
                $this->setAction(URL.'/contato/enviar');
                
                $this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
                // $this->addElement('text','empresa',array('label'=>'empresa','class'=>'txt2'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                // $this->addElement('text','celular',array('label'=>'celular','class'=>'txt2 mask-cel'));
                // $this->addElement('select','assunto',array('label'=>'assunto','class'=>'txt2','multiOptions'=>array(
                //     '0' => 'certificação',
                //     '1' => 'treinamento',
                // )));
                $this->addElement('select','assunto',array('label'=>'assunto','class'=>'txt2','multiOptions'=>$this->extra_emails[APPLICATION_ENV]['fale_conosco']));
                $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
                break;
            
            case 12: // trabalhe conosco
                $this->setAction(URL.'/contato/enviar');
                
                $this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
                // $this->addElement('text','empresa',array('label'=>'empresa','class'=>'txt2'));
                // $this->addElement('text','cpf',array('label'=>'CPF','class'=>'txt2 mask-cpf'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
                $this->addElement('hidden','assunto',array('value'=>"Trabalhe conosco"));
                break;

            case 13: // treinamentos interesse
                $this->setAction(URL.'/treinamentos/interesse');

                // $cidades = $treinamentos->q('select * from cidades order by id');
                $cidades = $treinamentos->q('select * from cidades order by uf,cidade');
                $cidadesKV = array('__none__' => 'cidade * [selecione]');
                foreach($cidades as $c) if((bool)trim($c->uf)) $cidadesKV[$c->id] = '['.$c->uf.'] '.$c->cidade;
                
                $this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                // $this->addElement('select','tema',array('label'=>'tema','class'=>'txt2','multiOptions'=>$temas_array));
                // // $this->addElement('text','cidade',array('label'=>'cidade','class'=>'txt2'));
                // $this->addElement('text','cidade',array('label'=>'cidade','class'=>'txt2'));
                
                // // $this->addElement('select','cidade',array('label'=>'cidade','class'=>'txt2','multiOptions'=>$cidadesKV));
                // // $this->addElement('select','cidade',array('label'=>'cidade','class'=>'txt2','multiOptions'=>array(
                // //     '__none__' => 'cidade [selecione]',
                // //     '0' => 'lorem ipsum',
                // //     '1' => 'dolor sit amet',
                // // )));
                
                // $this->addElement('radio','horario',array('label'=>'Informe sua preferência por período:','class'=>'chk','multiOptions'=>array(
                //     '0' => 'durante a semana período Integral',
                //     '1' => 'durante a semana período da Manhã',
                //     '2' => 'durante a semana período da Tarde',
                //     '3' => 'durante a semana período da Noite',
                //     '4' => 'aos Sábados: período da Manhã',
                //     '5' => 'aos Sábados: período Integral',
                // )));
                $this->addElement('hidden','assunto',array('value'=>"Registre seu interesse"));
                break;

            case 22: // treinamentos saiba mais
                $this->setAction(URL.'/treinamento/saiba-mais');

                $this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                // $this->addElement('select','tema',array('label'=>'tema','class'=>'txt2','multiOptions'=>$temas_array));
                $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
                $this->addElement('hidden','assunto',array('value'=>"Saiba mais sobre o Curso"));
                break;

            case 14: // instrutores
                $this->setAction(URL.'/treinamentos/nossos-instrutores');
                
                $this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                break;

            case 15: // treinamentos in company
                $this->setAction(URL.'/treinamentos/treinamentos-in-company');
                
                // $this->addElement('select','tema',array('label'=>'tema','class'=>'txt2','multiOptions'=>$temas_array));
                // $this->addElement('text','cidade',array('label'=>'região','class'=>'txt2'));
                // $this->addElement('text','horario',array('label'=>'período de realização','class'=>'txt2'));
                // $this->addElement('text','participantes',array('label'=>'número de participantes','class'=>'txt2'));
                // $this->addElement('text','empresa',array('label'=>'empresa','class'=>'txt2'));
                // $this->addElement('text','cnpj',array('label'=>'CNPJ','class'=>'txt2 mask-cnpj'));
                $this->addElement('text','nome',array('label'=>'nome do solicitante','class'=>'txt2'));
                // $this->addElement('text','cargo',array('label'=>'cargo','class'=>'txt2'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
                break;

            case 16: // certificacao
                $this->setAction(URL.'/certificacao-sistemas/enviar');

                $this->addElement('text','cnpj',array('label'=>'CNPJ','class'=>'txt2 mask-cnpj'));
                $this->addElement('text','empresa',array('label'=>'razão social','class'=>'txt2'));
                $this->addElement('text','nome',array('label'=>'contato','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','site',array('label'=>'site','class'=>'txt2'));
                // $this->addElement('select','certificacao',array('label'=>'tipo de certificação','class'=>'txt2','multiOptions'=>$certsKV));
                $this->addElement('select','certificacao',array('label'=>'tipo de certificação','class'=>'txt2','multiOptions'=>array(
                    '__none__' => '[selecione]',
                    '0' => 'lorem ipsum',
                    '1' => 'dolor sit amet',
                )));
                break;

            case 21: // fast-track
                $this->setAction(URL.'/fast-track/enviar');

                $this->addElement('text','nome',array('label'=>'contato','class'=>'txt2'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
                break;

            case 23: // consultoria
                $this->setAction(URL.'/consultoria/enviar');

                $this->addElement('text','nome',array('label'=>'contato','class'=>'txt2'));
                $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
                $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
                $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
                break;
            
            default:
                # code...
                break;
        }

        $this->addElement('hidden','mensagem_id',array('value'=>$this->mensagem_id));

        foreach ($this->getElements() as $elm) {
            // _d($elm->getType(),false);
            if($elm->getType()!='Zend_Form_Element_Hidden'){
                $elm->setRequired()
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->setAttrib('data-validate',true)
                    ->setAttrib('data-errmsg', 'Campo '.$elm->getLabel().' inválido');

                switch ($elm->getId()) {
                    case 'email':
                        $elm->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'));
                        break;
                }
            }
        }
        // _d('ok');

        // filtros / validações
        /*if($this->getElement('nome'))
            $this->getElement('nome')->setRequired()->setAttrib('data-errmsg','Nome inválido')
                ->addFilter('StripTags')->addFilter('StringTrim');
        if($this->getElement('telefone'))
            $this->getElement('telefone')//->setRequired()
                ->addFilter('StripTags')->addFilter('StringTrim');
        if($this->getElement('mensagem'))
            $this->getElement('mensagem')->setRequired()->setAttrib('data-errmsg','Escreva uma mensagem')
                ->addFilter('StripTags')->addFilter('StringTrim');
        if($this->getElement('cpf'))
            $this->getElement('cpf')->setRequired()->setAttrib('data-errmsg','CPF inválido')
                ->addFilter('StripTags')->addFilter('StringTrim');
        if($this->getElement('cnpj'))
            $this->getElement('cnpj')->setRequired()->setAttrib('data-errmsg','CNPJ inválido')
                ->addFilter('StripTags')->addFilter('StringTrim');
        if($this->getElement('email'))
            $this->getElement('email')->setRequired()
                // ->addValidator('EmailAddress')->addFilter('StripTags');
                // ->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'));
                ->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'))
                ->addFilter('StripTags')->addFilter('StringTrim')
                ->setAttrib('data-validate',true)->setAttrib('data-errmsg','E-mail inválido');
        
        // validações extras (por assunto / mensagem_id)
        if($this->mensagem_id==13) { // interesse
            $this->getElement('telefone')->setRequired()->setAttrib('data-errmsg','Telefone inválido');
            $this->getElement('tema')->setRequired()->setAttrib('data-errmsg','Escolha um tema');
            $this->getElement('cidade')->setRequired()->setAttrib('data-errmsg','Cidade inválida');
            $this->getElement('horario')->setRequired()->setAttrib('data-errmsg','Horário inválido');
        }

        if($this->mensagem_id==15) { // treinamentos in company
            $this->getElement('tema')->setRequired()->setAttrib('data-errmsg','Escolha um tema');
            $this->getElement('cidade')->setRequired()->setAttrib('data-errmsg','Região inválida');
            $this->getElement('horario')->setRequired()->setAttrib('data-errmsg','Período inválido');
            $this->getElement('participantes')->setRequired()->setAttrib('data-errmsg','Preencha o campo participantes');
            $this->getElement('empresa')->setRequired()->setAttrib('data-errmsg','Empresa inválida');
            $this->getElement('nome')->setRequired()->setAttrib('data-errmsg','Nome inválido');
            $this->getElement('cargo')->setRequired()->setAttrib('data-errmsg','Cargo inválido');
            $this->getElement('telefone')->setRequired()->setAttrib('data-errmsg','Telefone inválido');
        }*/

        // remove decoradores
        $this->removeDecs();
    }

    public function changeClasses($class='txt',$disable=false)
    {
        foreach ($this->getElements() as $elm)
            if($elm->getType()!='Zend_Form_Element_Radio'){
                $elm->setAttrib('class',$class);
                if($disable) $elm->setAttrib('disabled',true);
            }
        return $this;
    }

    public function setAdminParams()
    {
        if($this->getElement('tema')){
            $this->removeElement('tema');
            $this->addElement('text','tema',array('label'=>'tema','class'=>'txt2'));
        }

        if($this->getElement('cidade')){
            $this->removeElement('cidade');
            $this->addElement('text','cidade',array('label'=>'região','class'=>'txt2'));
        }

        if($this->getElement('assunto')){
            $this->removeElement('assunto');
            $this->addElement('text','assunto',array('label'=>'assunto','class'=>'txt2'));
        }

        if($this->getElement('certificacao')){
            $this->removeElement('certificacao');
            $this->addElement('text','certificacao',array('label'=>'certificação','class'=>'txt2'));
        }

        if($this->getElement('horario')){
            $this->removeElement('horario');
            $this->addElement('text','horario',array('label'=>'horário','class'=>'txt2'));
        }

        if($this->getElement('participantes')){
            $this->getElement('participantes')->setLabel('n. de participantes');
        }

        $this->changeClasses('txt disabled',true);
    }

    public function isValid($values){
        if($this->getElement('cpf')) if(!Is_Cpf::isValid($values['cpf'])){
            $this->getElement('cpf')->markAsError();
            $this->addErrorMessage('CPF inválido');
        }

        if($this->getElement('cnpj')) if(!Is_Cpf::isValidCnpj($values['cnpj'])){
            $this->getElement('cnpj')->markAsError();
            $this->addErrorMessage('CNPJ inválido');
        }

        // validando campos select para seleção vazia == __none__ (título do campo)
        foreach ($this->getElements() as $elm) {
            if($elm->getType()=='Zend_Form_Element_Select'){
                if($values[$elm->getId()]=='__none__'){
                    $elm->markAsError();
                    $this->addErrorMessage('Campo '.$elm->getLabel().' inválido');
                }
            }
        }

        /*// if($this->mensagem_id==13 || $this->mensagem_id==15) if($values['tema']=='__none__'){
        if($this->getElement('tema')) if($values['tema']=='__none__'){
            $this->getElement('tema')->markAsError();
            $this->addErrorMessage('Tema inválido');
            return false;
        }

        // if($this->mensagem_id==13) if($values['cidade']=='__none__'){
        if($this->getElement('cidade')) if($values['cidade']=='__none__'){
            $this->getElement('cidade')->markAsError();
            $this->addErrorMessage('Cidade inválida');
            return false;
        }*/
        
        return parent::isValid($values);
    }

}

