<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
    protected function _initRoute()
    {
        $this->router = new Zend_Controller_Router_Rewrite();
		$this->request =  new Zend_Controller_Request_Http();
		$this->router->route($this->request); // pegar todos os parametros
        
        $this->bootstrap('view');
        $this->view = $this->getResource('view');
        $this->bootstrap('layout');
        $this->layout = $this->getResource('layout');

        if($this->isDirectAccess()) return null;
        
        if($this->request->getModuleName()=="default"){
            // dados da empresa
            $dados_empresa = new Application_Model_Db_DadosEmpresa();
            $dados_empresas = Is_Array::utf8DbRows(
                $dados_empresa->fetchAll(null,'ordem')
            );
            $this->view->dados_empresas = $dados_empresas;
            $this->view->dados_empresa  = array_shift($dados_empresas);
            $this->view->empresas       = $this->view->dados_empresas;
            $this->view->empresa        = $this->view->dados_empresa;
            $this->view->dados_filiais  = $dados_empresas;
            $this->view->filiais        = $this->view->dados_filiais;

            $_campos = $dados_empresa->s('paginas_campos_text','id,titulo,valor','pagina_id=1','ordem');
            $campos = array(); foreach($_campos as $_c) $campos[$_c->id] = $_c;
            $this->view->home_campos = $campos;
            $this->view->home_links = $campos;
        }
        
        if($this->request->getControllerName()=="admin"){
            $this->layout->setLayout("admin");
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            
            $this->view->isLogged = $this->view->logged = $auth->hasIdentity();
            $this->view->login = $auth->hasIdentity() ? $auth->getIdentity() : null;
        } else {
            $auth = new Zend_Session_Namespace(SITE_NAME.'_login_cliente');
            
            if((bool)@$auth->user){
                // verificando listas do cliente
                /*$chadebebe = new Application_Model_Db_Chadebebe();
                $listas = Is_Array::utf8dbResult(
                    $chadebebe->fetchAll('cliente_id = "'.$auth->user->id.'" and status = 1 and data_encerramento > "'.date('Y-m-d').'"')
                );
                $auth->user->chadebebe = $listas;*/
            }

            $this->view->isLogged = $this->view->logged = (bool)$auth;
            $this->view->login = $auth;
        }
		
        if(array_key_exists('busca',$this->request->getParams())){
            $this->view->busca = $this->request->getParam('busca');
        }
    }
    
    public function isDirectAccess()
    {
        return in_array($this->request->getControllerName(),array('img','public')) ||
               strstr($this->request->getActionName(), '.json') ||
               strstr($this->request->getActionName(), '.xml');
    }
    
    protected function _initLayoutConfigs(){
        $this->view->doctype('XHTML1_STRICT');
        $this->view->modulo = $this->request->getControllerName();
        $this->view->action = $this->request->getActionName();
        if(strstr($this->view->action,'?')) $this->view->action = reset(explode('?', $this->view->action));
        $this->view->controller = $this->request->getControllerName();
        $this->view->module = $this->request->getModuleName();

        if(($this->request->getControllerName() == 'admin') ||
           ($this->request->getControllerName() == 'portal')){
            $actionName = $this->request->getActionName();
            $actions = explode('/',$this->request->getRequestUri());
            // _d($actions);
            $curAction = end($actions);
            if($this->request->getControllerName() == 'portal') $curAction = (bool)@$actions[3] ? $actions[3] : 'index'; //end($actions);
            $s = array_search($actionName,$actions);
            $action = $curAction!=$actions[$s]?$curAction:'index';
            // _d($action);
            $this->view->action = $action;
            if(strstr($this->view->action,'?')) $this->view->action = reset(explode('?', $this->view->action));
            if(!(bool)trim($this->view->action)||strstr($this->view->action,'?')) $this->view->action = $actions[sizeof($actions)-1];
            if(!(bool)trim($this->view->action)||strstr($this->view->action,'?')) $this->view->action = $actions[sizeof($actions)-2];
            if(!(bool)trim($this->view->action)||strstr($this->view->action,'?')) $this->view->action = $actions[sizeof($actions)-3];
            $this->view->controller = $actionName;
            $this->view->module = $this->request->getControllerName();
            $this->view->action2 = $action;
            $this->view->controller2 = $actionName;
            $this->view->module2 = $this->request->getControllerName();
            $this->view->modulo2 = $this->request->getControllerName();
        }
        
        if($this->request->getControllerName() == 'portal'){
            $this->view->addScriptPath( APPLICATION_PATH . '/views/scripts' );
            $this->view->addhelperPath( APPLICATION_PATH . '/views/helpers' );
        }
        
        if($this->isDirectAccess()) return null;
        
        // adiciona a view a sessão do carrinho
        $this->view->cart = new Zend_Session_Namespace(SITE_NAME."_cart");
        if(!isset($this->view->cart->items)){
            $this->view->cart->items = array();
            $this->view->cart->forma = 1;
        }
		
		// switch($this->request->getControllerName()){
		// 	case 'admin':
		// 		$actionName = $this->request->getActionName();
		// 		$actions = explode('/',$this->request->getRequestUri());
		// 		$curAction = end($actions);
		// 		$s = array_search($actionName,$actions);
		// 		$action = $curAction!=$actions[$s]?$curAction:'index';
		// 		//_d($action);
		// 		$this->view->action = $action;
		// 		$this->view->controller = $actionName;
		// 		$this->view->module = $this->request->getControllerName();
		// 		break;
		// 	default:
		// 		$this->view->action = $this->request->getActionName();
		// 		$this->view->controller = $this->request->getControllerName();
		// 		$this->view->module = $this->request->getModuleName();
		// }
    }
    
    /**
	 * used for handling top-level navigation
	 * @return Zend_Navigation
	 */
	protected function _initNavigation()
	{
        if($this->isDirectAccess()) return null;
        if($this->request->getControllerName()=="admin"){   
            $login = new Zend_Session_Namespace(SITE_NAME.'_login');         
            $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation-admin.xml', 'nav');
            $config = $config->toArray();
            // _d(new Zend_Navigation($config['admin-top']));
            $this->view->menu = new stdClass();

            // lista paginas
            $pag = new Application_Model_Db_Paginas();
            $rows_pags = Is_Array::utf8DbResult($pag->fetchAll(
                'status_id = 1 '.
                    'and id not in (1,3) '.
                    'and alias not in ("'.$pag->model_row_alias.'")',
                array('ordem','id')));
            $subpags = array();
            
            if(count($rows_pags)){
                $config['admin-top']['paginas']['pages'] = array();

                foreach($rows_pags as $row_pag){
                    $page = array(
                        'label' => $row_pag->titulo,
                        'uri'   => ($row_pag->has_body) ?
                                    URL.'/admin/paginas/edit/'.$row_pag->id :
                                    '#',
                    );
                    
                    if($row_pag->parent_id) {
                        $k = 'pagina-'.$row_pag->parent_id;
                        if(!isset($subpags[$k])) $subpags[$k] = array();

                        if($row_pag->parent_id==9) if(!isset($subpags[$k]['pagina-9-banner'])) {
                            $subpags[$k]['pagina-9-banner'] = array(
                                'label' => 'Banner',
                                'uri'   => URL.'/admin/paginas/edit/9',
                            );
                        }
                        $subpags[$k]['pagina-'.$row_pag->id] = $page;

                        if($row_pag->id==13) {
                            $subpags[$k]['pagina-'.$row_pag->id.'-segmentos'] = array(
                                'label' => 'Segmentos',
                                'uri'   => URL.'/admin/segmentos',
                            );  
                        }

                        continue;
                    }
                    
                    $config['admin-top']['paginas']['pages']['pagina-'.$row_pag->id] = $page;
                }
            }

            if(count($subpags)) foreach ($subpags as $k => $subpag) {
                $config['admin-top']['paginas']['pages'][$k]['class'] = 'has-sub-sub';
                $config['admin-top']['paginas']['pages'][$k]['pages'] = $subpag;
            }

            // lista produtos
            $this->produtos = new Application_Model_Db_Produtos();
            $produtos = Is_Array::utf8DbResult($this->produtos->fetchAll(
                '1=1',
                'ordem'
            ));
            
            if((bool)$produtos){
                $config['admin-top']['produtos']['class'] = 'has-sub';
                $config['admin-top']['produtos']['pages'] = array();

                foreach ($produtos as $row) {
                    $config['admin-top']['produtos']['pages']['produto-'.$row->id] = array(
                        'label' => $row->titulo,
                        'uri'   => URL.'/admin/produtos/edit/'.$row->id,
                        'class' => 'has-sub-sub',
                        'pages' => array(
                            'modulos' => array(
                                'label' => 'Módulos',
                                'uri'   => URL.'/admin/modulos/?produto_id='.$row->id,
                            )
                        ),
                    );
                }
                
                // $config['admin-top']['produtos']['pages']['solucao-10'] = 
                //     $config['admin-top']['paginas']['pages']['pagina-9']['pages']['pagina-10'];
                // $config['admin-top']['produtos']['pages']['solucao-11'] = 
                //     $config['admin-top']['paginas']['pages']['pagina-9']['pages']['pagina-11'];
                $menu_paginas_servicos = $config['admin-top']['paginas']['pages']['pagina-9']['pages'];
                unset($menu_paginas_servicos['pagina-9-banner']);
                $config['admin-top']['produtos']['pages']['servicos'] = array(
                    'label' => 'Serviços',
                    'class' => 'has-sub-sub',
                    'uri'   => URL.'/admin/paginas/index/pid/9',
                    'pages' => $menu_paginas_servicos,
                );
                $config['admin-top']['produtos']['pages']['solucao-3'] = 
                    $config['admin-top']['home']['pages']['webinars'];
            }

            // lista mensagens contato
            $msg = new Application_Model_Db_Mensagens();
            $msgs = $msg->fetchAll('status_id=1 and id >= 11','titulo');

            if ($msgs) {
                $menu_msgs = array();
                $menu_msgs['mensagens-todas'] = array(
                    'label' => 'Todas',
                    'uri'   => URL.'/admin/mensagens-contato',
                );

                foreach ($msgs as $m) {
                    $menu_msgs['mensagens-'.$m->id] = array(
                        'label' => $m->titulo,
                        'uri'   => URL.'/admin/mensagens-contato/?search-by=mensagem_id&search-txt='.$m->id,
                        // 'uri'   => URL.'/admin/mensagens-contato/index/search-by/mensagem_id/search-txt/'.$m->id,
                    );
                }

                // _d($menu_msgs);
                $config['admin-top']['mensagens']['pages']['recebidas']['class'] = 'has-sub-sub';
                $config['admin-top']['mensagens']['pages']['recebidas']['pages'] = $menu_msgs;
            }

            self::configUrlPrefix($config['admin-top']);
            $this->view->menu->admin_top  = new Zend_Navigation($config['admin-top']);
        } else {
            if($this->request->getControllerName()=="portal") {
                // if(!$this->paginas) $this->paginas = new Application_Model_Db_Paginas();
                // $pagina_instrucoes = Is_Array::utf8DbRow($this->paginas->fetchRow('id=9'));
                // $this->view->pagina_instrucoes = $pagina_instrucoes;
                
                if(!$this->login) $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente');
                $pagina_instrucoes = $this->login->instrucoes_gerais;
                $this->view->pagina_instrucoes = $pagina_instrucoes;
                // _d($pagina_instrucoes);

                if(!$this->categorias_arquivos)
                    $this->categorias_arquivos = new Application_Model_Db_CategoriasArquivos();

                $menu_categ = $this->categorias_arquivos->getMenuByLogin($this->login->user->id);
                self::configUrlPrefix($menu_categ);
                $this->view->menu_categ = new Zend_Navigation($menu_categ);
            }
            
            $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
            $menu_nav = $_menu_nav->toArray();
            $menu_nav_top = $menu_nav['top'];
            $menu_nav_footer = $menu_nav['footer'];

            // adicionar links de serviços em soluções
            $this->paginas = new Application_Model_Db_Paginas();
            $pgs = $this->paginas->getPaginasWhere(
                'status_id = 1 '.
                'and alias not in ("'.$this->paginas->model_row_alias.'") '.
                'and parent_id = 9 ',
                false
            );
            $pgs_sol = array();
            $pgs_sol['produtos'] = array(
                'label' => 'Produtos',
                'uri' => '/solucoes/produtos',
            );
            foreach($pgs as $pg) $pgs_sol[$pg->alias] = array(
                'label' => $pg->titulo,
                'uri' => '/solucoes/solucao/'.$pg->alias.'-'.$pg->id,
            );
            $pgs_sol['webinars'] = array(
                'label' => 'Webinars',
                'uri' => '/solucoes/solucao/webinars-3',
            );
            $menu_nav_top['solucoes']['pages'] = $pgs_sol;

            $menu_nav_footer1 = array_slice($menu_nav_top, 0, 2);
            $menu_nav_footer2 = array_slice($menu_nav_top, 2, 2);
            $menu_nav_footer3 = array_slice($menu_nav_top, 4, 3);

            $menu_nav_footer3['contato']['class'] = 'has-sub';
            $menu_nav_footer3['contato']['pages'] = $menu_nav_footer['contato']['pages'];
            if($this->view->home_links) if(trim(@$this->view->home_links[1]->valor))
                $menu_nav_footer3['contato']['pages']['suporte']['uri'] = $this->view->home_links[1]->valor;
            
            // prefixos
            self::configUrlPrefix($menu_nav_top);
            self::configUrlPrefix($menu_nav_footer1);
            self::configUrlPrefix($menu_nav_footer2);
            self::configUrlPrefix($menu_nav_footer3);
            // self::configUrlPrefix($menu_nav['footer']);
			
            $this->view->menu = new stdClass();
            $this->view->menu->top        = new Zend_Navigation($menu_nav_top);
            $this->view->menu->footer1    = new Zend_Navigation($menu_nav_footer1);
            $this->view->menu->footer2    = new Zend_Navigation($menu_nav_footer2);
            $this->view->menu->footer3    = new Zend_Navigation($menu_nav_footer3);
            // $this->view->menu->footer    = new Zend_Navigation($menu_nav['footer']);
			
            $uri = APPLICATION_ENV == 'development' ?
                    URL.$this->request->getPathInfo() : // dev
                    URL.$this->request->getPathInfo();  // production
            
            foreach(get_object_vars($this->view->menu) as $menu){
                $activeNav = $menu->findByUri($uri) or
                $activeNav = $menu->findByUri(str_replace('http://'.$_SERVER['HTTP_HOST'],'',$uri));
                
                if(null !== $activeNav){
                    $activeNav->active = true;
                    $activeNav->setClass($activeNav->getClass()." active");	
                }
            }
        }
	}

    function configUrlPrefix(&$config)
    {
        foreach($config as &$c){
            // adiciona url ao link
            if(isset($c['uri'])) if($c['uri'] != '#' & !strstr($c['uri'],'http')) $c['uri'] = URL.$c['uri'];
            // adiciona recursão à função
            if(isset($c['pages'])) $c['pages'] = self::configUrlPrefix($c['pages']);
        }
        
        return $config;
    }
}