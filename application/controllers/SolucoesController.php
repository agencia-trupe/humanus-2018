<?php

class SolucoesController extends ZendPlugin_Controller_Action
{
	protected $_require = array(
		'table' => 'Db_Produtos',
		'paginas' => 'Db_Paginas',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $pgs = $this->paginas->getPaginasWhere(
            'status_id = 1 '.
            'and alias not in ("'.$this->paginas->model_row_alias.'") '.
            'and (id in (9,3) or parent_id = 9) '
        );
        $this->view->pagina1 = $pgs[9];
        $this->view->pagina2 = $pgs[10];
        $this->view->pagina3 = $pgs[11];
        $this->view->pagina4 = $pgs[3];
        $p3 = $pgs[3]; unset($pgs[3]); array_push($pgs, $p3);
        $this->view->paginas = $pgs;
        
        $produtos = $this->table->fetchAllWithPhoto('t1.status_id=1','t1.destaque desc, t1.ordem');
        $produtos = $this->table->parseModulos($produtos);
        $this->view->produtos = $produtos;

        $this->view->pagina_ativa = $this->_getParam('pagina');
    }

    public function index1Action()
    {
    	$paginas = $this->paginas->getPaginas('9,10,11,3');
    	$this->view->pagina1 = $paginas[9];
    	$this->view->pagina2 = $paginas[10];
        $this->view->pagina3 = $paginas[11];
    	$this->view->pagina4 = $paginas[3];
        
        $produtos = $this->table->fetchAllWithPhoto('t1.status_id=1','t1.destaque desc, t1.ordem');
		$produtos = $this->table->parseModulos($produtos);
		$this->view->produtos = $produtos;

        $this->view->pagina_ativa = $this->_getParam('pagina');
    }

    public function produtoAction()
    {
    	$alias = $this->_getParam('alias');
    	$id = ($alias) ? (int)end(explode('-', $alias)) : null;
    	$where = 't1.id = "'.$id.'" or t1.alias = "'.$alias.'"';
    	if(!$produto = $this->table->fetchRowWithPhoto($where))
    		return $this->_forward('not-found','error','default',array('url'=>URL.'/solucoes'));
    	$produto->modulos = $this->table->getModulos($produto->id,' and status_id=1',1);
    	
    	$this->view->produto = $produto;
    }

    public function solucaoAction()
    {
    	$alias = $this->_getParam('alias');
    	$id = ($alias) ? (int)end(explode('-', $alias)) : null;
    	if(!$pagina = $this->paginas->getPagina($id))
    		return $this->_forward('not-found','error','default',array('url'=>URL.'/solucoes'));
    	$this->view->pagina = $pagina;

        if($pagina->id==3) {
            $this->view->webinars = $this->table->s('videos','id,alias,titulo,url','status_id=1','ordem');
            $this->_helper->viewRenderer('webinars');
        }
    }

}

