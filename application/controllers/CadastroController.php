<?php

class CadastroController extends Zend_Controller_Action
{

    public function init()
    {
        // if(APPLICATION_ENV=='production') return $this->_redirect('/');
        // models
        $this->messenger = new Helper_Messenger();
        $this->paginas = new Application_Model_Db_Paginas();
        $this->clientes = new Application_Model_Db_Clientes();
        $this->clientes_enderecos = new Application_Model_Db_ClientesEnderecos();
        $this->clientes_dados_cobranca = new Application_Model_Db_ClientesDadosCobranca();
        // $this->promocoes = new Application_Model_Db_Promocoes();
        $this->mensagens = new Application_Model_Db_Mensagens();
        $this->sessionCadastro = new Zend_Session_Namespace(SITE_NAME."_cadastro");
        // _d($this->view->sessionCadastro);

        $pagina = $this->paginas->getPagina('treinamentos');
        $this->view->pagina = $pagina;
        $this->view->pagina_infoad = $this->paginas->getPagina(15);
        $this->view->steps = true;

        if($this->view->action=='index2' && $this->_hasParam('continue')){
            $continue = unserialize(base64_decode($this->_getParam('continue')));
            
            $cliente = Is_Array::utf8DbRow($this->clientes->fetchRow(
                'id="'.$continue->cliente_id.'"'
            ));

            if($cliente){
                $this->sessionCadastro->continue = true;
                $this->sessionCadastro->dados = (array)$cliente;
                $this->view->continue_cadastro = true;
                $this->view->steps = false;
            }

            $this->pedidos = new Application_Model_Db_Pedidos();
            $pedido = $this->pedidos->fetchRow('sessid = "'.$continue->pedido_sessid.'"');

            if($pedido){
                $this->sessionCadastro->pedido = $pedido;
                $pedido_items = $this->pedidos->getItens($pedido->id,true);
                $this->sessionCadastro->pedido_items = $pedido_items;
                $this->view->continue_items = $pedido_items;
                $this->view->steps = true;
            }
        }

        if(($this->view->action=='index2' && !$this->_hasParam('data')) ||
           ($this->view->action=='index2-grupo' && !$this->_hasParam('data')) ||
           ($this->view->action=='index3' && !$this->_hasParam('data'))){
            if($this->view->login->user && !(bool)$this->sessionCadastro->dados)
                $this->sessionCadastro->dados = array_map('utf8_encode',(array)$this->view->login->user);
        }
        
        $this->view->sessionCadastro = $this->sessionCadastro;
        $this->view->dadosCadastro = $this->sessionCadastro->dados;
        // _d($this->login->user,0);
        // _d($this->sessionCadastro->dados);

        $areasCheckVagas = 'index32,save2grupo';
        if(count(@$this->view->cart->items) && in_array($this->view->action, explode(',', $areasCheckVagas))){
            foreach($this->view->cart->items as $item){
                // checagem de vagas
                $vagas_restantes = $item->item->vagas_disponiveis-$item->item->vagas_preenchidas;
                if($this->sessionCadastro->isGrupo) $vagas_restantes = $vagas_restantes - $this->sessionCadastro->grupoQtde;
                else if(isset($_POST['email']) && sizeof($_POST['email'])>1) $vagas_restantes = $vagas_restantes - sizeof($_POST['email']);
                if(!($vagas_restantes > 0)) $this->_redirect('cadastro/interesse/produto/'.$item->item->id);
            }
        }

        $areasCheckCadCons = 'consultoria,save1';
        if(!in_array($this->view->action, explode(',', $areasCheckCadCons))){
            return $this->_redirect('cadastro/consultoria');
        }
    }

    public function indexAction()
    {
        $form_meus_dados = new Application_Form_MeusDados1();
        $form_meus_dados->setAction(URL.'/cadastro/save1/');
        
        if($this->_hasParam('data')){
            $form_meus_dados->populate($this->_getParam('data'));
        }
        
        $this->view->form_meus_dados = $form_meus_dados;

        $incompany = $this->_hasParam('incompany');
        $incompany_id = (int)$this->_getParam('incompany');
        $this->view->incompany = $incompany;
        $this->view->incompany_id = $incompany_id;
        $this->view->return = $this->_getParam('return');
    }

    public function save1Action()
    {
        $incompany = $this->_hasParam('incompany');
        $incompany_id = (int)$this->_getParam('incompany');
        $url_red1 = URL.'/cadastro';
        $url_red2 = URL.'/cadastro/choose';
        $area = null;

        if($this->_hasParam('area')) {
            $area = $this->_getParam('area');
            if($area=='consultoria'){
                $url_red1 = URL.'/cadastro/consultoria';
                $url_red2 = URL.'/portal';
            }
            if($area=='grupo'){
                $url_red1 = URL.'/cadastro/grupo';
                $url_red2 = URL.'/portal';
            }
            if($area=='incompany'){
                $url_red1 = URL.'/cadastro?return=cadastro.index2-grupo&incompany='.$incompany_id;
                $url_red2 = URL.'/cadastro/index2-grupo';
            }
        }

        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$url_red1));
            return;
        }
        
        $post = $this->getRequest()->getPost();
        $form = new Application_Form_MeusDados1();
        $form->requireSenha();
        
        if($form->isValid($post)){
            $redirect_url = Application_Model_Login::getRedirectUrl();
            Application_Model_Login::unsetRedirectUrl();
            // $redirect_url = $redirect_url ? $redirect_url : URL.'/login/choose';
            $redirect_url = $redirect_url ? $redirect_url : $url_red2;
            if($area=='incompany') $redirect_url = $url_red2;
            
            try{
                $post = ($form->getValues());
                if(APPLICATION_ENV!='development1') $post = Is_Array::deUtf8All($post);
                $dados_login = array('login'=>$post['email'],'senha'=>$post['senha']);
                $post['nome'] = reset(explode('@',$post['email']));
                $post['senha'] = md5($post['senha']);
                $post['status_id'] = 1;

                $row_consult = null;
                $row_grupo = null;
                if($area=='consultoria') {
                    $row_consult = $this->clientes->fetchRow('email="'.$post['email'].'" and senha is null');
                    $post['cliente_treinamentos'] = 0;
                    $post['cliente_consultoria'] = 1;
                }
                if($area=='grupo') {
                    $row_grupo = $this->clientes->fetchRow('email="'.$post['email'].'" and senha is null');
                }
                if($area=='incompany') {
                    $row_incompany = $this->clientes->fetchRow('email="'.$post['email'].'" and senha is null');
                    $row_incompany2 = $this->clientes->fetchRow('email="'.$post['email'].'" and senha = "'.$post['senha'].'"');
                }
                
                $unsets = 'senhac,emailc,area';
                foreach(explode(',',$unsets) as $u) if(isset($post[$u])) unset($post[$u]);
                
                if($area=='consultoria' && $row_consult){
                    $cliente_id = $row_consult->id;
                    $row_consult->senha = $post['senha'];
                    $row_consult->save();
                } else if($area=='grupo' && $row_grupo){
                    $cliente_id = $row_grupo->id;
                    $row_grupo->senha = $post['senha'];
                    $row_grupo->save();

                    // aviso de criacao de senha
                    $msg = $this->mensagens->get(24);
                    $html = $msg->body;
                    $html.= '<br><p>';
                    $html.= '<b>Nome do cliente:</b> '.$row_grupo->nome.'<br>';
                    $html.= '<b>E-mail de cadastro:</b> '.$row_grupo->email.'<br>';
                    $html.= '</p>';
                    try { Trupe_Humanus_Mail::send(
                        $msg->email,
                        ucfirst(reset(explode('@', $msg->email))),
                        $msg->subject,
                        $html
                    ); } catch (Exception $e) { }
                } else if($area=='incompany'){ // cadastro incompany
                    // verificar email==treinamento.email
                    if(!$this->produtos) $this->produtos = new Application_Model_Db_Produtos();
                    $row_incompany_treinamento = $this->produtos->fetchRow(
                        'id = "'.addslashes($incompany_id).'" '.
                        'and incompany_email = "'.addslashes($post['email']).'" '.
                        'and incompany=1 '
                    );
                    
                    // email diferente de incompany > exibe erro
                    if(!$row_incompany_treinamento){
                        $this->messenger->addMessage('Este curso é restrito a uma Empresa pré-cadastrada.','error');
                        return $this->_redirect($url_red1);
                    }

                    // email certo
                    // sem senha (row): cadastra senha, faz login, continua
                    if($row_incompany){
                        $cliente_id = $row_incompany->id;
                        $row_incompany->senha = $post['senha'];
                        $row_incompany->save();
                    } else                
                    // com senha (row2):
                    if($row_incompany2){
                        // senha certa: faz login, continua
                        $cliente_id = $row_incompany2->id;
                    } else {
                        // senha errada: volta,  msg erro login
                        // sem row: volta, msg erro login
                        $this->messenger->addMessage('Login não encontrado.','error');
                        return $this->_redirect($url_red1);
                    }
                } else {
                    $cliente_id = $this->clientes->insert($post);
                }
                $post['id'] = $cliente_id;
                $this->sessionCadastro->dados = $post;

                $login = Application_Model_LoginCliente::process($dados_login);

                // $this->messenger->addMessage('Login criado! Finalize seu cadastro:');
                // $this->_redirect($redirect_url);
                $this->_redirect($url_red2);
                // $this->_forward('index');
                // echo Js::script('window.setTimeout(function(){ window.location = "'.$redirect_url.'" },3000)');
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),'uplicate') ? 'E-mail já cadastrado. Faça seu login.' : $e->getMessage();
                $this->messenger->addMessage($msg,'error');
                // print Js::alert($msg);
                print Js::location($redirect_url);
                exit();
            }
        } else {
            $this->messenger->addMessage(nl2br($form->getMessage()),'error');
            // $this->messenger->addMessage('Preencha todos os campos corretamente.','error');
            // $this->_forward('index',null,null,array('data'=>$post));
            $this->_redirect($url_red1);
        }
    }

    public function interesseAction()
    {
        $this->aviseme = new Application_Model_Db_ProdutosAviseme();
        $this->view->produto_id = $this->_getParam('produto');

        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            $post = Is_Array::utf8All($post);
            $form = new Application_Form_Interesse();

            if($form->isValid($post)){
                $post['data_cad'] = date('Y-m-d H:i:s');
                $post['telefone'] = Is_Cpf::clean($post['telefone']);
                $this->aviseme->insert($post);
                $this->view->interesse_ok = true;
            } else {
                $this->messenger->addMessage($form->getMessage('<br>',true),'error');
                return $this->_redirect('cadastro/interesse/produto/'.$post['produto_id']);
            }
        }
    }

    public function chooseAction() { }

    public function index2Action()
    {
        $form_meus_dados = new Application_Form_MeusDados2(true);
        $form_meus_dados->setAction(URL.'/cadastro/save2/');
        
        if(!$this->sessionCadastro->dados && (!$this->_hasParam('continue') && !$this->_hasParam('c'))){
            return $this->_forward('denied','error');
        }
        if($this->_hasParam('data')){
            $form_meus_dados->populate($this->_getParam('data'));
        }
        if((bool)$this->sessionCadastro->dados){
            $form_meus_dados->populate($this->sessionCadastro->dados);
        }

        $this->view->form_meus_dados = $form_meus_dados;
    }

    public function index2GrupoAction()
    {
        // 
    }

    public function save2Action()
    {
        if(!$this->_request->isPost() || !$this->sessionCadastro->dados){
            $this->_forward('denied','error','default',array('url'=>URL.'/cadastro/'));
            return;
        }
        
        $post = $this->getRequest()->getPost();
        $form = new Application_Form_MeusDados2(true);
        $this->sessionCadastro->dados = $post+$this->sessionCadastro->dados;

        $continue_id = null;
        if(isset($post['id']) && isset($post['senha'])){
            $form->requireSenha();
            $continue_id = $post['id'];
        }
        
        if($form->isValid($post)){
            $redirect_url = Application_Model_Login::getRedirectUrl();
            Application_Model_Login::unsetRedirectUrl();
            // $redirect_url = $redirect_url ? $redirect_url : URL.'/login/choose';
            $redirect_url = $redirect_url ? $redirect_url : URL.'/cadastro/choose';
            
            try{
                $post = ($form->getValues());
                if(APPLICATION_ENV!='development1') $post = Is_Array::deUtf8All($post);
                if(!$this->login) $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
                // $post['data_nascimento'] = Is_Date::br2am($post['data_nascimento']);
                $post['data_cad'] = date('Y-m-d H:i:s');
                $post['data_edit'] = date('Y-m-d H:i:s');
                $post['status_id'] = 1;
                $post['telefone'] = Is_Str::removeCaracteres($post['telefone']);
                $post['celular'] = Is_Str::removeCaracteres($post['celular']);
                $post['cep'] = Is_Str::removeCaracteres($post['cep']);
                $post['cpf'] = Is_Str::removeCaracteres($post['cpf']);
                if(!strstr($post['site'],'http')) $post['site'] = 'http://'.$post['site'];

                if((bool)trim($post['cpf'])) {
                    $post['cnpj'] = new Zend_Db_Expr('NULL');
                }

                if(isset($post['nome'])) if((bool)trim($post['nome'])) {
                    if($this->login->user) $this->login->user->nome = $post['nome'];
                }

                if(isset($post['senha']) && trim(@$post['senha'])!=''){
                    $post['senha'] = md5($post['senha']);
                }

                $ufs = $form->getElement('uf_id')->getMultiOptions();
                // $ufs = $form->getElement('uf')->getMultiOptions();
                // $post['uf_id'] = $post['uf'];
                $post['uf'] = $ufs[$post['uf_id']];
                
                if($post['local_id'] == '__none__'){
                    $post['local_id'] = new Zend_Db_Expr('NULL');
                }

                $unsets = 'senhac,emailc';
                foreach(explode(',',$unsets) as $u) if(isset($post[$u])) unset($post[$u]);
                
                $this->sessionCadastro->dados = $post+$this->sessionCadastro->dados;

                // atualiza dados da sessao
                $newUser = $this->sessionCadastro->dados+(array)$this->login->user;
                if($this->login->user) $this->login->user = (object)$newUser;

                // $cliente_id = $this->clientes->insert($post);
                $cliente_id = $this->sessionCadastro->dados['id'];
                $this->clientes->update($post,'id = "'.$cliente_id.'"');

                // insere endereço na lista de endereços do cliente
                // $dados_endereco = $post;
                // $unsets = 'nome,sobrenome,apelido,cpf,email,emailc,senha,senhac,data_nascimento,sexo,status_id';
                // foreach(explode(',',$unsets) as $u) if(isset($dados_endereco[$u])) unset($dados_endereco[$u]);
                // $dados_endereco['principal'] = 1;
                // $dados_endereco['cliente_id'] = $cliente_id;
                // $dados_endereco['user_cad'] = $cliente_id;

                // $this->clientes_enderecos->insert($dados_endereco);

                // envia email de confirmação de cadastro
                $mensagem = $this->mensagens->get(1);
                $html = $mensagem->body;
                $subject = $mensagem->subject;
                try { Trupe_Humanus_Mail::send($this->sessionCadastro->dados['email'],$post['nome'],$subject,$html); } catch (Exception $e) { }

                $subject2 = 'Novo cadastro realizado no site';
                $html2 = $subject2.':<br><br>';
                $html2.= '<a href="'.URL.'/admin/clientes/edit/'.$cliente_id.'">Visualizar cadastro</a>';
                $html2.= '<br><br>';
                try { Trupe_Humanus_Mail::send($mensagem->email,$mensagem->contato,$subject2,$html2); } catch (Exception $e) { }

                // finalizando: adicionando mensagem de retorno e redirecionando
                $this->messenger->addMessage($continue_id?
                    'Cadastro realizado com sucesso!':
                    'Cadastro realizado com sucesso!<br>Escolha sua forma de pagamento:');
                
                $redirect_url = $continue_id ? URL.'/login' : URL.'/cadastro/index3';
                if($continue_id) $redirect_url = URL.'/treinamento/'.
                                                 $this->sessionCadastro->pedido_items[0]->produto->treinamento->alias.'-'.
                                                 $this->sessionCadastro->pedido_items[0]->produto->treinamento->id;
                // $this->_redirect($redirect_url);
                $this->_redirect($redirect_url);
                // $this->_forward('index');
                // echo Js::script('window.setTimeout(function(){ window.location = "'.$redirect_url.'" },3000)');
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),'uplicate') ? 'CPF já cadastrado. Faça seu login.' : $e->getMessage();
                $this->messenger->addMessage($msg,'error');
                // print Js::alert($msg);
                print Js::location($redirect_url);
                exit();
            }
        } else {
            $this->messenger->addMessage(nl2br($form->getMessage()),'error');
            // $this->messenger->addMessage('Preencha todos os campos corretamente.','error');
            // $this->_forward('index2',null,null,array('data'=>$post));
            $this->_redirect('cadastro/index2'.($continue_id?'?continue='.base64_encode($continue_id):''));
        }
    }

    public function save2grupoAction()
    {
        if(!$this->_request->isPost() || !$this->sessionCadastro->dados){
            $this->_forward('denied','error','default',array('url'=>URL.'/cadastro/'));
            return;
        }
        
        $post = $this->getRequest()->getPost();
        $err = array();
        $grupoIds = array();

        $unsets = 'controller,action,module';
        foreach(explode(',', $unsets) as $u) if(isset($post[$u])) unset($post[$u]);

        for ($i=0; $i < sizeof($post['email']); $i++) { 
            $c = array();
            if(trim($post['email'][$i])!='') {
                $c = array(
                    'nome'  => utf8_decode($post['nomecompleto'][$i]),
                    'email' => $post['email'][$i],
                    'status_id'  => '1',
                    'cliente_id' => $this->sessionCadastro->dados['id'],
                );

                try {
                    $c['id'] = $this->clientes->insert($c);
                    $grupoIds[] = $c['id'];
                    $this->clientes->sendMailCompletarCadastroGrupo($c['id'],(object)$c);
                } catch(Exception $e){
                    if(strstr($e->getMessage(), 'uplicate')){
                        $this->clientes->update($c,'email="'.$c['email'].'"');
                        $row_cliente = $this->clientes->fetchRow('email="'.$c['email'].'"');
                        $c['id'] = $row_cliente->id;
                        $grupoIds[] = $c['id'];
                        // $this->clientes->sendMailCompletarCadastro($c['id'],(object)$c);
                        // $this->clientes->sendMailCompletarCadastroGrupo($c['id'],(object)$c);
                        $this->clientes->sendMailConviteGrupo($c['id'],(object)$c);
                    } else {
                        $err[] = $e->getMessage();
                    }
                }
            }
        }

        try {
            $mensagem = $this->mensagens->get(1);
            $subject2 = 'Novo cadastro realizado no site';
            $html2 = $subject2.':<br><br>';
            $html2.= '<a href="'.URL.'/admin/clientes/edit/'.$this->sessionCadastro->dados['id'].'">Visualizar cadastro</a>';
            Trupe_Humanus_Mail::send($mensagem->email,$mensagem->contato,$subject2,$html2);
        } catch (Exception $e) {
            // 
        }

        if(count($err)) {
            $this->messenger->addMessage(implode('<br>', $err),'error');
            return $this->_redirect('cadastro/index2-grupo');
        }
        
        // $this->messenger->addMessage('Grupo inserido! Finalize seu cadastro:');
        // return $this->_redirect('cadastro/index2');
        $this->sessionCadastro->isGrupo = true;
        $this->sessionCadastro->grupoQtde = sizeof($post['email']);
        $this->sessionCadastro->grupoIds  = $grupoIds;
        // _d($grupoIds);

        $carrinho = new Application_Model_Carrinho();
        
        $isCursoFromIncompany = $carrinho->checkHasCursoIncompany($this->view->login->user->email);
        $this->sessionCadastro->isCursoFromIncompany = $isCursoFromIncompany;

        $carrinho->updateQtde(sizeof($post['email']));
        return $this->_redirect('cadastro/index32');
    }

    public function index3Action() {
        // if(APPLICATION_ENV=='production') return $this->_forward('not-found','error');
        if(isset($_SERVER['HTTP_REFERER']))
            if(strstr($_SERVER['HTTP_REFERER'], 'index2') && strstr($_SERVER['HTTP_REFERER'], 'continue=')){
                $this->messenger->addMessage('Obrigado! Inscrição finalizada com sucesso!<br>Faça seu login!');
                return $this->_redirect('portal');
            }

        if($this->_hasParam('responsavel-pagamento')){
            $resp = $this->_getParam('responsavel-pagamento');
            
            if($resp=='0'){
                if(!$this->login) $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
                $dados_clone = (bool)$this->sessionCadastro->dados ?
                                $this->sessionCadastro->dados :
                                $this->login->user;
                $this->clientes_dados_cobranca->cloneFromCliente($dados_clone['id']);

                if((bool)$this->view->cart->items){
                    // $this->messenger->addMessage('Dados salvos.<br>Confirme sua compra.');
                    // return $this->_redirect('meu-carrinho/pagamento');
                    $this->messenger->addMessage('Dados salvos.<br>Confirme seus dados de pagamento.');
                    // return $this->_forward('index31');
                    return $this->_redirect('cadastro/index31');
                }

                $this->messenger->addMessage('Seu cadastro foi realizado com sucesso.');
                return $this->_redirect('/');
            }
            
            return $this->_forward('index3'.$resp);
            // return $this->_redirect('cadastro/index3'.$resp);
        }
    }

    public function index31Action() {
        $form = new Application_Form_MeusDadosCobranca1();
        $row = ($this->clientes_dados_cobranca->fetchRow('cliente_id="'.$this->sessionCadastro->dados['id'].'"','id desc'));
        if((bool)$row){
            $form->populate((array)$row);
            $this->view->row_dados = $row;
        }
        $this->view->form = $form;
    }
    public function index32Action() {
        $form = new Application_Form_MeusDadosCobranca2();
        $row = _utfRow($this->clientes_dados_cobranca->fetchRow('cliente_id="'.$this->sessionCadastro->dados['id'].'"','id desc'));
        if((bool)$row){
            $form->populate((array)$row);
            $this->view->row_dados = $row;
        }
        $this->view->form = $form;
    }

    public function save3Action()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/cadastro/'));
            return;
        }
        
        $post = $this->getRequest()->getPost();
        $form = (bool)$post['cpf'] ? new Application_Form_MeusDadosCobranca1() : new Application_Form_MeusDadosCobranca2();
        $this->sessionCadastro->dados_cobranca = $post;
        
        if($form->isValid($post)){
            try{
                // $post = ($form->getValues());
                if(APPLICATION_ENV!='development1') $post = Is_Array::deUtf8All($post);
                $row = $this->clientes_dados_cobranca->fetchRow('cliente_id="'.$post['cliente_id'].'"','id desc');

                $post['cep'] = Is_Str::removeCaracteres($post['cep']);
                $post['telefone'] = Is_Str::removeCaracteres(@$post['telefone']);
                $post['celular'] = Is_Str::removeCaracteres(@$post['celular']);
                if((bool)$post['cpf']){
                    $post['cnpj'] = null;
                    $post['cpf'] = Is_Str::removeCaracteres($post['cpf']);
                    if(!$row || @$row->tipo_pessoa!='0') $post['tipo_pessoa'] = 1;
                }
                if((bool)$post['cnpj']){
                    $post['cpf'] = null;
                    $post['cnpj'] = Is_Str::removeCaracteres($post['cnpj']);
                    $post['tipo_pessoa'] = 2;
                }
                $post['cliente_id'] = $this->sessionCadastro->dados['id'];
                $post['user_cad'] = $this->sessionCadastro->dados['id'];
                $post['user_edit'] = $this->sessionCadastro->dados['id'];
                $post['data_cad'] = date('Y-m-d H:i:s');
                $post['data_edit'] = date('Y-m-d H:i:s');

                $ufs = $form->getElement('uf')->getMultiOptions();
                $post['uf_id'] = $post['uf'];
                $post['uf'] = $ufs[$post['uf_id']];
                
                // unset in post for update
                $unsets = 'senhac,emailc,codigo-promocional,termos';

                // check cupom desconto + add to unset if true
                if(MOD_CUPOM_DESCONTO && isset($post['item-cod-cupom-desconto'])){
                    $this->carrinho = new Application_Model_Carrinho();
                    $this->carrinho->addCupom($post);
                    $unsets.= ',item-valor-cupom-desconto,item-valor-total-cupom-desconto,item-id-cupom-desconto,item-cod-cupom-desconto,item-amount-cupom-desconto';
                }

                foreach(explode(',',$unsets) as $u) if(isset($post[$u])) unset($post[$u]);
                
                if($row){
                    $this->clientes_dados_cobranca->update($post, 'id="'.$row->id.'"');
                    $dados_id = $row->id;
                } else {
                    $dados_id = $this->clientes_dados_cobranca->insert($post);
                }
                $post['id'] = $dados_id;
                $this->sessionCadastro->dados_cobranca = $post;

                // clonando dados de pagamento juridico p/ cliente se for cadastro de grupo
                if($this->sessionCadastro->isGrupo){
                    $dados_cliente = $post;
                    $dados_cliente_id = $post['cliente_id'];
                    unset($dados_cliente['id']);
                    unset($dados_cliente['cliente_id']);
                    unset($dados_cliente['tipo_pessoa']);
                    unset($dados_cliente['email']);
                    unset($dados_cliente['funcao_area']);
                    unset($dados_cliente['nf_resp']);
                    unset($dados_cliente['nf_resp_email']);
                    unset($dados_cliente['nf_num_pedido']);
                    unset($dados_cliente['forma_pagamento']);
                    unset($dados_cliente['insc_est']);

                    $this->clientes->update($dados_cliente,'id="'.$dados_cliente_id.'"');
                }

                // $this->_redirect($redirect_url);
                if((bool)$this->view->cart->items){
                    $this->messenger->addMessage('Dados salvos.<br>Confirme sua compra.');
                    return $this->_redirect('meu-carrinho/pagamento');
                }

                $this->messenger->addMessage('Seu cadastro foi realizado com sucesso.');
                return $this->_redirect('/treinamentos');
                // $this->_forward('index');
                // echo Js::script('window.setTimeout(function(){ window.location = "'.$redirect_url.'" },3000)');
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),'uplicate') ? 'E-mail já cadastrado. Faça seu login.' : $e->getMessage();
                $this->messenger->addMessage($msg,'error');
                // print Js::alert($msg);
                print Js::location(strstr($e->getMessage(),'uplicate') ? URL.'/login?return=cadastro.index3' : URL.'/cadastro/index3'.$post['tipo_pessoa']);
                exit();
            }
        } else {
            $this->messenger->addMessage(nl2br($form->getMessage("\n",1)),'error');
            // $this->messenger->addMessage('Preencha todos os campos corretamente.','error');
            // $this->_forward('index',null,null,array('data'=>$post));
            // $this->_redirect('cadastro/index3');
            $actionReturn = 'index3';
            if(isset($_SERVER['HTTP_REFERER']))
                if(strstr($_SERVER['HTTP_REFERER'], 'index32'))
                    $actionReturn = 'index32';
            $this->_redirect('cadastro/'.$actionReturn);
        }
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/cadastro/'));
            return;
        }
        
        $post = $this->getRequest()->getPost();
        $form = new Application_Form_MeusDados();
        $form->requireSenha();
        
        if($form->isValid($post)){
            $redirect_url = Application_Model_Login::getRedirectUrl();
            Application_Model_Login::unsetRedirectUrl();
            $redirect_url = $redirect_url ? $redirect_url : URL.'/login';
            
            try{
                $post = ($form->getValues());
                if(APPLICATION_ENV=='development') $post = Is_Array::deUtf8All($post);
                $post['data_nascimento'] = Is_Date::br2am($post['data_nascimento']);
                $post['data_cad'] = date('Y-m-d H:i:s');
                $post['senha'] = md5($post['senha']);
                $post['status_id'] = 1;
                $post['telefone'] = Is_Str::removeCaracteres($post['telefone']);
                $post['celular'] = Is_Str::removeCaracteres($post['celular']);
                $post['cep'] = Is_Str::removeCaracteres($post['cep']);
                $post['cpf'] = Is_Str::removeCaracteres($post['cpf']);

                $ufs = $form->getElement('uf')->getMultiOptions();
                $post['uf_id'] = $post['uf'];
                $post['uf'] = $ufs[$post['uf_id']];
                
                if($post['local_id'] == '__none__'){
                    $post['local_id'] = new Zend_Db_Expr('NULL');
                }

                $unsets = 'senhac,emailc';
                foreach(explode(',',$unsets) as $u) if(isset($post[$u])) unset($post[$u]);
                
                $cliente_id = $this->clientes->insert($post);

                // insere endereço na lista de endereços do cliente
                $dados_endereco = $post;
                $unsets = 'nome,sobrenome,apelido,cpf,email,emailc,senha,senhac,data_nascimento,sexo,status_id';
                foreach(explode(',',$unsets) as $u) if(isset($dados_endereco[$u])) unset($dados_endereco[$u]);
                $dados_endereco['principal'] = 1;
                $dados_endereco['cliente_id'] = $cliente_id;
                $dados_endereco['user_cad'] = $cliente_id;

                $this->clientes_enderecos->insert($dados_endereco);

                // envia email de confirmação de cadastro
                $mensagem = $this->mensagens->get(1);
                $html = $mensagem->body;
                $subject = $mensagem->subject;
                try { Trupe_Humanus_Mail::send($post['email'],$post['nome'],$subject,$html); } catch (Exception $e) { }
                
                // finalizando: adicionando mensagem de retorno e redirecionando
                $this->messenger->addMessage('<div class="'.$this->view->controller.'">'.
                                             /*(trim($mensagem->body) ? $mensagem->body : 
                                                '<h3>Parabéns</h3>'.
                                                '<p>O seu cadastro foi realizado com sucesso</p>').*/
                                             '<p>Cadastro realizado com sucesso!</p>'.
                                             '</div>');
                $this->_redirect($redirect_url);
                // $this->_forward('index');
                // echo Js::script('window.setTimeout(function(){ window.location = "'.$redirect_url.'" },3000)');
            } catch(Zend_Mail_Protocol_Exception $e){
                $this->messenger->addMessage('Seu cadastro foi salvo mas não conseguimos enviar o e-mail de confirmação. Por favor, cheque seus dados de cadastro fazendo login no site.','error');
                $this->_redirect('cadastro',array('data'=>$post));
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),'uplicate') ? 'CPF/E-mail já cadastrados. Redirecionando para página de login.' : $e->getMessage();
                $this->messenger->addMessage($msg,'error');
                // print Js::alert($msg);
                print Js::location($redirect_url);
                exit();
                //$this->_redirect('login/');
            }
        } else {
            $this->messenger->addMessage('Preencha todos os campos corretamente.','error');
            $this->_forward('index',null,null,array('data'=>$post));
        }
    }

    public function consultoriaAction() { }
    public function grupoAction() { }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}