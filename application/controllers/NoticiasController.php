<?php

class NoticiasController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        // models
        $this->categorias = new Application_Model_Db_CategoriasNoticias();
        $this->noticias = new Application_Model_Db_Noticias();
        $this->table = $this->noticias;
        // $this->links = new Application_Model_Db_Links();
        
        // $this->view->meta_description = 'Últimas notícias de '.SITE_NAME;
        // $this->view->meta_canonical = URL.'/noticias/';
        $this->view->meta_description = 'Últimos artigos e novidades de '.SITE_NAME;
        $this->view->meta_canonical = URL.'/blog';
        $this->view->url_noticias = URL.'/blog';

        $categorias = Is_Array::utf8DbResult($this->categorias->fetchAll(
            'status_id = 1',
            'ordem'
        ));

        $this->view->categorias = $categorias;
    }

    public function indexAction()
    {
        // $this->view->titulo = 'Notícias';
        $this->view->titulo = 'BLOG';

        $where = 'status_id = 1 ';
        $order = 'destaque desc, id desc';
        
        if($this->_hasParam('categoria')) {
            $alias = $this->_getParam('categoria');
            $categoria = $this->categorias->fetchRow('alias = "'.$alias.'"');
            $categoria = Is_Array::utf8DbRow($categoria);
            $where.= 'and categoria_id = "'.$categoria->id.'" ';
            // $this->view->titulo.= ' em '.$categoria->descricao;
            $this->view->categoria = $categoria;
        }

        $rows = $this->page_search('',$where,$order,13,10);
        $rows = $this->noticias->parseUrls($rows);
        // $rows = $this->noticias->parseCateg($rows);
        // _d($rows);
        
        $limitd = 3;
        $rowsd = array();
        $rowsd_ids = array();
        foreach($rows as $row) if(sizeof($rowsd)<$limitd)
            if($row->destaque) {
                $rowsd[] = $row;
                $rowsd_ids[] = $row->id;
            }
        
        $this->view->noticias = $rows;
        // $this->view->noticias_destaque = Is_Array::utf8DbResult($rowsd);
        $this->view->noticias_destaque = $rowsd;
        $this->view->noticias_destaque_ids = $rowsd_ids;
        return;
    }

    public function noticiaAction()
    {
        $alias = ($this->_hasParam('noticia')) ? $this->_getParam('noticia') : null;
        $id = ($alias) ? end(explode('-', $alias)) : null;
        if(!$alias) return $this->_redirect('blog');
        
        $noticia = _utfRow($this->table->get($id));

        if(!$noticia || @$noticia->status_id == 0){
            return $this->_forward('not-found','error','default',array('url'=>URL.'/blog'));
        }

        $noticia = reset($this->table->parseCateg(array($noticia)));
        $next = $this->table->s1('noticias','id,alias','id>'.$noticia->id,'id');
        $prev = $this->table->s1('noticias','id,alias','id<'.$noticia->id,'id');

        $noticias_links = @$this->table->parseUrls(array($noticia,$next,$prev));
        list($noticia,$next,$prev) = $noticias_links;

        // meta tags
        $this->view->titulo = $noticia->titulo;
        $this->view->meta_description = $noticia->titulo.': '.Php_Html::toText($noticia->body);
        // $this->view->meta_canonical = URL.'/noticia/'.$noticia->alias.'-'.$noticia->id;
        $this->view->meta_canonical.= '/'.@$noticia->categoria->alias.'/'.$noticia->alias.'-'.$noticia->id;
        
        $this->view->noticia = $noticia;
        $this->view->prev = $prev;
        $this->view->next = $next;
    }


}

