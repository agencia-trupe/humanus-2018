<?php

class SejaHumanusController extends ZendPlugin_Controller_Action
{
	protected $_require = array(
		'paginas' => 'Db_Paginas',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $paginas = $this->paginas->getPaginas('5,6,7,8');
        $this->view->pagina1 = $paginas[5];
        $this->view->pagina2 = $paginas[6];
        $this->view->pagina3 = $paginas[7];
        $this->view->pagina4 = $paginas[8];

        $pagina_ativa = ($this->_hasParam('pagina')) ? $this->_getParam('pagina') : null;
        $this->view->pagina_ativa = $pagina_ativa;
    }


}

