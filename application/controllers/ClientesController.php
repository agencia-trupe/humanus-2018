<?php

class ClientesController extends ZendPlugin_Controller_Action
{
	protected $_require = array(
		'paginas' => 'Db_Paginas',
		'table'   => 'Db_Clientes2',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->view->pagina = $this->paginas->getPagina(13);
        $this->view->clientes = $this->table->fetchAllWithPhoto('t1.status_id=1','ordem');
        $this->view->segmentos = $this->table->s('segmentos','*','status_id=1','ordem');
        $this->view->cases = $this->table->s('cases','id,alias,titulo,olho','status_id=1','ordem');

        $pagina_ativa = ($this->_hasParam('pagina')) ? $this->_getParam('pagina') : null;
        $this->view->pagina_ativa = $pagina_ativa;
    }

    public function caseAction()
    {
        $alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
        $id = ($alias) ? (int)end(explode('-', $alias)) : null;
        $cases = $this->table->s('cases','*','status_id=1','ordem');
        $case = null;

        if($alias) foreach($cases as $row) if($row->id==$id) $case = $row;
        if(!$case) $case = $cases[0];

        $this->view->case = $case;
        $this->view->cases = $cases;
    }

}

