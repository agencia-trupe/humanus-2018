<?php

class LoginController extends Zend_Controller_Action
{
    public $login;

    public function init()
    {
        // if(APPLICATION_ENV=='production') return $this->_redirect('/');
        // models
        $this->messenger = new Helper_Messenger();
        $this->clientes = new Application_Model_Db_Clientes();
        // $this->promocoes = new Application_Model_Db_Promocoes();
        $this->sessionRedirect = new Zend_Session_Namespace(SITE_NAME."_login_redirect");

        if(!HAS_SALE) $this->_redirect('/');
        if($this->_hasParam('return')){
            $this->sessionRedirect->url = $this->_getParam('return');
            $returnUrlParts = explode('.', $this->sessionRedirect->url);
            
            if(count($returnUrlParts)==3){
                $this->sessionRedirect->module = $returnUrlParts[0];
                $this->sessionRedirect->controller = $returnUrlParts[1];
                $this->sessionRedirect->action = $returnUrlParts[2];
            } else {
                $this->sessionRedirect->controller = $returnUrlParts[0];
                $this->sessionRedirect->action = $returnUrlParts[1];
            }
        }
        
        if(@$this->sessionRedirect->controller=='meu-carrinho'){
            $this->paginas = new Application_Model_Db_Paginas();
            $pagina = $this->paginas->getPagina('treinamentos');
            $this->view->pagina = $pagina;
            $this->view->steps = true;
        }

        $this->view->sessionRedirect = $this->sessionRedirect;
    }

    /**
     * Gera formulário e faz validação do login caso seja Post
     *
     * @see Aluno_LoginController::_process()
     *
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        
        if($request->isPost()){
            $post = $request->getPost();
            
            if(!Application_Model_LoginCliente::process($post)){
                $this->messenger->addMessage("Login incorreto.",'error');
                
                // if($this->sessionRedirect->url) return $this->_redirect(str_replace('.', '/', $this->sessionRedirect->url));

                // $f->populate($post);
            } else {
                $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente');
                // $this->messenger->addMessage("Seja bem-vind".($this->login->user->sexo==1 ? 'o' : 'a')."!");
                $this->messenger->addMessage("Seja bem-vindo!");
                
                if($this->sessionRedirect->url){
                    $urlRedirect = _parseUrlRedirect($this->sessionRedirect->url);
                    if($this->sessionRedirect->controller=='meu-carrinho'){
                        $urlRedirect.= '?redirect=cadastro.choose_c:'.base64_encode($this->view->login->user->id);
                    } else if($this->sessionRedirect->url=='portal.index'){
                        // se for cliente de consultoria
                        if($this->login->consultoria && $this->checkCamposLoginConsultoria()){
                            $urlRedirect = _parseUrlRedirect('portal.consultoria');
                        }
                        // se for cliente de consultoria 2 - instrucoes
                        if($this->login->has_consultoria && $this->login->instrucoes_gerais){
                            $urlRedirect = _parseUrlRedirect('portal.instrucoes');
                        }
                        // se for cliente membro de grupo incompany
                        if($this->login->user->cliente_id){
                            $urlRedirect = _parseUrlRedirect('portal.treinamentos');
                        }
                    }
                    return $this->_redirect($urlRedirect);
                }

                $this->_redirect("portal/consultoria");
                // $this->_redirect("/treinamentos");
                // $this->_redirect("meu-cadastro/");
            }
        }

        $this->view->hide_title = true;
    }

    /**
     * Checa campos necessários para preencher de consultoria
     * Define se redirecionará para finalizar cadastro ou página de consultoria
     */
    public function checkCamposLoginConsultoria()
    {
        if(!$this->login) return false;
        if(!$this->login->user) return false;

        return (bool)trim($this->login->user->nome) &&
               (bool)trim($this->login->user->telefone) &&
               (bool)trim($this->login->user->empresa) &&
               (bool)trim($this->login->user->cargo) &&
               (bool)trim($this->login->user->cpf);
    }

    /**
     * Ação de logout
     * >> Limpa o objeto Zend_Auth
     * >> Destrói a Session
     * >> Redireciona para pag. de login
     *
     */
    public function logoutAction()
    {
		Application_Model_LoginCliente::logout($this,false);
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }

    public function chooseAction()
    {
        // action body
    }


}



