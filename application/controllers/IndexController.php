<?php

class IndexController extends ZendPlugin_Controller_Action
{
    protected $_require = array(
    	'table'     => 'Db_Destaques',
    	'produtos'  => 'Db_Produtos',
    	'eventos'   => 'Db_Eventos',
    );

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	$paginas = $this->table->s('paginas','*','id in (1,3)');
        $this->view->pagina = $paginas[0];
        $this->view->pagina_webinars = $paginas[1];
        
        $produtos = $this->produtos->fetchAllWithPhoto('t1.status_id=1','t1.destaque desc, t1.ordem',3);
		$produtos = $this->produtos->parseModulos($produtos);
		$this->view->produtos = $produtos;
        
        // $this->view->banner = $this->table->fetchRowWithPhoto('t1.id=1');
        $this->view->banners = $this->table->fetchAllWithPhoto('t1.status_id=1','tipo,ordem');
        $this->view->eventos  = _utfRows($this->eventos->getNexts());
        $this->view->webinars = $this->table->s('videos','id,alias,titulo,url','status_id=1','destaque desc,ordem',3);
        $this->view->noticias = $this->table->s('noticias','id,alias,titulo,body,data_cad','status_id=1','destaque desc,data_cad desc',4);
        $this->view->depoimentos = $this->table->s('depoimentos','id,nome,empresa,body','status_id=1','ordem',10);
        $this->view->tecnologias = $this->table->s('tecnologias','id,titulo,capa','status_id=1','ordem');
    }


}