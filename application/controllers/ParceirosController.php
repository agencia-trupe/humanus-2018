<?php

class ParceirosController extends ZendPlugin_Controller_Action
{
	protected $_require = array(
		'paginas' => 'Db_Paginas',
		'table'   => 'Db_Parceiros',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->view->pagina = $this->paginas->getPagina(2);
        $this->view->regioes = $this->table->s('categorias_parceiros','*','status_id=1','ordem');
        $this->view->parceiros = $this->table->fetchAllWithPhoto('t1.status_id=1','ordem');

        $parcNoRegs = array();
        foreach ($this->view->parceiros as $parc)
        	if(!$parc->categorias_id||trim($parc->categorias_id)=='')
        		$parcNoRegs[] = $parc;
        $this->view->parceiros_noregs = $parcNoRegs;
    }


}

