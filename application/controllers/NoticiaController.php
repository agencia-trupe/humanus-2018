<?php

class NoticiaController extends Zend_Controller_Action
{

    public function init()
    {
        // models
        $this->categorias = new Application_Model_Db_CategoriasNoticias();
        $this->noticias = new Application_Model_Db_Noticias();
        // $this->links = new Application_Model_Db_Links();
        
        // $this->view->meta_description = 'Últimas notícias de '.SITE_NAME;
        $this->view->meta_description = 'Últimos artigos e novidades de '.SITE_NAME;
        // $this->view->meta_canonical = URL.'/noticias/';
        $this->view->meta_canonical = URL.'/artigos-e-novidades';
        $this->view->url_noticias = URL.'/artigos-e-novidades';

        $categorias = Is_Array::utf8DbResult($this->categorias->fetchAll(
            'status_id = 1',
            'ordem'
        ));

        $this->view->categorias = $categorias;
    }

    public function indexAction()
    {
        if(!$this->_hasParam('noticia')) return $this->_redirect('noticias');

        $param_alias = $this->_getParam('noticia');
        $param_alias_parts = explode('-', $param_alias);
        $id = array_pop($param_alias_parts);
        $alias = implode('-', $param_alias_parts);
        $noticia = $this->noticias->fetchRowWithPhoto(
            't1.id="'.$id.'"'
        );
        
        if(!$noticia || @$noticia->status_id == 0){
            $this->_forward('not-found','error','default',array('url'=>URL.'/noticias'));
            return false;
        }

        // $noticia->arquivos = $this->noticias->getArquivosById($noticia->id);
        $noticia->fotos = $this->noticias->getFotosById($noticia->id);
        $noticia->categoria = $noticia->categoria_id ? 
            Is_Array::utf8DbRow($this->categorias->fetchRow('id="'.$noticia->categoria_id.'"')) : 
            null;
        $this->view->categoria = $noticia->categoria;
        
        $this->view->noticia = $noticia;
        
        // meta tags
        $this->view->titulo = $noticia->titulo;
        $this->view->meta_description = $noticia->titulo.': '.Php_Html::toText($noticia->body);
        // $this->view->meta_canonical = URL.'/noticia/'.$noticia->alias.'-'.$noticia->id;
        $this->view->meta_canonical.= '/'.$noticia->categoria->alias.'/'.$noticia->alias.'-'.$noticia->id;
        
        // mais noticias
        $rows_destaque = Is_Array::utf8DbRows($this->noticias->fetchAll('status_id = 1 and destaque = 1','id desc',3));
        $rows = Is_Array::utf8DbRows($this->noticias->fetchAll('status_id = 1','id desc',8));

        $rows_destaque = $this->noticias->parseCateg($rows_destaque);
        $rows = $this->noticias->parseCateg($rows);
        $rows_destaque = $this->noticias->parseUrls($rows_destaque);
        $rows = $this->noticias->parseUrls($rows);

        $this->view->noticias = $rows;
        $this->view->noticias_destaque = $rows_destaque;
    }


}

